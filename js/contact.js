﻿// get our objects
var form			= $('#contact-form');
var feedbackSuccess = $('.feedback-success');
var feedbackFailure	= $('.feedback-failure');
	
// called when the user clicks the yes/no links
function hideFeedback()
{
	$(feedbackFailure).fadeOut( 300 );
}

// clears our form
function clearForm()
{
	$(form).trigger( 'reset' );
}

// called when the contact form is submitted
$(form).on( 'submit', function( e ) {
	e.preventDefault();
	
	// disable our send button for the minute
	var btn	= $('#contact-submit', $(form));
	$(btn).attr( 'disabled', 'disabled' );
	
	// hide our feedback
	$(feedbackSuccess).hide();
	$(feedbackFailure).hide();
	
	// send it to our comment php file, which will send the actual email
	$.post( '/php/sendEmail.php', $(form).serialize(), function(data) {
			
			// clear our form
			clearForm();
			
			// show our success message
			$(feedbackSuccess).html( 'Your message was sent!' ).fadeIn( 300 ).delay( 2000 ).fadeOut( 300 );
		},
		'json' // return json
	).error( function( e ) {
		
		// allow them to try by email
		var name = $('#contact-name', $(form)).val();
		var body = encodeURIComponent( $('#contact-body', $(form)).val() );
		
		// create the failure message
		var msg = 'Couldn\'t send your message :( Would you like to try sending it directly by email?<br><a href="mailto:' + getEmail() + '?subject=Contact message from ' + name + '&body=' + body + '" target="_blank" onClick="clearForm(); hideFeedback();">Yes</a> <a href="#" onClick="hideFeedback(); return false;">No</a>';
		
		// show our feedback and fallback options
		$(feedbackFailure).html( msg ).fadeIn( 300 );
		
	}).always( function() {
	
		// re-enable our button
		$(btn).removeAttr( 'disabled' ); 
	});
});