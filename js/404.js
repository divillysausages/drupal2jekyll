﻿// take whatever's in the url and use it to fill our search input, so that they don't have to
// type anything they don't want to
var pathname	= window.location.pathname.replace( /[0-9]{4}\/[0-9]{2}\/[0-9]{2}\//, '' ); // remove the year/month/day timestamp
var path 		= pathname.split( '/' );
var len			= path.length;
var re1			= /[^a-zA-Z0-9]/g; 					// replace everything that's not a char or a number
var re2			= /( |^)[a-zA-Z0-9]{1,2}( |$)/g;	// replace 1 and 2 char words
var re3			= / {2,}/g;							// replace multiple spaces
for( var i = len - 1; i > 0; i-- )
{
	path[i] = path[i].replace( re1, ' ' );
	path[i] = path[i].replace( re2, '' );
}
var input 	= path.join( ' ' ).trim();
input		= input.replace( re3, ' ' );
$( '#main-search-form input' ).val( input ); // set the search terms in our search input field