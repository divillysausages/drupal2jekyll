﻿// get our search string
var half 	= location.search.split( 'query=' ); // set by the search form
var query	= ( half !== undefined ) ? decodeURIComponent( half[1].split( '&' )[0] ) : undefined;
query		= query.replace( /\+/g, ' ' ).trim();
var reQuery	= /[^\s"']+|"([^"]*)"/g // searches words and quoted words

// when we focus the search form, clear it
$( '#main-search-form #query' ).bind( 'focus', function()
{
	$( this ).val( '' );
});

// called when we get our search results - parse and show them
function onSearchResults( results )
{
	// show how many results we found
	$( '#search-feedback' ).html( results.length + ' results found for <span id="search-feedback-query">' + query + '</span>' );
	if( results.length == 0 )
		return;
		
	// split our query to find the elements
	var querySplit = query.match( reQuery );
	if( querySplit == undefined ) // NOTE: ==, not === to also match 'null'
		querySplit = [];
		
	// go through our query and remove any quotes and short strings
	for( var i = querySplit.length - 1; i >= 0; i-- )
	{
		// remove any quotes
		var q = querySplit[i];
		if( q[0] == '"' )
			q = q.substring( 1, q.length - 1 );
			
		// set our updated string, or remove it if it's too short
		var minLen = ( querySplit.length == 1 ) ? 2 : 3;
		if( q.length <= minLen )
			querySplit.splice( i, 1 );
		else
			querySplit[i] = q.toLowerCase(); // to make our life easier
	}
	
	// sort by length so longer strings are matched first
	querySplit.sort( function( a, b )
	{
		return b.length - a.length;
	});
	
	// create our regExp
	var len		= querySplit.length;
	var reStr 	= ( len > 0 ) ? "(\\b" : null;
	for( i = 0; i < len; i++ )
		reStr += querySplit[i] + "\\b" + ( ( i == len - 1 ) ? ")" : "|\\b" );
	var re = ( reStr != null ) ? new RegExp( reStr, "ig" ) : null;
	
	// show our results
	var html	= '';
	len 		= results.length;
	for( i = 0; i < len; i++ )
	{
		// create our title, and match our search result
		html 			+= '<div class="search-result"><p><a class="search-result-title" href="' + results[i].link + '">' + results[i].title + '</a><a class="search-result-link" href="' + results[i].link + '">' + results[i].link + '</a></p>';
		content			= results[i].summary;
		var contentLen	= content.length;
		var desc		= '';
		var count		= 0;
		var ei			= -1; // our end index
		if( re != null && i < 5 ) // only show snippets for the first 5
		{
			while( m = re.exec( content ) ) // use exec so we get the indexes etc
			{
				// get our start index - if it overlaps with our previous end index, ignore
				var si = m.index - 60;
				if( si < 0 ) si = 0;
				if( si <= ei )
					continue;
					
				// get our end index
				ei = m.index + m[0].length + 60;
				if( ei > contentLen ) 
					ei = contentLen;
				
				// get our string
				var str = content.substring( si, ei );
					
				// strip any html out
				var index 		= str.indexOf( '<' );
				var hasCleanEnd	= false;
				while( index != -1 )
				{
					var endIndex 	= str.indexOf( '>', index );
					str				= ( endIndex === -1 ) ? str.substring( 0, index ) : str.substring( 0, index ) + str.substring( endIndex + 1 );
					index			= str.indexOf( '<', index );
					hasCleanEnd		= ( endIndex === -1 ); // so we don't strip the last word out, below
				}
				index = str.indexOf( '>' ); // in case the start of the tag is outside our str
				if( index != -1 )
					str = str.substring( index + 1 );
					
				// strip to the first and last proper word
				index 	= str.indexOf( ' ' );
				str		= ( index === -1 ) ? '' : str.substring( index + 1 );
				if( !hasCleanEnd )
				{
					index	= str.lastIndexOf( ' ' );
					str		= ( index === -1 ) ? '' : str.substring( 0, index );
				}
						
				// check that we still have a result
				if( str.length > 0 )
				{
					// check if our query is in the result
					var strLower 	= str.toLowerCase(); // the query is lower case
					var qLen		= querySplit.length;
					var found		= false;
					for( var j = 0; j < qLen; j++ )
					{
						if( !found && strLower.indexOf( querySplit[j] ) !== -1 )
							found = true;
					}
					
					// if we still have the query in our str, add it
					if( found )
					{
						str		= str.replace( /[\n\t]+/g, ' ' ); // replace new lines and tabs
						desc 	+= str + ' ... ';
						
						// limit how much we show
						if( count++ >= 1 ) // return max 2 results
							break;
					}
				}
			}
		}
		
		// add our description to our html
		if( desc.length > 0 )
			html += '<p>... ' + desc + '</p>';
		
		// tidy up our div
		html += '</div>';
	}
	
	// append our search results
	$( '#search-results' ).append( html );
}

// if we have a query, then treat it
if( query !== undefined )
{
	// set the value in the search form
	$( '#main-search-form #query' ).val( query );
	$( '#search-feedback' ).html( 'Searching for <span id="search-feedback-query">' + query + '</span>...' );
	
	// get our search
	$.ajax(
	{
		type:'POST',
		url:'http://www.tapirgo.com/api/1/search.json?token=YOUR_SEARCH_TOKEN&query=' + encodeURIComponent( query ),
		dataType:'jsonp'
	}).done( onSearchResults ).fail( function( e )
	{
		onSearchResults( [] ); // show nothing
	});
}