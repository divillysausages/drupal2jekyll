package view 
{
	import com.bit101.components.CheckBox;
	import com.bit101.components.InputText;
	import com.bit101.components.Label;
	import com.bit101.components.Panel;
	import com.bit101.components.PushButton;
	import com.bit101.components.RadioButton;
	import com.divillysausages.ds.DS;
	import controllers.output.ConvertToPageController;
	import controllers.output.HTAccess301Controller;
	import controllers.output.OutputController;
	import controllers.output.ReplaceSiteURLController;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import flash.net.FileFilter;
	import flash.utils.Dictionary;
	import mx.utils.StringUtil;
	import org.osflash.signals.Signal;
	
	/**
	 * The view where we can modify our settings
	 * @author Damian Connolly
	 */
	public class Settings extends Panel 
	{
		
		/******************************************************************************/
		
		/**
		 * The signal dispatched when we're hidden. It should take no parameters
		 */
		public var signalOnHide:Signal = null;
		
		/******************************************************************************/
		
		private var m_txtXMLFolder:Label				= null;	// the label for the xml folders path
		private var m_btnXMLFolder:PushButton			= null;	// the button to change the xml folder
		private var m_txtSiteURL:InputText				= null;	// the input text for the site url
		private var m_txtSiteFilesFolder:InputText		= null;	// the input text for the site files folder
		private var m_txtCSSPath:Label					= null;	// the label for the css path
		private var m_btnCSSPath:PushButton				= null; // the button for the css path
		private var m_txtDeadLinkFolder:Label			= null;	// the label for the dead link folder
		private var m_btnDeadLinkFolder:PushButton		= null;	// the button for the dead link folder
		private var m_txtJekyllFolder:Label 			= null;	// the label for the save posts folder output
		private var m_btnJekyllFolder:PushButton		= null;	// the button to change the save posts folder output
		private var m_btnClose:PushButton				= null;	// the button to close the panel
		private var m_txtStyleCodeType:InputText		= null;	// the input text for the style code type
		private var m_txtJekyllCodeType:InputText		= null; // the input text for the jekyll style code type
		private var m_toggleJekyllCodeLineNos:CheckBox	= null;	// the check box for if we should use line nos
		private var m_txtQuoteType:InputText			= null;	// the quote type that we want to use (' or ")
		private var m_txtPages:InputText				= null;	// the input text for the urls to treat as pages, not posts
		private var m_txtTapirSecret:InputText			= null;	// the input text for our tapir go secret
		private var m_controllerToggles:Dictionary		= null;	// the toggles for our controllers
		private var m_toggleAlsoUseSiteURL:CheckBox		= null;	// the check box for if we should use site.url
		private var m_toggleUse302:CheckBox				= null;	// the check box for if we should use 302 redirects instead of 301
		private var m_toggleLines:Shape					= null;	// a shape to we can easily match the toggles with their box
		
		/******************************************************************************/
		
		/**
		 * Sets if the view is currently enabled or not
		 */
		public function set isEnabled( b:Boolean ):void
		{
			this.m_txtXMLFolder.enabled				= b;
			this.m_btnXMLFolder.enabled				= b;
			this.m_txtSiteURL.enabled				= b;
			this.m_txtSiteFilesFolder.enabled		= b;
			this.m_txtCSSPath.enabled				= b;
			this.m_btnCSSPath.enabled				= b;
			this.m_txtDeadLinkFolder.enabled		= b;
			this.m_btnDeadLinkFolder.enabled		= b;
			this.m_txtJekyllFolder.enabled			= b;
			this.m_btnJekyllFolder.enabled			= b;
			this.m_txtStyleCodeType.enabled			= b;
			this.m_txtJekyllCodeType.enabled		= b;
			this.m_toggleJekyllCodeLineNos.enabled	= b;
			this.m_txtQuoteType.enabled				= b;
			this.m_txtPages.enabled					= b;
			this.m_txtTapirSecret.enabled			= b;
			this.m_toggleAlsoUseSiteURL.enabled		= b;
			this.m_toggleUse302.enabled				= b;
			for each( var key:* in this.m_controllerToggles )
			{
				var check:CheckBox 	= ( key as CheckBox );
				check.enabled 		= b;
			}
		}
		
		/******************************************************************************/
		
		public function Settings( parent:DisplayObjectContainer = null, xpos:Number = 0, ypos:Number = 0 ) 
		{
			super( parent, xpos, ypos );
			
			var main:Main 				= ( DS.main as Main );
			this.m_controllerToggles	= new Dictionary;
			
			var totalWidth:Number = DS.stage.stageWidth * 0.7;
			
			// create our xml folder
			this.m_txtXMLFolder = new Label( this, 10, 10, "Drupal XML folder:" );
			this.m_btnXMLFolder	= new PushButton( this, totalWidth - 110.0, this.m_txtXMLFolder.y, "Change", this._onClickXMLFolder );
			this._updateXMLFolder();
			
			// create our site url
			var label:Label			= new Label( this, 10, this.m_txtXMLFolder.y + this.m_txtXMLFolder.height + 5.0, "Site URL (without trailing slash):" );
			this.m_txtSiteURL 		= new InputText( this, label.x + label.width + 5.0, label.y, main.settings.siteURL, this._onChangeSiteURL );
			this.m_txtSiteURL.width	= totalWidth - this.m_txtSiteURL.x - 10.0;
			
			// create our site files folder
			label							= new Label( this, 10, this.m_txtSiteURL.y + this.m_txtSiteURL.height + 5.0, "Site files folder name (without trailing slash):" );
			this.m_txtSiteFilesFolder		= new InputText( this, label.x + label.width + 5, label.y, main.settings.siteFilesFolder, this._onChangeSiteFilesFolder );
			this.m_txtSiteFilesFolder.width	= totalWidth - this.m_txtSiteFilesFolder.x - 10.0;
			
			// create our css file
			this.m_txtCSSPath 	= new Label( this, 10, this.m_txtSiteFilesFolder.y + this.m_txtSiteFilesFolder.height + 5.0, "Site CSS file: " );
			this.m_btnCSSPath	= new PushButton( this, totalWidth - 110.0, this.m_txtCSSPath.y, "Browse", this._onClickCSSPath );
			this._updateCSSPath();
			
			// create our dead link file
			this.m_txtDeadLinkFolder 	= new Label( this, 10, this.m_txtCSSPath.y + this.m_txtCSSPath.height + 5.0, "Dead link folder: " );
			this.m_btnDeadLinkFolder	= new PushButton( this, totalWidth - 110.0, this.m_txtDeadLinkFolder.y, "Change", this._onClickDeadLinkFolder );
			this._updateDeadLinkFolder();
			
			// create our save folder
			this.m_txtJekyllFolder 	= new Label( this, 10, this.m_txtDeadLinkFolder.y + this.m_txtDeadLinkFolder.height + 5.0, "Jekyll folder: " );
			this.m_btnJekyllFolder	= new PushButton( this, totalWidth - 110.0, this.m_txtJekyllFolder.y, "Change", this._onClickJekyllFolder );
			this._updateJekyllFolder();
			
			// create our style code type
			label							= new Label( this, 10, this.m_txtJekyllFolder.y + this.m_txtJekyllFolder.height + 5.0, "Default <code> type (style):" );
			this.m_txtStyleCodeType 		= new InputText( this, label.x + label.width + 5, label.y, main.settings.styleCodeType, this._onChangeStyleCodeType );
			this.m_txtStyleCodeType.width	= totalWidth - this.m_txtStyleCodeType.x - 10.0;
			
			// create our jekyll code type
			label						= new Label( this, 10, this.m_txtStyleCodeType.y + this.m_txtStyleCodeType.height + 5.0, "Jekyll <code> type {% highlight %}:" );
			this.m_txtJekyllCodeType	= new InputText( this, label.x + label.width + 5, label.y, main.settings.jekyllCodeType, this._onChangeJekyllCodeType );
			
			// create if we should use line numbers or not
			this.m_toggleJekyllCodeLineNos 			= new CheckBox( this, 10, this.m_txtJekyllCodeType.y, "Use line numbers", this._onChangeJekyllCodeLineNos );
			this.m_toggleJekyllCodeLineNos.selected	= main.settings.jekyllCodeLineNos;
			this.m_toggleJekyllCodeLineNos.x		= totalWidth - this.m_toggleJekyllCodeLineNos.width - 10.0;
			this.m_toggleJekyllCodeLineNos.y		= this.m_txtJekyllCodeType.y + this.m_txtJekyllCodeType.height * 0.5 - this.m_toggleJekyllCodeLineNos.height * 0.5;
			this.m_txtJekyllCodeType.width			= this.m_toggleJekyllCodeLineNos.x - this.m_txtJekyllCodeType.x - 5.0;
			
			// create our quote type
			label							= new Label( this, 10, this.m_txtJekyllCodeType.y + this.m_txtJekyllCodeType.height + 5.0, "Quote type (' or \"):" );
			this.m_txtQuoteType				= new InputText( this, label.x + label.width + 5, label.y, main.settings.quoteType, this._onChangeQuoteType );
			this.m_txtQuoteType.restrict	= "\"'";
			this.m_txtQuoteType.width		= totalWidth - this.m_txtQuoteType.x - 10.0;
			
			// create our pages url
			label						= new Label( this, 10, this.m_txtQuoteType.y + this.m_txtQuoteType.height + 5.0, "URLs to treat as pages, not posts (e.g. 'contact,cv,about'):" );
			this.m_txtPages				= new InputText( this, label.x + label.width + 5, label.y, main.settings.pages.join( "," ), this._onAddPageURL );
			this.m_txtPages.restrict	= "^ "; // only disallow spaces
			this.m_txtPages.width		= totalWidth - this.m_txtPages.x - 10.0;
			
			// create our tapir go secret
			label						= new Label( this, 10, this.m_txtPages.y + this.m_txtPages.height + 5.0, "TapirGo secret token:" );
			this.m_txtTapirSecret		= new InputText( this, label.x + label.width + 5, label.y, main.settings.tapirSecret, this._onChangeTapirSecret );
			this.m_txtTapirSecret.width	= totalWidth - this.m_txtTapirSecret.x - 10.0;
			
			// create all our enabled buttons
			var controllers:Vector.<OutputController> 	= main.controllerManager.controllers;
			var y:Number								= this.m_txtTapirSecret.y + this.m_txtTapirSecret.height + 5.0;
			var toggleX:Number							= totalWidth - 20.0;
			this.m_toggleLines							= new Shape;
			this.m_toggleLines.graphics.lineStyle( 1.0, 0, 0.1 );
			for each( var controller:OutputController in controllers )
			{
				var classname:String				= controller.simpleClassname.substring( 0, controller.simpleClassname.indexOf( "Controller" ) );
				var labelStr:String					= classname + ( ( controller.tooltip != null ) ? ": " + controller.tooltip : "" );
				label 								= new Label( this, 10, y, labelStr );
				var toggle:CheckBox					= new CheckBox( this, toggleX, label.y + label.height * 0.5 - 5, "", this._onToggleControllerEnable );
				toggle.selected						= main.settings.getUseController( controller.simpleClassname );
				toggle.enabled						= !( controller is ConvertToPageController );
				y									+= label.height;
				this.m_controllerToggles[toggle] 	= controller;
				
				// if this is the replace site url controller, add the checkbox for site.url
				if ( controller is ReplaceSiteURLController )
				{
					// create if we should use line numbers or not
					this.m_toggleAlsoUseSiteURL 			= new CheckBox( this, 10, toggle.y, "Also use site.url", this._onChangeAlsoUseSiteURL );
					this.m_toggleAlsoUseSiteURL.selected	= main.settings.alsoUseSiteURL;
					this.m_toggleAlsoUseSiteURL.x			= totalWidth - this.m_toggleAlsoUseSiteURL.width - 10.0;
					this.m_toggleAlsoUseSiteURL.y			= toggle.y;
					
					// move the toggle
					toggle.x = this.m_toggleAlsoUseSiteURL.x - toggle.width;
				}
				else if ( controller is HTAccess301Controller ) // if it's the 301 redirect controller, add the checkbox for using 302
				{
					// create the checkbox for using 302 redirects
					this.m_toggleUse302				= new CheckBox( this, 10, toggle.y, "Use 302 instead", this._onChangeUse302 );
					this.m_toggleUse302.selected	= main.settings.use302Redirects;
					this.m_toggleUse302.x			= totalWidth - this.m_toggleUse302.width - 10.0;
					this.m_toggleUse302.y			= toggle.y;
					
					// move the toggle
					toggle.x = this.m_toggleUse302.x - toggle.width;
				}
				
				// draw our line
				this.m_toggleLines.graphics.moveTo( label.x + label.width + 5.0, toggle.y + toggle.height * 0.5 );
				this.m_toggleLines.graphics.lineTo( toggle.x - 5.0, toggle.y + toggle.height * 0.5 );
			}
			
			this.addChild( this.m_toggleLines );
			
			// create our close button
			this.m_btnClose = new PushButton( this, this.m_btnXMLFolder.x, y + 5, "OK", this._onClickClose );
			this._validate();
			
			// set our size
			this.width 	= this.m_btnClose.x + this.m_btnClose.width + 10;
			this.height = this.m_btnClose.y + this.m_btnClose.height + 10;
			
			// create our signal
			this.signalOnHide = new Signal;
		}
		
		/**
		 * Shows the Settings panel
		 */
		public function show():void
		{
			this.x 					= DS.stage.stageWidth * 0.5 - this.width * 0.5;
			this.y 					= DS.stage.stageHeight * 0.5 - this.height * 0.5;
			this.m_btnClose.enabled	= !( DS.main as Main ).settings.needsInput; // can't close if we need input
			DS.main.addChild( this );
		}
		
		/**
		 * Hides the Settings panel
		 */
		public function hide():void
		{
			if ( this.parent != null )
			{
				this.parent.removeChild( this );
				this.signalOnHide.dispatch();
			}
		}
		
		/******************************************************************************/
		
		// called when our site url changes
		private function _onChangeSiteURL( e:Event ):void
		{
			( DS.main as Main ).settings.siteURL = this.m_txtSiteURL.text;
			this._validate();
		}
		
		// called whe nour site files folder changes
		private function _onChangeSiteFilesFolder( e:Event ):void
		{
			( DS.main as Main ).settings.siteFilesFolder = this.m_txtSiteFilesFolder.text;
			this._validate();
		}
		
		// called when we click on the css file button to search for a new css file
		private function _onClickCSSPath( e:Event ):void
		{
			var file:File = this._createBrowseFile( this._onSelectCSSPath );
			file.browseForOpen( "Choose your main site CSS file", [new FileFilter( "CSS files", "*.css" )] );
		}
		
		// called when we select our css file
		private function _onSelectCSSPath( e:Event ):void
		{
			var file:File = ( e.target as File );
			( DS.main as Main ).settings.cssFile = file;
			this._updateCSSPath();
			this._cleanUpFile( e );
		}
		
		// updates the css file display
		private function _updateCSSPath():void
		{
			var s:String = "Site CSS file: ";
			if ( ( DS.main as Main ).settings.cssFile != null )
				s += ( DS.main as Main ).settings.cssFile.url;
				
			// clamp it
			if ( s.length > 47 )
				s = "Site CSS file: ..." + s.substr( -47 );
			this.m_txtCSSPath.text = s;
			
			// check if the ok button is good
			this._validate();
		}
		
		// called when we click on the dead link button to look for our dead link file
		private function _onClickDeadLinkFolder( e:Event ):void
		{
			var file:File = this._createBrowseFile( this._onSelectDeadLinkFolder );
			file.browseForDirectory( "Choose the folder to save the dead links file in" );
		}
		
		// called when we select our dead link path
		private function _onSelectDeadLinkFolder( e:Event ):void
		{
			var file:File = ( e.target as File );
			( DS.main as Main ).settings.deadLinkFolder = file;
			this._updateDeadLinkFolder();
			this._cleanUpFile( e );
		}
		
		// updates the dead link folder display
		private function _updateDeadLinkFolder():void
		{
			var s:String = "Dead link folder: ";
			if ( ( DS.main as Main ).settings.deadLinkFolder != null )
				s += ( DS.main as Main ).settings.deadLinkFolder.url;
				
			// clamp if
			if ( s.length > 47 )
				s = "Dead link folder: " + s.substr( -47 );
			this.m_txtDeadLinkFolder.text = s;
			
			// check if the ok button is good
			this._validate();
		}
		
		// called when we click on the xml folder button to update the xml folder location
		private function _onClickXMLFolder( e:Event ):void
		{
			var file:File = this._createBrowseFile( this._onSelectXMLFolder );
			file.browseForDirectory( "Choose the XML data folder where you've extracted your Drupal data as XML files" );
		}
		
		// called when we select our xml folder
		private function _onSelectXMLFolder( e:Event ):void
		{
			var file:File = ( e.target as File );
			( DS.main as Main ).settings.xmlFolder = file;
			this._updateXMLFolder();
			this._cleanUpFile( e );
		}
		
		// updates the xml folder display
		private function _updateXMLFolder():void
		{
			var s:String = "Drupal XML folder: ";
			if ( ( DS.main as Main ).settings.xmlFolder != null )
				s += ( DS.main as Main ).settings.xmlFolder.url;
				
			// clamp it
			if ( s.length > 45 )
				s = "Drupal XML folder: ..." + s.substr( -45 );
			this.m_txtXMLFolder.text = s;
			
			// check if the ok button is good
			this._validate();
		}
		
		// called when we click on the save folder button to update the jekyll folder location
		private function _onClickJekyllFolder( e:Event ):void
		{
			var file:File = this._createBrowseFile( this._onSelectJekyllFolder );
			file.browseForDirectory( "Choose the jekyll root folder to save the output to" );
		}
		
		// called when we select our save folder
		private function _onSelectJekyllFolder( e:Event ):void
		{
			var file:File = ( e.target as File );
			( DS.main as Main ).settings.jekyllFolder = file;
			this._updateJekyllFolder();
			this._cleanUpFile( e );
		}
		
		// updates the save folder display
		private function _updateJekyllFolder():void
		{
			var s:String = "Jekyll folder: ";
			if ( ( DS.main as Main ).settings.jekyllFolder != null )
				s += ( DS.main as Main ).settings.jekyllFolder.url;
				
			// clamp it
			if ( s.length > 45 )
				s = "Jekyll folder: ..." + s.substr( -45 );
			this.m_txtJekyllFolder.text = s;
			
			// check if the ok button is good
			this._validate();
		}
		
		// updates the style code type
		private function _onChangeStyleCodeType( e:Event ):void
		{
			( DS.main as Main ).settings.styleCodeType = StringUtil.trim( this.m_txtStyleCodeType.text );
			this._validate();
		}
		
		// updates the jekyll code type
		private function _onChangeJekyllCodeType( e:Event ):void
		{
			( DS.main as Main ).settings.jekyllCodeType = StringUtil.trim( this.m_txtJekyllCodeType.text );
			this._validate();
		}
		
		// updates the jekyll code use linenos
		private function _onChangeJekyllCodeLineNos( e:Event ):void
		{
			( DS.main as Main ).settings.jekyllCodeLineNos = this.m_toggleJekyllCodeLineNos.selected;
			this._validate();
		}
		
		// updates the quote type
		private function _onChangeQuoteType( e:Event ):void
		{
			( DS.main as Main ).settings.quoteType = StringUtil.trim( this.m_txtQuoteType.text );
			this._validate();
		}
		
		// called when we're adding a page url to the list of pages to treat as pages
		private function _onAddPageURL( e:Event ):void
		{
			( DS.main as Main ).settings.pages = this.m_txtPages.text.split( "," ); // no need to trim as space is disallowed
			this._validate();
		}
		
		// called when we change our tapir go secret
		private function _onChangeTapirSecret( e:Event ):void
		{
			( DS.main as Main ).settings.tapirSecret = StringUtil.trim( this.m_txtTapirSecret.text );
			this._validate();
		}
		
		// called when we click on a toggle for a controller
		private function _onToggleControllerEnable( e:Event ):void
		{
			var check:CheckBox				= ( e.target as CheckBox );
			var controller:OutputController	= ( this.m_controllerToggles[check] as OutputController );
			if ( controller != null )
				( DS.main as Main ).settings.setUseController( controller.simpleClassname, check.selected );
			else
				DS.error( this, "Can't find the classname for a checkbox" );
		}
		
		// updates if we should also use site.url
		private function _onChangeAlsoUseSiteURL( e:Event ):void
		{
			( DS.main as Main ).settings.alsoUseSiteURL = this.m_toggleAlsoUseSiteURL.selected;
			this._validate();
		}
		
		// updates if we should use 302 redirects instead of 301
		private function _onChangeUse302( e:Event ):void
		{
			( DS.main as Main ).settings.use302Redirects = this.m_toggleUse302.selected;
			this._validate();
		}
		
		// creates a file object to browse for a file/object
		private function _createBrowseFile( onSelect:Function ):File
		{
			var file:File = new File;
			file.addEventListener( Event.SELECT, onSelect );
			file.addEventListener( Event.CANCEL, this._cleanUpFile );
			file.addEventListener( IOErrorEvent.IO_ERROR, this._cleanUpFile );
			file.addEventListener( SecurityErrorEvent.SECURITY_ERROR, this._cleanUpFile );
			return file;
		}
		
		// cleans up the file object
		private function _cleanUpFile( e:Event ):void
		{
			var file:File = ( e.target as File );
			
			// this can get called from an error, so we remove the SELECT event for all possible events
			file.removeEventListener( Event.SELECT, this._onSelectXMLFolder );
			file.removeEventListener( Event.SELECT, this._onSelectCSSPath );
			file.removeEventListener( Event.SELECT, this._onSelectDeadLinkFolder );
			file.removeEventListener( Event.SELECT, this._onSelectJekyllFolder );
			file.removeEventListener( Event.CANCEL, this._cleanUpFile );
			file.removeEventListener( IOErrorEvent.IO_ERROR, this._cleanUpFile );
			file.removeEventListener( SecurityErrorEvent.SECURITY_ERROR, this._cleanUpFile );
		}
		
		// checks if we can click the close button
		private function _validate():void
		{
			if( this.m_btnClose != null )
				this.m_btnClose.enabled	= !( DS.main as Main ).settings.needsInput; // can't close if we need input
		}
		
		// called when we click the close button
		private function _onClickClose( e:Event ):void
		{
			this.hide();
		}
		
	}

}