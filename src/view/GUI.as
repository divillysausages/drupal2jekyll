package view 
{
	import com.bit101.components.ProgressBar;
	import com.bit101.components.PushButton;
	import com.bit101.components.TextArea;
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import flash.events.Event;
	import flash.utils.getTimer;
	
	/**
	 * The GUI creates the main interface for the app
	 * @author Damian Connolly
	 */
	public class GUI 
	{
		
		/******************************************************************************/
		
		/**
		 * The font that we've embedded
		 */
		public static const FONT:String = "PF Ronda Seven";
		
		/******************************************************************************/
		
		[Embed(source="../../assets/pf_ronda_seven.ttf", embedAsCFF="false", fontName="PF Ronda Seven", mimeType="application/x-font")]
		private var m_ronda:Class;
		
		/******************************************************************************/
		
		private var m_pcCurrController:ProgressBar	= null;	// the progress bar for the current controller
		private var m_pcTotal:ProgressBar			= null;	// the progress bar for the total
		private var m_btnStart:PushButton			= null;	// the button to start it all off
		private var m_btnSettings:PushButton		= null;	// the button to show our settings
		private var m_log:TextArea					= null;	// the area for our messages
		private var m_settings:Settings				= null;	// the settings view
		private var m_startTime:int					= 0;	// the time when we started
		
		/******************************************************************************/
		
		public function set isEnabled( b:Boolean ):void
		{
			this.m_btnStart.enabled		= b;
			this.m_btnSettings.enabled	= b;
		}
		
		/******************************************************************************/
		
		public function GUI() 
		{
			var main:Main = ( DS.main as Main );
			
			// create our progress bars
			this.m_pcCurrController 		= new ProgressBar( main, 10, 10 );
			this.m_pcTotal					= new ProgressBar( main, this.m_pcCurrController.x, this.m_pcCurrController.y + this.m_pcCurrController.height + 5.0 );
			this.m_pcCurrController.width	= DS.stage.stageWidth - 20;
			this.m_pcTotal.width			= this.m_pcCurrController.width;
			
			// create the start button
			this.m_btnStart = new PushButton( main, this.m_pcCurrController.x, this.m_pcTotal.y + this.m_pcTotal.height + 5.0, "Start", this._onClickStart );
			
			// create the settings button
			this.m_btnSettings = new PushButton( main, this.m_btnStart.x + this.m_btnStart.width + 10, this.m_btnStart.y, "Settings", this._onClickSettings );
			
			// create our log
			this.m_log 			= new TextArea( main, 10, this.m_btnStart.y + this.m_btnStart.height + 5.0 );
			this.m_log.html		= true;
			this.m_log.width	= DS.stage.stageWidth - 20.0;
			this.m_log.height	= DS.stage.stageHeight - this.m_log.y - 10.0;
			
			// create our settings
			this.m_settings = new Settings;
			this.m_settings.signalOnHide.add( this._onSettingsHide );
			
			// set up our signals
			main.xmlDataLoader.signalOnFinished.add( this._onXMLDataLoaded );
			main.controllerManager.signalOnUpdate.add( this._onControllersUpdate );
			main.controllerManager.signalOnComplete.add( this._onControllersComplete );
			
			// if we need input, open the settings
			if ( main.settings.needsInput )
				this.m_settings.show();
		}
		
		/**
		 * Logs a message to the log on the screen
		 * @param level The level to log the message at. Use the static consts in the Log class
		 * @param msg The message that we want to log
		 */
		public function log( level:uint, msg:String ):void
		{
			if ( level == Log.DEBUG )
				this.m_log.text += "<font color='#999999'>" + msg + "</font>\n";
			else if ( level == Log.LOG )
				this.m_log.text += "<font color='#000000'>" + msg + "</font>\n";
			else if ( level == Log.WARN )
				this.m_log.text += "<font color='#999900'>" + msg + "</font>\n";
			else
				this.m_log.text += "<font color='#990000'>" + msg + "</font>\n";
				
			DS.callLater( this.m_log.scrollToBottom, 100 ); // calls next frame
		}
		
		/******************************************************************************/
		
		// called when we click on the start button
		private function _onClickStart( e:Event ):void
		{
			// if settings are showing, do nothing
			if ( this.m_settings.parent != null )
				return;
				
			this.isEnabled		= false;
			this.m_startTime	= getTimer();
			this.m_log.text		= "";
			( DS.main as Main ).xmlDataLoader.loadData();
		}
		
		// called when all our xml data has loaded
		private function _onXMLDataLoaded():void
		{
			// get our site to parse our xml
			if ( !( DS.main as Main ).site.parseXMLFiles() )
			{
				this.isEnabled = true;
				return;
			}
			
			// start the conversion
			this.log( Log.LOG, "Starting conversion..." );
			( DS.main as Main ).controllerManager.start();
		}
		
		// called when we click the settings button
		private function _onClickSettings( e:Event ):void
		{
			this.m_settings.show();
			this.isEnabled = false;
		}
		
		// called when our settings have been hidden
		private function _onSettingsHide():void
		{
			this.isEnabled = true;
		}
		
		// called when our controllers manager is updating
		private function _onControllersUpdate( currPC:Number, totalPC:Number ):void
		{
			this.m_pcCurrController.value	= currPC;
			this.m_pcTotal.value			= totalPC;
		}
		
		// called when our controllers manager is complete
		private function _onControllersComplete():void
		{
			this.isEnabled = true;
			this.log( Log.LOG, "Conversion complete; " + ( DS.main as Main ).site.nodes.length + " pages treated in " + ( ( getTimer() - this.m_startTime ) / 1000 ).toFixed( 2 ) + "s" );
		}
		
	}

}