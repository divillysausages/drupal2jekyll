package model.app 
{
	import flash.system.System;
	
	/**
	 * The class that holds the data that we've loaded from an XML
	 * @author Damian Connolly
	 */
	public class XMLData
	{
			
		/******************************************************************************/
		
		/**
		 * The class that this is for
		 */
		public var clazz:Class = null;
		
		/**
		 * The data that we've loaded for this class, or null if we don't have it
		 */
		public var data1:XML = null;
		
		/**
		 * The data that we've loaded from the second file, if we have one
		 */
		public var data2:XML = null;
		
		/**
		 * The filename for the data that we're going to load
		 */
		public var filename1:String = null;
		
		/**
		 * The second filename to load, if we need to load one
		 */
		public var filename2:String = null;
			
		/******************************************************************************/
		
		/**
		 * Creates a new XMLData object
		 * @param clazz The class that this data is for
		 * @param filename1 The filename for the data that we're going to load
		 * @param filename2 The second filename to load for this class, if we need one
		 */
		public function XMLData( clazz:Class, filename1:String, filename2:String = null )
		{
			this.clazz		= clazz;
			this.filename1	= filename1;
			this.filename2	= filename2;
		}
		
		/**
		 * Clears any data that we've loaded, freeing up memory
		 */
		public function clear():void
		{
			if ( this.data1 != null )
				System.disposeXML( this.data1 );
			if ( this.data2 != null )
				System.disposeXML( this.data2 );
				
			this.data1 	= null;
			this.data2	= null;
		}
		
	}

}