package model.app 
{
	import com.divillysausages.ds.cache.LSO;
	import com.divillysausages.ds.collections.linkedlist.LinkedList;
	import com.divillysausages.ds.util.ClassUtils;
	import flash.filesystem.File;
	import mx.utils.StringUtil;
	
	/**
	 * The settings for the app itself
	 * @author Damian Connolly
	 */
	public class Settings 
	{
		
		/******************************************************************************/
		
		private static const M_LSO_NAME:String 					= "settings";			// the name of our lso
		private static const M_KEY_XML_FOLDER_PATH:String		= "xmlFolderPath";		// the key for the xml folder path where we load our data
		private static const M_KEY_JEKYLL_FOLDER_PATH:String	= "jekyllFolderPath";	// the key for the jekyll folder path
		private static const M_KEY_SITE_URL:String				= "siteURL";			// the key for the site url
		private static const M_KEY_SITE_FILES_FOLDER:String		= "siteFiles";			// the key for the site files folder
		private static const M_KEY_CSS_PATH:String				= "cssPath";			// the path to load our css
		private static const M_KEY_DEAD_LINK_FOLDER_PATH:String	= "deadLinkFolder";		// the key for the dead link folder path
		private static const M_KEY_STYLE_CODE_TYPE:String		= "styleCodeType";		// the default code type
		private static const M_KEY_JEKYLL_CODE_TYPE:String		= "jekyllCodeType";		// the key for the default jekyll code type
		private static const M_KEY_JEKYLL_CODE_LINENOS:String	= "jekyllCodeLineNos";	// the key for if we should use linenos or not
		private static const M_QUOTE_TYPE:String				= "quoteType";			// the type of quotes we want to use
		private static const M_PAGES:String						= "pages";				// the key for our pages (not posts)
		private static const M_KEY_TAPIR_SECRET:String			= "tapirSecret";		// the key for our tapir secret
		private static const M_KEY_ALSO_USE_SITE_URL:String		= "useSiteURL";			// use site.url as well as site.baseurl
		private static const M_KEY_USE_302:String				= "use302";				// use 302 redirects instead of 301
		
		/******************************************************************************/
		
		private var m_lso:LSO 						= null; // the lso that holds our settings
		private var m_xmlFolderPath:String			= null;	// the folder path for our xml files
		private var m_xmlFolder:File				= null;	// the xml file folder
		private var m_jekyllFolderPath:String		= null;	// the folder path for our jekyll installation
		private var m_jekyllFolder:File				= null;	// the jekyll folder
		private var m_savePostsFolder:File			= null;	// the folder to save our output to
		private var m_savePagesFolder:File			= null;	// the folder to save our pages to
		private var m_saveCommentsFolder:File		= null;	// the folder to save our comments in
		private var m_saveFilesFolder:File			= null;	// the folder to save our files in
		private var m_saveOpenGraphFolder:File		= null; // the folder to save our open graph files in
		private var m_siteURL:String				= null;	// the main site url
		private var m_siteFilesFolder:String		= null;	// the site files folder
		private var m_cssPath:String				= null;	// the path for our css file
		private var m_cssFile:File					= null;	// our css file
		private var m_deadLinkFolderPath:String		= null; // the path for our dead link folder
		private var m_deadLinkFolder:File			= null;	// our dead link file
		private var m_styleCodeType:String			= null;	// our style code type
		private var m_jekyllCodeType:String			= null; // our jekyll code type
		private var m_jekyllCodeLineNos:Boolean		= false;// should we use line numbers when generating jekyll code?
		private var m_quoteType:String				= null;	// the type of quotes that we want to use
		private var m_pages:Array/**String*/		= null;	// all the urls for pages, not posts
		private var m_tapirSecret:String			= null;	// the secret key for our tapir go search
		private var m_useSiteURL:Boolean			= false;// should we also use site.url as well as site.baseurl
		private var m_use302:Boolean				= false;// should we use 302 redirects instead of 301?
		
		/******************************************************************************/
		
		/**
		 * Do we currently need user input for one of our variables?
		 */
		public function get needsInput():Boolean
		{
			return ( this.m_xmlFolderPath == null ||
					 this.m_jekyllFolderPath == null ||
					 ( this.m_siteURL == null || this.m_siteURL == "" ) ||
					 ( this.m_siteFilesFolder == null || this.m_siteFilesFolder == "" ) ||
					 ( this.m_quoteType == null ));
		}
		
		/**
		 * The folder where we're going to load our xml files from
		 */
		public function get xmlFolder():File { return this.m_xmlFolder; }
		public function set xmlFolder( f:File ):void
		{
			this.m_xmlFolder 		= f;
			this.m_xmlFolderPath	= this.m_xmlFolder.url;
			this.m_lso.add( Settings.M_KEY_XML_FOLDER_PATH, this.m_xmlFolderPath, false, true );
		}
		
		/**
		 * The folder to save our jekyll files to
		 */
		public function get jekyllFolder():File { return this.m_jekyllFolder; }
		public function set jekyllFolder( f:File ):void
		{
			this.m_jekyllFolder 	= f;
			this.m_jekyllFolderPath	= this.m_jekyllFolder.url;
			this.m_lso.add( Settings.M_KEY_JEKYLL_FOLDER_PATH, this.m_jekyllFolderPath, false, true );
			this._getOrCreateSaveFolders(); // get the other save folders
		}
		
		/**
		 * The folder to save our output in
		 */
		public function get savePostsFolder():File { return this.m_savePostsFolder; }
		
		/**
		 * The folder to save our pages in
		 */
		public function get savePagesFolder():File { return this.m_savePagesFolder; }
		
		/**
		 * The folder to save our comments in
		 */
		public function get saveCommentsFolder():File { return this.m_saveCommentsFolder; }
		
		/**
		 * The folder to save our files in
		 */
		public function get saveFilesFolder():File { return this.m_saveFilesFolder; }
		
		/**
		 * The folder to save our open graph in
		 */
		public function get saveOpenGraphFolder():File { return this.m_saveOpenGraphFolder; }
		
		/**
		 * The root site url, with no trailing slash, e.g. "http://divillysausages.com"
		 */
		public function get siteURL():String { return this.m_siteURL; }
		public function set siteURL( s:String ):void
		{
			this.m_siteURL = s;
			this.m_lso.add( Settings.M_KEY_SITE_URL, this.m_siteURL, false, false ); // NOTE: don't save it all the time, as it's changing from a text input update
		}
		
		/**
		 * The folder where we can find our files
		 */
		public function get siteFilesFolder():String { return this.m_siteFilesFolder; }
		public function set siteFilesFolder( s:String ):void
		{
			this.m_siteFilesFolder = s;
			this.m_lso.add( Settings.M_KEY_SITE_FILES_FOLDER, this.m_siteFilesFolder, false, false ); // NOTE: don't save it all the time, as it's changing from a text input update
		}
		
		/**
		 * The file for our css
		 */
		public function get cssFile():File { return this.m_cssFile; }
		public function set cssFile( f:File ):void
		{
			this.m_cssFile	= f;
			this.m_cssPath	= this.m_cssFile.url;
			this.m_lso.add( Settings.M_KEY_CSS_PATH, this.m_cssPath, false, true );
		}
		
		/**
		 * The folder for our dead links file
		 */
		public function get deadLinkFolder():File { return this.m_deadLinkFolder; }
		public function set deadLinkFolder( f:File ):void
		{
			this.m_deadLinkFolder		= f;
			this.m_deadLinkFolderPath	= this.m_deadLinkFolder.url;
			this.m_lso.add( Settings.M_KEY_DEAD_LINK_FOLDER_PATH, this.m_deadLinkFolderPath, false, true );
		}
		
		/**
		 * The default code type to put for our <code> style
		 */
		public function get styleCodeType():String { return this.m_styleCodeType; }
		public function set styleCodeType( s:String ):void
		{
			this.m_styleCodeType = s;
			this.m_lso.add( Settings.M_KEY_STYLE_CODE_TYPE, this.m_styleCodeType, false, false ); // NOTE: don't save it all the time, as it's changing from a text input
		}
		
		/**
		 * The jekyll code type
		 */
		public function get jekyllCodeType():String { return this.m_jekyllCodeType; }
		public function set jekyllCodeType( s:String ):void
		{
			this.m_jekyllCodeType = s;
			this.m_lso.add( Settings.M_KEY_JEKYLL_CODE_TYPE, this.m_jekyllCodeType, false, false ); // NOTE: don't save it all the time, as it's changing from a text input
		}
		
		/**
		 * If generating jekyll code, should we include line nos?
		 */
		public function get jekyllCodeLineNos():Boolean { return this.m_jekyllCodeLineNos; }
		public function set jekyllCodeLineNos( b:Boolean ):void
		{
			this.m_jekyllCodeLineNos = b;
			this.m_lso.add( Settings.M_KEY_JEKYLL_CODE_LINENOS, ( ( this.m_jekyllCodeLineNos ) ? "true" : "false" ), false, true );
		}
		
		/**
		 * The type of quotes that we want to use (' or ")
		 */
		public function get quoteType():String { return this.m_quoteType; }
		public function set quoteType( s:String ):void
		{
			this.m_quoteType = ( s == "'" ) ? "'" : "\"";
			this.m_lso.add( Settings.M_QUOTE_TYPE, this.m_quoteType, false, false ); // NOTE: don't save it all the time, as it's changing from a text input
		}
		
		/**
		 * The list of urls to treat as pages, not posts
		 */
		public function get pages():Array/**String*/ { return this.m_pages; }
		public function set pages( a:Array/**String*/ ):void
		{
			this.m_pages = ( a == null || a.length == 0 ) ? null : a;
			if( this.m_pages != null )
				this.m_lso.add( Settings.M_PAGES, this.m_pages.join( "," ), false, false ); // NOTE: don't save it all the time, as it's changing from a text input
			else
				this.m_lso.remove( Settings.M_PAGES, true );
		}
		
		/**
		 * The secret for our tapir go api
		 */
		public function get tapirSecret():String { return this.m_tapirSecret; }
		public function set tapirSecret( s:String ):void
		{
			this.m_tapirSecret = StringUtil.trim( s );
			this.m_lso.add( Settings.M_KEY_TAPIR_SECRET, this.m_tapirSecret, false, true );
		}
		
		/**
		 * When we're replacing site.baseurl, should we also use site.url?
		 */
		public function get alsoUseSiteURL():Boolean { return this.m_useSiteURL; }
		public function set alsoUseSiteURL( b:Boolean ):void
		{
			this.m_useSiteURL = b;
			this.m_lso.add( Settings.M_KEY_ALSO_USE_SITE_URL, ( ( this.m_useSiteURL ) ? "true" : "false" ), false, true );
		}
		
		/**
		 * Should we use 302 redirects instead of 301
		 */
		public function get use302Redirects():Boolean { return this.m_use302; }
		public function set use302Redirects( b:Boolean ):void
		{
			this.m_use302 = b;
			this.m_lso.add( Settings.M_KEY_USE_302, ( ( this.m_use302 ) ? "true" : "false" ), false, true );
		}
		
		/******************************************************************************/
		
		public function Settings() 
		{
			this.m_lso					= new LSO( Settings.M_LSO_NAME );
			this.m_siteURL				= this.m_lso.get( Settings.M_KEY_SITE_URL ) as String;
			this.m_siteFilesFolder		= this.m_lso.get( Settings.M_KEY_SITE_FILES_FOLDER ) as String;
			this.m_xmlFolderPath		= this.m_lso.get( Settings.M_KEY_XML_FOLDER_PATH ) as String;
			this.m_jekyllFolderPath		= this.m_lso.get( Settings.M_KEY_JEKYLL_FOLDER_PATH ) as String;
			this.m_cssPath				= this.m_lso.get( Settings.M_KEY_CSS_PATH ) as String;
			this.m_deadLinkFolderPath	= this.m_lso.get( Settings.M_KEY_DEAD_LINK_FOLDER_PATH ) as String;
			this.m_styleCodeType		= this.m_lso.get( Settings.M_KEY_STYLE_CODE_TYPE ) as String;
			this.m_jekyllCodeType		= this.m_lso.get( Settings.M_KEY_JEKYLL_CODE_TYPE ) as String;
			var lineNos:String			= this.m_lso.get( Settings.M_KEY_JEKYLL_CODE_LINENOS ) as String;
			this.m_quoteType			= this.m_lso.get( Settings.M_QUOTE_TYPE ) as String;
			var pages:String			= this.m_lso.get( Settings.M_PAGES ) as String;
			this.m_tapirSecret			= this.m_lso.get( Settings.M_KEY_TAPIR_SECRET ) as String;
			var useSiteURL:String		= this.m_lso.get( Settings.M_KEY_ALSO_USE_SITE_URL ) as String;
			var use302:String			= this.m_lso.get( Settings.M_KEY_USE_302 ) as String;
			
			// create any file objects
			if ( this.m_xmlFolderPath != null && StringUtil.trim( this.m_xmlFolderPath ) != "" )
				this.m_xmlFolder = new File( this.m_xmlFolderPath );
			if ( this.m_jekyllFolderPath != null && StringUtil.trim( this.m_jekyllFolderPath ) != "" )
				this.m_jekyllFolder = new File( this.m_jekyllFolderPath );
			if ( this.m_cssPath != null && StringUtil.trim( this.m_cssPath ) != "" )
				this.cssFile = new File( this.m_cssPath );
			if ( this.m_deadLinkFolderPath != null && StringUtil.trim( this.m_deadLinkFolderPath ) != "" )
				this.m_deadLinkFolder = new File( this.m_deadLinkFolderPath );
			if ( this.m_styleCodeType == null )
				this.m_styleCodeType = "language-actionscript";
			if ( this.m_jekyllCodeType == null )
				this.m_jekyllCodeType = "as3";
			this.m_jekyllCodeLineNos	= ( lineNos == "true" );
			this.m_quoteType 			= ( this.m_quoteType == "'" ) ? "'" : "\"";
			this.m_useSiteURL			= ( useSiteURL == "true" );
			this.m_use302				= ( use302 == "true" );
			
			// set/create our different files
			if ( this.m_jekyllFolder != null )
				this._getOrCreateSaveFolders();
			
			// get our pages
			this.m_pages = ( pages != null ) ? pages.split( "," ) : [];
			for ( var i:int = this.m_pages.length - 1; i >= 0; i-- )
			{
				this.m_pages[i] = StringUtil.trim( this.m_pages[i] );
				if ( this.m_pages[i] == null || this.m_pages[i] == "null" || this.m_pages[i].length == 0 )
					this.m_pages.splice( i, 1 );
			}
		}
		
		// gets or creates the different folders necessary
		private function _getOrCreateSaveFolders():void
		{
			if ( this.m_jekyllFolder == null )
				return;
				
			// set our file objects
			this.m_savePostsFolder 		= this.m_jekyllFolder.resolvePath( "_posts" );
			this.m_savePagesFolder		= this.m_jekyllFolder.resolvePath( "pages" );
			this.m_saveCommentsFolder	= this.m_jekyllFolder.resolvePath( "_includes/comments" );
			this.m_saveFilesFolder		= this.m_jekyllFolder.resolvePath( "_includes/files" );
			this.m_saveOpenGraphFolder	= this.m_jekyllFolder.resolvePath( "_includes/opengraph" );
			
			// create the folders if they don't exist
			if ( !this.m_savePostsFolder.exists )
				this.m_savePostsFolder.createDirectory();
			if ( !this.m_savePagesFolder.exists )
				this.m_savePagesFolder.createDirectory();
			if ( !this.m_saveCommentsFolder.exists )
				this.m_saveCommentsFolder.createDirectory();
			if ( !this.m_saveFilesFolder.exists )
				this.m_saveFilesFolder.createDirectory();
			if ( !this.m_saveOpenGraphFolder.exists )
				this.m_saveOpenGraphFolder.createDirectory();
		}
		
		/**
		 * Gets if we should use a specific controller type
		 * @param classname The simple classname of controller that we should use
		 * @return True if we should use this controller, false otherwise
		 */
		public function getUseController( classname:String ):Boolean
		{
			var shouldUse:String = this.m_lso.get( classname ) as String;
			return ( shouldUse == "false" ) ? false : true; // true is the default (i.e. if the classname doesn't exist)
		}
		
		/**
		 * Sets if we should use a specific controller type
		 * @param classname The simple classname of the controller that we should use
		 * @param shouldUse If we should use the controller
		 */
		public function setUseController( classname:String, shouldUse:Boolean ):void
		{
			this.m_lso.add( classname, ( shouldUse ) ? "true" : "false", false, false );
		}
		
	}

}