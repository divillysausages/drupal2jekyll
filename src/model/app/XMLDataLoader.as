package model.app 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import model.Comment;
	import model.File;
	import model.MenuLink;
	import model.Node;
	import model.OpenGraph;
	import model.Term;
	import model.TermNode;
	import model.Upload;
	import model.URLAlias;
	import org.osflash.signals.Signal;
	
	/**
	 * The class that we use to load all our data xml files
	 * @author Damian Connolly
	 */
	public class XMLDataLoader 
	{
		
		/******************************************************************************/
		
		/**
		 * The signal dispatched when our loading has finished. It should take no parameters
		 */
		public var signalOnFinished:Signal = null;
		
		/******************************************************************************/
		
		private var m_data:Vector.<XMLData>				= null; // the xml that we've loaded for different classes
		private var m_loaded:int						= 0;	// the number of xml that we've loaded
		private var m_currFile:flash.filesystem.File	= null;	// the current file that we're loading (so it doesn't get garbage collected)
		
		/******************************************************************************/
		
		/**
		 * Creates our new XMLDataLoader
		 */
		public function XMLDataLoader() 
		{
			// create our signal
			this.signalOnFinished = new Signal;
			
			// create our data vector
			this.m_data = new Vector.<XMLData>;
			this.m_data.push( new XMLData( Comment, "comments.xml" ) );
			this.m_data.push( new XMLData( model.File, "files.xml" ) );
			this.m_data.push( new XMLData( MenuLink, "menu_links.xml" ) );
			this.m_data.push( new XMLData( Node, "node_revisions.xml", "node.xml" ) ); // node_revisions is the actual content, node is the meta
			this.m_data.push( new XMLData( OpenGraph, "opengraph_meta.xml" ) );
			this.m_data.push( new XMLData( Term, "term_data.xml" ) );
			this.m_data.push( new XMLData( TermNode, "term_node.xml" ) );
			this.m_data.push( new XMLData( Upload, "upload.xml" ) );
			this.m_data.push( new XMLData( URLAlias, "url_alias.xml" ) );
		}
		
		/**
		 * Gets the XML data for a specific class
		 * @param clazz The class that the data is for
		 * @return The XMLData for the class, or null if we don't have it loaded
		 */
		public function getData( clazz:Class ):XMLData 
		{
			for each( var xd:XMLData in this.m_data )
			{
				if ( xd.clazz == clazz )
					return xd;
			}
			return null;
		}
		
		/**
		 * Clears any previously loaded data and loads in new data fresh. Listen to 
		 * signalOnFinished to know when it's done
		 */
		public function loadData():void
		{
			( DS.main as Main ).log( Log.LOG, "Loading XML files" );
			
			// first, reset all our previous data
			for each( var xd:XMLData in this.m_data )
				xd.clear();
				
			// start loading in our xml
			this.m_loaded	= -1;
			this.m_currFile	= null;
			this._loadNext();
		}
		
		/******************************************************************************/
		
		// loads the next XML in line
		private function _loadNext():void
		{
			// if we already have a file, check if we loaded the first file, and there's a second to load
			var shouldLoadFilename2:Boolean = ( this.m_currFile != null && this.m_currFile.name == this.m_data[this.m_loaded].filename1 && this.m_data[this.m_loaded].filename2 != null );
			
			// update our loaded number if necessary
			if( !shouldLoadFilename2 )
				this.m_loaded++;
			
			// check to see if we've finished
			if ( this.m_loaded >= this.m_data.length )
			{
				this.m_currFile = null;
				( DS.main as Main ).log( Log.LOG, "All XML files loaded" );
				this.signalOnFinished.dispatch();
				return;
			}
				
			// get the name of the file to load
			var filename:String = ( shouldLoadFilename2 ) ? this.m_data[this.m_loaded].filename2 : this.m_data[this.m_loaded].filename1;
			
			// create our file object to load our next xml in line
			var file:flash.filesystem.File = ( DS.main as Main ).settings.xmlFolder.resolvePath( filename );
			file.addEventListener( Event.COMPLETE, this._onComplete );
			file.addEventListener( IOErrorEvent.IO_ERROR, this._cleanUpAndMoveOn );
			file.addEventListener( SecurityErrorEvent.SECURITY_ERROR, this._cleanUpAndMoveOn );
			file.load();
			this.m_currFile = file; // so it doesn't get garbage collected
		}
		
		// called when we complete a load
		private function _onComplete( e:Event ):void
		{
			var file:flash.filesystem.File = ( e.target as flash.filesystem.File );
			
			// get the xml from the file
			var xml:XML = new XML( file.data );
			
			// get our XMLData and store our xml on it
			var xd:XMLData = this.m_data[this.m_loaded];
			if ( file.name == xd.filename1 )
				xd.data1 = xml;
			else
				xd.data2 = xml;
			
			// clean up and load the next one
			this._cleanUpAndMoveOn( e );
		}
		
		// cleans up the file object
		private function _cleanUpAndMoveOn( e:Event ):void
		{
			// check if it was an error
			if ( e is IOErrorEvent )
			{
				var ioe:IOErrorEvent = ( e as IOErrorEvent );
				( DS.main as Main ).log( Log.ERROR, "IO Error when loading '" + this.m_data[this.m_loaded].filename1 + "': " + ioe.errorID + ": " + ioe.text );
			}
			else if ( e is SecurityErrorEvent )
			{
				var se:SecurityErrorEvent = ( e as SecurityErrorEvent );
				( DS.main as Main ).log( Log.ERROR, "Security Error when loading '" + this.m_data[this.m_loaded].filename1 + "': " + se.errorID + ": " + se.text );
			}
			
			// get our file object
			var file:flash.filesystem.File = ( e.target as flash.filesystem.File );
			
			// this can get called from an error, so we remove the SELECT event for all possible events
			file.removeEventListener( Event.COMPLETE, this._onComplete );
			file.removeEventListener( IOErrorEvent.IO_ERROR, this._cleanUpAndMoveOn );
			file.removeEventListener( SecurityErrorEvent.SECURITY_ERROR, this._cleanUpAndMoveOn );
			
			// load the next one in line
			this._loadNext();
		}
		
	}

}