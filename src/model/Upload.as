package model
{
	import com.divillysausages.ds.DS;
	import model.app.XMLData;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class Upload 
	{
		/******************************************************************************/
		
		public static var uploads:Vector.<Upload> = null;
		
		/******************************************************************************/
		
		public static function parseXML():void
		{
			// get our xml data
			var xmlData:XMLData	= ( DS.main as Main ).xmlDataLoader.getData( Upload );
			var xml:XML			= ( xmlData != null ) ? xmlData.data1 : null;
			if ( xml == null )
			{
				Upload.uploads = new Vector.<Upload>( 0, true );
				return;
			}
				
			// parse our xml
			Upload.uploads 	= new Vector.<Upload>( xml.database.table.length(), true );
			var i:int		= 0;
			for each( var ux:XML in xml.database.table )
			{
				var upload:Upload = new Upload;
				for each( var cx:XML in ux.column )
				{
					// file id
					if ( cx.@name == "fid" )
						upload.fid = int( cx.toString() );
						
					// node id
					if ( cx.@name == "nid" )
						upload.nid = cx.toString();
						
					// description
					if ( cx.@name == "description" )
						upload.description = cx.toString();
				}
				
				// add it
				Upload.uploads[i++] = upload;
			}
		}
		
		public static function getForNode( nid:int ):Vector.<Upload>
		{
			var v:Vector.<Upload> = new Vector.<Upload>;
			for each( var u:Upload in Upload.uploads )
			{
				if ( u.nid == nid )
					v.push( u );
			}
			return v;
		}
		
		/******************************************************************************/
		
		public var fid:int 				= 0;
		public var nid:int 				= 0;
		public var description:String 	= null;
		
		/******************************************************************************/
		
		public function get file():File { return File.get( this.fid ); }
		public function get node():Node { return Node.get( this.nid ); }
		
		/******************************************************************************/
		
		public function toString():String
		{
			return "[Upload nid:" + this.nid + ", fid: " + this.fid + ", description:" + this.description + "]";
		}
		
	}

}