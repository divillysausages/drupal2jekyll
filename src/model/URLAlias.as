package model
{
	import com.divillysausages.ds.DS;
	import model.app.XMLData;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class URLAlias 
	{
		/******************************************************************************/
		
		public static var aliases:Vector.<URLAlias> = null;
		
		/******************************************************************************/
		
		public static function parseXML():void
		{
			// get our xml data
			var xmlData:XMLData = ( DS.main as Main ).xmlDataLoader.getData( URLAlias );
			var xml:XML			= ( xmlData != null ) ? xmlData.data1 : null;
			if ( xml == null )
			{
				URLAlias.aliases = new Vector.<URLAlias>( 0, true );
				return;
			}
				
			// parse our xml
			URLAlias.aliases	= new Vector.<URLAlias>( xml.database.table.length(), true );
			var i:int			= 0;
			for each( var ux:XML in xml.database.table )
			{
				var alias:URLAlias = new URLAlias;
				for each( var cx:XML in ux.column )
				{
					// pid
					if ( cx.@name == "pid" )
						alias.pid = int( cx.toString() );
						
					// source
					if ( cx.@name == "src" )
						alias.src = cx.toString();
						
					// destination
					if ( cx.@name == "dst" )
						alias.dst = cx.toString();
				}
				
				// set our node id (NOTE: ignore taxonomy links, otherwise we'll get the wrong nid)
				if ( alias.src != null && alias.src.indexOf( "taxonomy" ) == -1 )
					alias.nid = int( alias.src.substr( alias.src.lastIndexOf( "/" ) + 1 ) );
				
				// add it
				URLAlias.aliases[i++] = alias;
			}
		}
		
		public static function get( pid:int ):URLAlias
		{
			for each( var a:URLAlias in URLAlias.aliases )
			{
				if ( a.pid == pid )
					return a;
			}
			return null;
		}
		
		public static function getForNID( nid:int ):URLAlias
		{
			for each( var a:URLAlias in URLAlias.aliases )
			{
				if ( a.nid == nid )
					return a;
			}
			return null;
		}
		
		/******************************************************************************/
		
		public var pid:int 		= 0;
		public var nid:int		= -1;	// the id for the node that this is for
		public var src:String	= null;
		public var dst:String	= null;
		
		/******************************************************************************/
		
		public function get node():Node
		{
			if ( this.nid == -1 )
				return null;
			return Node.get( this.nid );
		}
		
		/******************************************************************************/
		
		public function toString():String
		{
			return "[URLAlias src:" + this.src + ", dst:" + this.dst + ", nid: " + this.nid + "]";
		}
	}

}