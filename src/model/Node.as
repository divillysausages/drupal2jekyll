package model
{
	import com.divillysausages.ds.DS;
	import model.app.XMLData;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class Node 
	{
		/******************************************************************************/
		
		public static var nodes:Vector.<Node> = null;
		
		/******************************************************************************/
		
		public static function parseXML():void
		{
			// get our xml data
			var xmlData:XMLData = ( DS.main as Main ).xmlDataLoader.getData( Node );
			var xml:XML			= ( xmlData != null ) ? xmlData.data1 : null; // our actual content
			var xml2:XML		= ( xmlData != null ) ? xmlData.data2 : null; // our created at date etc
			if ( xml == null || xml2 == null )
			{
				Node.nodes = new Vector.<Node>( 0, true );
				return;
			}
			
			// parse our xml
			Node.nodes	= new Vector.<Node>( xml.database.table.length(), true );
			var i:int	= 0;
			for each( var nx:XML in xml.database.table )
			{
				var node:Node = new Node;
				for each( var cx:XML in nx.column )
				{
					// node id
					if ( cx.@name == "nid" )
						node.nid = int( cx.toString() );
						
					// title
					if ( cx.@name == "title" )
						node.title = cx.toString();
						
					// body
					if ( cx.@name == "body" )
						node.body = cx.toString();
				}
				
				// add it
				Node.nodes[i++] = node;
			}
			
			// go through the node xml and get the created timestamp
			for each( nx in xml2.database.table )
			{
				var nid:int = ( nx.column.(@name == "nid") != undefined ) ? int( nx.column.(@name == "nid").toString() ) : -1;
				if ( nid == -1 )
					DS.error( Node, "Can't find the node ID for some XML: " + nx.toXMLString() );
				else
				{
					// find our node
					node = Node.get( nid );
					if ( node == null )
						DS.error( Node, "Can't get the Node object for nid " + nid );
					else
					{
						// get our timestamp
						var created:Number = ( nx.column.(@name == "created" ) != undefined ) ? Number( nx.column.(@name == "created").toString() ) : -1.0;
						if ( isNaN( created ) || created == -1 )
							DS.error( Node, "Can't find the created timestamp for node " + node );
						else
						{
							node.timestamp 		= new Date;
							node.timestamp.time	= created * 1000; // created is in seconds
						}
					}
				}
			}
		}
		
		public static function get( nid:int ):Node
		{
			for each( var n:Node in Node.nodes )
			{
				if ( n.nid == nid )
					return n;
			}
			return null;
		}
		
		/******************************************************************************/
		
		public var nid:int 			= 0;
		public var title:String 	= null;
		public var body:String 		= null;
		public var timestamp:Date 	= null;
		
		/******************************************************************************/
		
		public function toString():String
		{
			return "[Node nid:" + this.nid + ", title: " + this.title + ", date: " + this.timestamp + "]";
		}
		
	}

}