package model
{
	import com.divillysausages.ds.DS;
	import model.app.XMLData;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class Term 
	{
		/******************************************************************************/
		
		public static var terms:Vector.<Term> = null;
		
		/******************************************************************************/
		
		public static function parseXML():void
		{
			// get our xml data
			var xmlData:XMLData = ( DS.main as Main ).xmlDataLoader.getData( Term );
			var xml:XML			= ( xmlData != null ) ? xmlData.data1 : null;
			if ( xml == null )
			{
				Term.terms = new Vector.<Term>( 0, true );
				return;
			}
				
			// parse our xml
			Term.terms	= new Vector.<Term>( xml.database.table.length(), true );
			var i:int	= 0;
			for each( var tx:XML in xml.database.table )
			{
				var term:Term = new Term;
				for each( var cx:XML in tx.column )
				{
					// term id
					if ( cx.@name == "tid" )
						term.tid = int( cx.toString() );
						
					// name
					if ( cx.@name == "name" )
						term.name = cx.toString();
				}
				
				// add it
				Term.terms[i++] = term;
			}
		}
		
		public static function get( tid:int ):Term
		{
			for each( var t:Term in Term.terms )
			{
				if ( t.tid == tid )
					return t;
			}
			return null;
		}
		
		/******************************************************************************/
		
		public var tid:int 		= 0;
		public var name:String 	= null;
		
		/******************************************************************************/
		
		public function toString():String
		{
			return "[Term tid:" + this.tid + ", name: " + this.name + "]";
		}
		
	}

}