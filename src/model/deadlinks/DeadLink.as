package model.deadlinks 
{
	/**
	 * The class that represents a link in our dead links file
	 * @author Damian Connolly
	 */
	public class DeadLink 
	{
	
		/******************************************************************************/
		
		private static var m_reDeadLinkURL:RegExp = / +\|\|\| +/g; // the regexp we use for splitting lines
		
		/******************************************************************************/
		
		/**
		 * Splits a line from our dead link file, returning a DeadLink
		 * @param line The line that we're going to split
		 * @return A DeadLink
		 */
		public static function splitLine( line:String ):DeadLink 
		{
			var segments:Array/**String*/ = line.split( DeadLink.m_reDeadLinkURL );
			
			// create our dead link
			var dl:DeadLink = new DeadLink;
			dl.oldURL		= segments[0];
			
			// get the code
			var index:int 	= segments[1].indexOf( "(" );
			dl.statusCode	= int( segments[1].substring( index + 1, segments[1].indexOf( ")", index ) ) );
			
			// get the page title
			index 			= segments[1].indexOf( "'" );
			dl.pageTitle	= segments[1].substring( index + 1, segments[1].lastIndexOf( "'" ) );
			
			// should be exactly 3 segments if it's been filled in
			if ( segments.length == 3 && segments[2].length > 0 )
				dl.newURL = segments[2];
			return dl;
		}
		
		/******************************************************************************/
		
		/**
		 * The old url, that doesn't work
		 */
		public var oldURL:String = null;
		
		/**
		 * The new url, that we've loaded from our file
		 */
		public var newURL:String = null;
		
		/**
		 * The status code that was returned for this url
		 */
		public var statusCode:int = 0;
		
		/**
		 * The page title for the first occurance of the old url
		 */
		public var pageTitle:String = null;
		
		/******************************************************************************/
		
		/**
		 * Returns if this DeadLink has a replacement or not
		 */
		public function get hasReplacement():Boolean { return ( this.newURL != null ); }
		
		/******************************************************************************/
		
		/**
		 * Creates a new DeadLink
		 * @param url The old url, that we have a problem with
		 * @param status The status code for this url
		 * @param pageTitle The page title that we first found this url on
		 */
		public function DeadLink( url:String = null, status:int = 0, pageTitle:String = null )
		{
			this.oldURL 	= url;
			this.statusCode	= status;
			this.pageTitle	= pageTitle;
		}
		
		/**
		 * Converts this DeadLink to the string that we're going to save in our file
		 */
		public function toFileString():String
		{
			return this.oldURL + " ||| (" + this.statusCode + ") '" + this.pageTitle + "' ||| " + ( ( this.newURL != null ) ? this.newURL : "" ); 
		}
		
	}

}