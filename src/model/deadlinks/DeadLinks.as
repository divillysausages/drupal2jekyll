package model.deadlinks 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.Dictionary;
	import org.osflash.signals.Signal;
	
	/**
	 * The class that handles loading the dead links file
	 * @author Damian Connolly
	 */
	public class DeadLinks 
	{
		
		/******************************************************************************/
		
		private static const M_FILE_NAME:String = "deadLinks.txt"; // the name of the file that we're loading
		
		/******************************************************************************/
		
		private static var m_instance:DeadLinks = null; // the singleton instance of this class
		private static var m_creating:Boolean	= false;// are we creating this class
		
		/******************************************************************************/
		
		/**
		 * Returns the singleton instance of this class
		 */
		public static function get instance():DeadLinks
		{
			if ( DeadLinks.m_instance == null )
			{
				DeadLinks.m_creating = true;
				DeadLinks.m_instance = new DeadLinks;
				DeadLinks.m_creating = false;
			}
			return DeadLinks.m_instance;
		}
		
		/******************************************************************************/
		
		/**
		 * The signal fired when we're ready to go. It should take no parameters
		 */
		public var signalOnReady:Signal = null;
		
		/******************************************************************************/
		
		private var m_unreplacedDeadLinks:Vector.<DeadLink> = null;	// any new dead links that we have
		private var m_replacedDeadLinks:Vector.<DeadLink>	= null;	// replaced dead links from our file
		private var m_deadLinks:Dictionary					= null; // our key/DeadLink dictionary for quick recovery
		private var m_hasLoaded:Boolean						= false;// have we loaded our file yet?
		private var m_file:File								= null; // for some reason, for loading, we need to keep a reference to the file
		
		/******************************************************************************/
		
		/**
		 * Returns all the dead links in our site that don't have a replacement
		 */
		public function get unreplacedDeadLinks():Vector.<DeadLink> { return this.m_unreplacedDeadLinks; }
		
		/**
		 * Returns all the dead links that can be replaced
		 */
		public function get allReplaceableDeadLinks():Vector.<DeadLink> { return this.m_replacedDeadLinks; }
		
		/******************************************************************************/
		
		/**
		 * Creates a new DeadLinks class
		 */
		public function DeadLinks() 
		{
			if ( !DeadLinks.m_creating )
				throw new Error( "DeadLinks is a singleton" );
				
			this.signalOnReady			= new Signal;
			this.m_unreplacedDeadLinks	= new Vector.<DeadLink>;
			this.m_replacedDeadLinks	= new Vector.<DeadLink>;
			this.m_deadLinks			= new Dictionary;
		}
		
		/**
		 * Clears the DeadLinks class
		 */
		public function reset():void
		{
			this.m_unreplacedDeadLinks.length	= 0;
			this.m_replacedDeadLinks.length		= 0;
			for ( var key:String in this.m_deadLinks )
			{
				this.m_deadLinks[key] = null;
				delete this.m_deadLinks[key];
			}
			this.m_hasLoaded = false;
			if ( this.m_file != null )
			{
				this._cleanFile( this.m_file );
				this.m_file = null;
			}
		}
		
		/**
		 * Loads the file that we're going to use
		 * @param onReady The callback to call when we're ready
		 */
		public function loadFile( onReady:Function = null ):void
		{
			// if we're already loaded, just return
			if ( this.m_hasLoaded )
			{
				if ( onReady != null )
					onReady();
				return;
			}
			this.m_hasLoaded = true;
			if ( onReady != null )
				this.signalOnReady.addOnce( onReady );
				
			// get our file object, and load it
			var deadLinkFolder:File	= ( DS.main as Main ).settings.deadLinkFolder;
			this.m_file 			= ( deadLinkFolder != null ) ? deadLinkFolder.resolvePath( DeadLinks.M_FILE_NAME ) : null;
			if ( this.m_file != null && this.m_file.exists )
			{
				this.m_file.addEventListener( Event.COMPLETE, this._onDeadLinksLoad );
				this.m_file.addEventListener( IOErrorEvent.IO_ERROR, this._onDeadLinksFail );
				try { this.m_file.load(); }
				catch ( e:Error )
				{
					( DS.main as Main ).log( Log.ERROR, "An error occurred when loading our dead links file: " + e.errorID + ": " + e.name + ": " + e.message );
					this._cleanFile( this.m_file );
				}
			}
			else
				this.signalOnReady.dispatch();
		}
		
		/**
		 * Saves our dead links file
		 */
		public function saveFile():void
		{	
			// make sure we have our folder
			if ( ( DS.main as Main ).settings.deadLinkFolder == null )
			{
				( DS.main as Main ).log( Log.ERROR, "Can't save our dead links file as the dead links folder hasn't been set in the Settings" );
				return;
			}
			
			// sort our links so those we need to find replacements for are at the front
			this.m_unreplacedDeadLinks.sort( this._sort );
			this.m_replacedDeadLinks.sort( this._sort );
			
			// save our xml
			var str:String 	= "###########################################################################################\r\n";
			str				+= "# For automatic dead link substitution, add in new urls after the last ' ||| ' on each line.\r\n";
			str				+= "# e.g. http://oldURL.com ||| (404) Some title ||| http://newURL.com\r\n";
			str				+= "# Please use absolute urls\r\n";
			str				+= "###########################################################################################\r\n";
			for each( var dl:DeadLink in this.m_unreplacedDeadLinks )
				str += dl.toFileString() + "\r\n";
			for each( dl in this.m_replacedDeadLinks )
				str += dl.toFileString() + "\r\n";
			
			// get the file were we're going to save it
			var file:File = ( DS.main as Main ).settings.deadLinkFolder.resolvePath( DeadLinks.M_FILE_NAME );
			
			// save it
			var fs:FileStream = new FileStream;
			fs.open( file, FileMode.WRITE );
			fs.writeUTFBytes( str );
			fs.close();
			( DS.main as Main ).log( Log.DEBUG, "Saved deadLinks data under the path '" + file.nativePath + "' - add new urls after the last ' ||| ' for automatic substitution next time" );
		}
		
		/**
		 * Returns if we have a dead links saved for a particular url
		 * NOTE: this doesn't guarantee that it can be replaced
		 * @param url The old url
		 * @param alsoNeedsReplacement If we have this url, do we care if it has a replacement or not
		 * @return True if we have it, false otherwise
		 */
		public function hasDeadLink( url:String, alsoNeedsReplacement:Boolean ):Boolean
		{
			var dl:DeadLink = this.m_deadLinks[url];
			if ( dl == null )
				return false;
			return ( !alsoNeedsReplacement || ( alsoNeedsReplacement && dl.newURL != null ) );
		}
		
		/**
		 * Adds a dead url to our lists
		 * @param url The url that was marked as dead
		 * @param statusCode The status code returned for this url
		 * @param title The title of the page that this url first appeared in
		 */
		public function addDeadLink( url:String, statusCode:int, title:String ):void
		{
			// check that it's not already there
			if ( url in this.m_deadLinks )
				return;
			var dl:DeadLink 			= new DeadLink( url, statusCode, title );
			this.m_deadLinks[dl.oldURL]	= dl;
			this.m_unreplacedDeadLinks.push( dl );
		}
		
		/**
		 * Gets the replacement url for our dead link
		 * @param url The url that we were looking for
		 * @return The replacement url, or null if we don't have one
		 */
		public function getDeadLinkReplacement( url:String ):String
		{
			var dl:DeadLink = this.m_deadLinks[url];
			return ( dl != null ) ? dl.newURL : null; // newURL will be null if we don't have a replacement saved
		}
		
		/******************************************************************************/
		
		// called when we've got our dead links file
		private function _onDeadLinksLoad( e:Event ):void
		{
			var file:File 	= ( e.target as File );
			var str:String 	= file.data.readUTFBytes( file.data.length );
			
			// break into links
			var lines:Array = str.split( "\r\n" );
			var count:int	= 0;
			for each( var line:String in lines )
			{
				if ( line.length == 0 || line.charAt( 0 ) == "#" ) // ignore empty lines and comments
					continue;
					
				// check if our dead link has been filled in
				var dl:DeadLink 			= DeadLink.splitLine( line );
				this.m_deadLinks[dl.oldURL] = dl;
				if ( dl.hasReplacement )
				{
					this.m_replacedDeadLinks.push( dl );
					count++;
				}
				else
					this.m_unreplacedDeadLinks.push( dl );
			}
			
			// clean our file so we can move on
			this._cleanFile( file );
		}
		
		// called when there was an error loading the dead links file
		private function _onDeadLinksFail( e:Event ):void
		{
			( DS.main as Main ).log( Log.ERROR, "An error event occurred when loading our dead links file: " + e.type )
			this._cleanFile( ( e.target as File ) );
		}
		
		// cleans up a file
		private function _cleanFile( file:File ):void
		{
			file.removeEventListener( Event.COMPLETE, this._onDeadLinksLoad );
			file.removeEventListener( IOErrorEvent.IO_ERROR, this._onDeadLinksFail );
			this.m_file = null;
			this.signalOnReady.dispatch();
		}
		
		// the function used to sort our dead links
		private function _sort( a:DeadLink, b:DeadLink ):int
		{
			// we want them sorted alphabetically, but with the ones we haven't set first
			if ( a.newURL == null && b.newURL != null )
				return -1;
			if ( a.newURL != null && b.newURL == null )
				return 1;
			return ( a.oldURL < b.oldURL ) ? -1 : 1;
		}
		
	}

}