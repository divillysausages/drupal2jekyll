package model
{
	import com.divillysausages.ds.DS;
	import flash.utils.Dictionary;
	import model.app.XMLData;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class Comment 
	{
		/******************************************************************************/
		
		public static var comments:Vector.<Comment> 	= null;
		public static var commentsPerNode:Dictionary	= null; // all the comments for a specific node
		
		/******************************************************************************/
		
		public static function parseXML():void
		{
			// destroy any previous ones
			if ( Comment.comments != null )
			{
				for each( var old:Comment in Comment.comments )
					old.destroy();
			}
			if ( Comment.commentsPerNode != null )
			{
				for ( var oldKey:* in Comment.commentsPerNode )
				{
					Comment.commentsPerNode[oldKey] = null;
					delete Comment.commentsPerNode[oldKey];
				}
			}
			
			// get our xml data
			var xmlData:XMLData = ( DS.main as Main ).xmlDataLoader.getData( Comment );
			var xml:XML			= ( xmlData != null ) ? xmlData.data1 : null;
			if ( xml == null )
			{
				Comment.comments 		= new Vector.<Comment>( 0, true );
				Comment.commentsPerNode	= new Dictionary;
				return;
			}
			
			// parse our xml
			Comment.comments		= new Vector.<Comment>( xml.database.table.length(), true );
			Comment.commentsPerNode	= new Dictionary;
			var i:int				= 0;
			for each( var nx:XML in xml.database.table )
			{
				var comment:Comment = new Comment;
				for each( var cx:XML in nx.column )
				{
					// comment id
					if ( cx.@name == "cid" )
						comment.cid = int( cx.toString() );
						
					// parent id
					if ( cx.@name == "pid" )
						comment.pid = int( cx.toString() );
						
					// node id
					if ( cx.@name == "nid" )
						comment.nid = int( cx.toString() );
						
					// comment
					if ( cx.@name == "comment" )
						comment.comment = cx.toString();
						
					// timestamp
					if ( cx.@name == "timestamp" )
					{
						comment.timestamp 		= new Date;
						var n:Number			= Number( cx.toString() );
						comment.timestamp.time	= n * 1000;
					}
					
					// name
					if ( cx.@name == "name" )
						comment.name = cx.toString();
						
					// mail
					if ( cx.@name == "mail" )
					{
						comment.mail = cx.toString();
						if ( comment.mail == "" )
							comment.mail = null;
					}
						
					// homepage
					if ( cx.@name == "homepage" )
					{
						comment.homepage = cx.toString(); 
						if ( comment.homepage == "" )
							comment.homepage = null;
					}
				}
				
				// add it
				Comment.comments[i++] = comment;
			}
			
			// go through and add all our comments to their parent
			var len:int = Comment.comments.length;
			for ( i = 0; i < len; i++ )
			{
				var c:Comment = Comment.comments[i];
				if ( c.pid > 0 )
				{
					// it has a parent
					var pc:Comment = Comment.get( c.pid );
					if ( pc != null )
						pc.children.push( c );
					else
						DS.error( Comment, "Can't get the parent comment for " + c + ", pid: " + c.pid );
				}
				else
				{
					// it doesn't have a parent, so try and get the vector for it's node
					var v:Vector.<Comment> = Comment.commentsPerNode[c.nid];
					if ( v == null )
					{
						v 								= new Vector.<Comment>;
						Comment.commentsPerNode[c.nid]	= v;
					}
					v.push( c );
				}
			}
			
			// now sort all our comments
			for ( i = 0; i < len; i++ )
			{
				if ( Comment.comments[i].children.length > 0 )
					Comment.comments[i].children.sort( Comment._sort );
			}
			for ( var key:* in Comment.commentsPerNode )
				Comment.commentsPerNode[key].sort( Comment._sort );
		}
		
		public static function get( cid:int ):Comment
		{
			for each( var c:Comment in Comment.comments )
			{
				if ( c.cid == cid )
					return c;
			}
			return null;
		}
		
		public static function getForNode( nid:int ):Vector.<Comment>
		{
			return Comment.commentsPerNode[nid];
		}
		
		/******************************************************************************/
		
		// the function we use to sort our comments
		private static function _sort( a:Comment, b:Comment ):int
		{
			return a.cid - b.cid;
		}
		
		/******************************************************************************/
		
		/**
		 * Any children that we have
		 */
		public var children:Vector.<Comment> = new Vector.<Comment>;
		
		public var cid:int			= 0;
		public var pid:int			= -1;
		public var nid:int			= -1;
		public var comment:String	= null;
		public var timestamp:Date	= null;
		public var name:String		= null;
		public var mail:String		= null;
		public var homepage:String	= null;
		
		/******************************************************************************/
		
		public function get parent():Comment { return Comment.get( this.pid ); }
		public function get node():Node { return Node.get( this.nid ); }
		
		/******************************************************************************/
		
		public function destroy():void
		{
			this.children = null;
		}
		
		public function toString():String 
		{
			var s:String = "[Comment cid:" + this.cid + ", name: " + this.name + ", comment: '" + this.comment + "'";
			if ( this.children.length > 0 )
			{
				s += "\n";
				for each( var child:Comment in this.children )
					s += "\t" + child + "\n";
				s += "]";
			}
			else
				s += "]";
			return s;
			//return "[Comment cid:" + this.cid + ", name: " + this.name + ", comment: " + this.comment + "]";
		}
		
	}

}