package model.output 
{
	import com.divillysausages.ds.util.ObjectUtils;
	import flash.globalization.DateTimeFormatter;
	import flash.globalization.LocaleID;
	
	/**
	 * FrontMatter is included on every page, and basically provides descriptive info for when we're generating
	 * @author Damian Connolly
	 */
	public class FrontMatter 
	{
		
		/******************************************************************************/
		
		private static var m_dateFormatter:DateTimeFormatter 	= null; // the formatter for setting the date
		private static var m_timezoneOffset:String				= null;	// the offset for our timezone
		
		/******************************************************************************/
		
		private var m_data:Object = null; // the data for this front matter
		
		/******************************************************************************/
		
		/**
		 * The type of layout to use - this determines the template to use from the _layouts folder
		 */
		public function set layout( s:String ):void
		{
			this.add( "layout", s );
		}
		
		/**
		 * If you need your processed blog post URLs to be something other than the default 
		 * /year/month/day/title.html then you can set this variable and it will be used as the final URL.
		 */
		public function get permalink():String { return this.m_data["permalink"]; }
		public function set permalink( s:String ):void
		{
			this.add( "permalink", s );
		}
		
		/**
		 * The title for the page
		 */
		public function set title( s:String ):void
		{
			this.add( "title", "\"" + s + "\"" ); // quote the title in case it has any ":" or "-" chars
		}
		
		/**
		 * The date to use for the publication of this post
		 */
		public function set date( d:Date ):void
		{
			this.add( "date", FrontMatter.m_dateFormatter.format( d ) + " " + FrontMatter.m_timezoneOffset );
		}
		
		/**
		 * Any tags for the post
		 */
		public function set tags( a:Array /**String*/ ):void
		{
			this.add( "tags", "[" + a + "]" );
		}
		
		/******************************************************************************/
		
		public function FrontMatter() 
		{
			this.m_data = new Object;
			if ( FrontMatter.m_dateFormatter == null )
			{
				FrontMatter.m_dateFormatter = new DateTimeFormatter( LocaleID.DEFAULT );
				FrontMatter.m_dateFormatter.setDateTimePattern( "yyyy-MM-dd HH:mm:ss" );
				
				// get our timezone offset
				var s:String 					= ( new Date() ).toTimeString();
				FrontMatter.m_timezoneOffset	= s.substr( -5 ); // just get the +/-XXXX part
			}
		}
		
		/**
		 * Adds a new value to the front matter. Any value already on this key is overridden
		 * @param key The key to add
		 * @param value The value for the key
		 */
		public function add( key:String, value:String ):void
		{
			this.m_data[key] = value;
		}
		
		/**
		 * Adds a tag for the page (NOTE: we're using tags, not categories, because categories
		 * affect the url)
		 * @param tag The tag that we want to add for the page
		 */
		public function addTag( tag:String ):void
		{
			if ( "tags" in this.m_data )
				this.m_data["tags"].push( tag );
			else
				this.m_data["tags"] = [tag];
		}
		
		/**
		 * Returns the front matter in the right format for the page
		 * @return
		 */
		public function toString():String
		{
			var s:String = "---\n";
			
			// sort the front matter alphabetically, otherwise it can be hard to see what's changed when we update, as
			// every file will be marked as having changed
			var keys:Array = ObjectUtils.keys( this.m_data );
			keys.sort();
			
			for each ( var key:String in keys )
			{
				if ( this.m_data[key] is Array )
					s += key + ": " + ( this.m_data[key] as Array ).join( " " ) + "\n"; // tags or categories
				else
					s += key + ": " + this.m_data[key] + "\n";
			}
			s += "---\n\n";
			return s;
		}
		
	}

}