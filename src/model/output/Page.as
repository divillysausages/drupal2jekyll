package model.output 
{
	import com.divillysausages.ds.DS;
	import flash.globalization.DateTimeFormatter;
	import flash.globalization.LocaleID;
	import model.Comment;
	import model.File;
	import model.OpenGraph;
	import mx.resources.Locale;
	import mx.utils.StringUtil;
	/**
	 * A page on the site, with all the references
	 * @author Damian Connolly
	 */
	public class Page 
	{
		
		/******************************************************************************/
		
		private static var m_reNewLine:RegExp 					= /\n/g; // the regexp we use for newlines when generating comments
		private static var m_timeTagFormatter:DateTimeFormatter	= null;	// the formatter for formatting time for the tag itself
		private static var m_months:Array						= ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
		
		/******************************************************************************/
		
		/**
		 * The front matter for the page, which is necessary if we're going to treat it
		 */
		public var frontMatter:FrontMatter = null;
		
		/**
		 * The node ID for this page
		 */
		public var nodeID:int = -1;
		
		/**
		 * Our old url, usually something like node/123
		 */
		public var urlOld:String = null;
		
		/**
		 * Our url alias, usually something like blog/foo_bar
		 */
		public var urlAlias:String = null;
		
		/**
		 * Our new url
		 */
		public var urlNew:String = null;
		
		/**
		 * Our filename, for jekyll, usually something like 2013-06-01-Foo-bar.html
		 */
		public var filename:String = null;
		
		/**
		 * Does this post have PHP code in it (and will thus need to be changed)?
		 */
		public var hasPHP:Boolean = false;
		
		/**
		 * The title for the page
		 */
		public var title:String = null;
		
		/**
		 * The content for this page
		 */
		public var content:String = null;
		
		/**
		 * Any files attached to this page
		 */
		public var files:Vector.<File> = null;
		
		/**
		 * Any comments associated with this page
		 */
		public var comments:Vector.<Comment> = null;
		
		/**
		 * Any open graph info associated with this page
		 */
		public var openGraph:OpenGraph = null;
		
		/**
		 * The date for this page
		 */
		public var timestamp:Date = null;
		
		/**
		 * Is this is the special tags page that lists all the tags for all the posts?
		 */
		public var isTagsPage:Boolean = false;
		
		/**
		 * Has this page been created by the app?
		 */
		public var isCreatedByApp:Boolean = false;
		
		/******************************************************************************/
		
		private var m_isPage:Boolean = false; // is this Page a page or a post?
		
		/******************************************************************************/
		
		/**
		 * Is this Page a page or a post?
		 */
		public function get isPage():Boolean { return this.m_isPage; }
		public function set isPage( b:Boolean ):void
		{
			this.m_isPage 			= b;
			this.frontMatter.layout	= ( this.m_isPage ) ? "page" : "post";
		}
		
		/******************************************************************************/
		
		public function Page() 
		{
			// create our front matter
			this.frontMatter = new FrontMatter;
			
			// we're a post by default
			this.isPage = false;
			
			// create any of our other objects
			this.files 		= new Vector.<File>;
			this.comments	= new Vector.<Comment>;
			
			// create our formatters
			if ( Page.m_timeTagFormatter == null )
			{
				// the tag is in the form "2014-05-08 19:00"
				Page.m_timeTagFormatter	= new DateTimeFormatter( LocaleID.DEFAULT );
				Page.m_timeTagFormatter.setDateTimePattern( "yyyy-MM-dd HH:mm" );
			}
		}
		
		/**
		 * Gets the comments for the page as a html string
		 * @return The comments as html, or null if we don't have any
		 */
		public function getCommentsStr():String
		{
			if ( this.comments == null || this.comments.length == 0 )
				return null;
				
			var qs:String = ( DS.main as Main ).settings.quoteType;
				
			var str:String = "\t\t\t<div id=" + qs + "comments-inner" + qs + ">\n";
			for each( var comment:Comment in this.comments )
				str += this._processComment( comment, "\t\t\t\t" );
			str += "\t\t\t</div>";
			return str;
		}
		
		/**
		 * Gets the files for the page, as a html string
		 * @return The files as html, or null if we don't have any
		 */
		public function getFilesStr():String
		{
			if ( this.files == null || this.files.length == 0 )
				return null;
				
			var qs:String = ( DS.main as Main ).settings.quoteType;
			
			var str:String = "\t\t<div id=" + qs + "files" + qs + ">\n" +
				"\t\t\t<h2>Files</h2>\n" +
				"\t\t\t<div id=" + qs + "files-inner" + qs + ">\n" +
				"\t\t\t\t<ul>\n";
			for each( var file:File in this.files )
				str += "\t\t\t\t\t<li><a href=" + qs + file.filepath + qs + ">" + file.filename + "</a></li>\n";
			str += "\t\t\t\t</ul>\n" +
				"\t\t\t</div>\n" + // end div.files-inner
				"\t\t</div>"; // end div.files
			return str;
		}
		
		/**
		 * Gets the open graph info for the page, as a html string
		 * @return The open graph info as html, or null if we don't have any
		 */
		public function getOpenGraphStr():String
		{
			if ( this.openGraph == null )
				return null;
				
			var qs:String 	= ( DS.main as Main ).settings.quoteType;
			var qs2:String	= ( qs == "'" ) ? "\"" : "'";
			
			var str:String = "";
			
			// title
			if ( this.openGraph.title != null && StringUtil.trim( this.openGraph.title ) != "" )
				str += "\t\t<meta property=" + qs + "og:title" + qs + " content=" + qs + this.openGraph.title + qs + " />\n";
			else
				str += "\t\t<meta property=" + qs + "og:title" + qs + " content=" + qs + this.title + qs + " />\n";
				
			// type
			if ( this.openGraph.type != null && StringUtil.trim( this.openGraph.type ) != "" )
				str += "\t\t<meta property=" + qs + "og:type" + qs + " content=" + qs + this.openGraph.type + qs + " />\n";
				
			// url
			var siteURL:String = ( DS.main as Main ).settings.siteURL;
			str += "\t\t<meta property=" + qs + "og:url" + qs + " content=" + qs + siteURL + this.urlNew + qs + " />\n";
			
			// image
			if ( this.openGraph.image != null && StringUtil.trim( this.openGraph.image ) + "" )
				str += "\t\t<meta property=" + qs + "og:image" + qs + " content=" + qs + this.openGraph.image + qs + " />\n";
				
			// description
			if ( this.openGraph.description != null && StringUtil.trim( this.openGraph.description ) + "" )
				str += "\t\t<meta property=" + qs + "og:description" + qs + " content=" + qs + this.openGraph.description + qs + " />";
			return str;
		}
		
		/******************************************************************************/
		
		// processes the comment string
		private function _processComment( comment:Comment, tab:String ):String
		{			
			var qs:String = ( DS.main as Main ).settings.quoteType;
			
			// construct our comment
			var str:String = tab + "<div class=" + qs + "comment clearfix" + qs + ">\n" +
				tab + "\t<div class=" + qs + "comment-author" + qs + ">";
				
			// author + link
			if ( comment.homepage != null )
				str += "<a href=" + qs + comment.homepage + qs + " rel=" + qs + "nofollow" + qs + ">" + comment.name + "</a>";
			else
				str += comment.name;
				
			// set the content so that it's tabbed with the rest (then refix any <pre> tabs)
			var content:String 	= comment.comment.replace( Page.m_reNewLine, "\n\t\t" + tab );
			var startIndex:int	= content.indexOf( "<pre" );
			while ( startIndex != -1 )
			{
				var endIndex:int 	= content.indexOf( "</pre>", startIndex );
				var fixed:String	= content.substring( startIndex, endIndex + 6 );
				fixed				= fixed.replace( new RegExp( "\\n\\t\\t" + tab, "g" ), "\n" );
				content				= content.substring( 0, startIndex ) + fixed + content.substring( endIndex + 6 );
				
				// get the next match
				startIndex = content.indexOf( "<pre", endIndex );
			}
				
			// all the rest
			str += this._getTimeNode( comment.timestamp, qs ) + "</div>\n" +
				tab + "\t<div class=" + qs + "comment-content" + qs + ">\n" +
				tab + "\t\t" + content + "\n" +
				tab + "\t</div>\n" +
				tab + "</div>\n";
				
			// now go through and add any children
			if ( comment.children.length > 0 )
			{
				str += tab + "<div class=" + qs + "indented" + qs + ">\n";
				for each( var child:Comment in comment.children )
					str += this._processComment( child, tab + "\t" );
				str += tab + "</div>\n";
			}
			return str;
		}
		
		// returns the properly formatted time node for the comment
		private function _getTimeNode( date:Date, qs:String ):String
		{
			// the time should be in the form :
			// <time class="comment-date" datetime="2014-05-08 19:00">May 08 2014</time>
			var str:String = "<time class=" + qs + "comment-date" + qs + " datetime=" + qs + Page.m_timeTagFormatter.format( date ) + qs + ">";
			
			// add our html
			str += ( date.date < 10 ) ? "0" + date.date : "" + date.date;
			str += " " + Page.m_months[date.month] + " " + date.fullYear + "</time>";
			return str;
		}
		
	}

}