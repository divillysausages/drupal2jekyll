package model
{
	import com.divillysausages.ds.DS;
	import model.app.XMLData;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class File 
	{
		/******************************************************************************/
		
		public static var files:Vector.<File> = null;
		
		/******************************************************************************/
		
		public static function parseXML():void
		{
			// get our xml data
			var xmlData:XMLData = ( DS.main as Main ).xmlDataLoader.getData( File );
			var xml:XML			= ( xmlData != null ) ? xmlData.data1 : null;
			if ( xml == null )
			{
				File.files = new Vector.<File>( 0, true );
				return;
			}
				
			// parse our xml
			File.files	= new Vector.<File>( xml.database.table.length(), true );
			var i:int	= 0;
			for each( var fx:XML in xml.database.table )
			{
				var file:File = new File;
				for each( var cx:XML in fx.column )
				{
					// file id
					if ( cx.@name == "fid" )
						file.fid = int( cx.toString() );
						
					// file name
					if ( cx.@name == "filename" )
						file.filename = cx.toString();
						
					// file path
					if ( cx.@name == "filepath" )
					{
						file.filepath = cx.toString();
						if ( file.filepath.charAt( 0 ) != "/" )
							file.filepath = "/" + file.filepath; // make sure it starts with a slash
					}
						
					// file mime
					if ( cx.@name == "filemime" )
						file.filemime = cx.toString();
						
					// file size
					if ( cx.@name == "filesize" )
						file.filesize = int( cx.toString() );
						
					// timestamp
					if ( cx.@name == "timestamp" )
					{
						file.timestamp 		= new Date;
						var n:Number		= Number( cx.toString() );
						file.timestamp.time	= n * 1000;
					}
				}
				
				// add it
				File.files[i++] = file;
			}
		}
		
		public static function get( fid:int ):File
		{
			for each( var f:File in File.files )
			{
				if ( f.fid == fid )
					return f;
			}
			return null;
		}
		
		/******************************************************************************/
		
		public var fid:int 			= 0;
		public var filename:String 	= null;
		public var filepath:String 	= null;
		public var filemime:String 	= null;
		public var filesize:int		= 0;
		public var timestamp:Date	= null;
		
		/******************************************************************************/
		
		public function toString():String
		{
			return "[File tid:" + this.fid + ", filename: " + this.filename + ", date:" + this.timestamp + "]";
		}
		
	}

}