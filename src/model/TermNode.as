package model
{
	import com.divillysausages.ds.DS;
	import model.app.XMLData;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class TermNode 
	{
		/******************************************************************************/
		
		public static var termNodes:Vector.<TermNode> = null;
		
		/******************************************************************************/
		
		public static function parseXML():void
		{
			// get our xml data
			var xmlData:XMLData = ( DS.main as Main ).xmlDataLoader.getData( TermNode );
			var xml:XML			= ( xmlData != null ) ? xmlData.data1 : null;
			if ( xml == null )
			{
				TermNode.termNodes = new Vector.<TermNode>( 0, true );
				return;
			}
				
			// parse our xml
			TermNode.termNodes	= new Vector.<TermNode>( xml.database.table.length(), true );
			var i:int			= 0;
			for each( var tx:XML in xml.database.table )
			{
				var term:TermNode = new TermNode;
				for each( var cx:XML in tx.column )
				{
					// node id
					if ( cx.@name == "nid" )
						term.nid = cx.toString();
						
					// term id
					if ( cx.@name == "tid" )
						term.tid = int( cx.toString() );
				}
				
				// add it
				TermNode.termNodes[i++] = term;
			}
		}
		
		public static function getForNode( nid:int ):Vector.<TermNode>
		{
			var v:Vector.<TermNode> = new Vector.<TermNode>;
			for each( var t:TermNode in TermNode.termNodes )
			{
				if ( t.nid == nid )
					v.push( t );
			}
			return v;
		}
		
		/******************************************************************************/
		
		public var nid:int	= 0;
		public var tid:int 	= 0;
		
		/******************************************************************************/
		
		public function get node():Node { return Node.get( this.nid ); }
		public function get term():Term { return Term.get( this.tid ); }
		
		/******************************************************************************/
		
		public function toString():String
		{
			return "[TermNode nid:" + this.nid + " tid:" + this.tid + "]";
		}
		
	}

}