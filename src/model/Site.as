package model 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	/**
	 * The main site class that holds everything
	 * @author Damian Connolly
	 */
	public class Site 
	{
		
		/******************************************************************************/
		
		public function get nodes():Vector.<Node> { return Node.nodes; }
		public function get comments():Vector.<Comment> { return Comment.comments; }
		public function get urlAliases():Vector.<URLAlias> { return URLAlias.aliases; }
		public function get terms():Vector.<Term> { return Term.terms; }
		public function get termNodes():Vector.<TermNode> { return TermNode.termNodes; }
		public function get files():Vector.<File> { return File.files; }
		public function get uploads():Vector.<Upload> { return Upload.uploads; }
		public function get openGraphs():Vector.<OpenGraph> { return OpenGraph.graphs; }
		public function get menuLinks():Vector.<MenuLink> { return MenuLink.links; }
		
		/******************************************************************************/
		
		/**
		 * Gets all our classes to parse their respective XML files
		 * @return If we can go on with the conversion
		 */
		public function parseXMLFiles():Boolean
		{
			Node.parseXML();
			Comment.parseXML();
			URLAlias.parseXML();
			Term.parseXML();
			TermNode.parseXML();
			File.parseXML();
			Upload.parseXML();
			OpenGraph.parseXML();
			MenuLink.parseXML();
			
			// print out warnings if our classes aren't going to work properly
			if ( Upload.uploads.length > 0 && File.files.length == 0 )
				( DS.main as Main ).log( Log.WARN, "We have uploads but no files, so any files on pages won't work" );
			if ( File.files.length > 0 && Upload.uploads.length == 0 )
				( DS.main as Main ).log( Log.WARN, "We have files, but no uploads, so any files on pages won't work" );
			if ( TermNode.termNodes.length > 0 && Term.terms.length == 0 )
				( DS.main as Main ).log( Log.WARN, "We have term nodes but no terms, so terms on pages won't work" );
			if ( Term.terms.length > 0 && TermNode.termNodes.length == 0 )
				( DS.main as Main).log( Log.WARN, "We have terms but no term nodes, so terms on pages won't work" );
				
			// at the very least, we need our nodes
			if ( Node.nodes.length == 0 )
			{
				( DS.main as Main ).log( Log.ERROR, "No nodes were found (we need both node.xml and node_revisions.xml), so nothing will happen" );
				return false;
			}
			return true;
		}
		
	}

}