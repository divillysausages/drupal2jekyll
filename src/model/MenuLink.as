package model
{
	import com.divillysausages.ds.collections.linkedlist.LinkedList;
	import com.divillysausages.ds.DS;
	import flash.utils.Dictionary;
	import model.app.XMLData;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class MenuLink 
	{
		/******************************************************************************/
		
		/**
		 * All the individual links
		 */
		public static var links:Vector.<MenuLink> = null;
		
		/**
		 * All our menus, in <string, Vector<MenuLink>> form
		 */
		public static var menus:Object = null;
		
		/******************************************************************************/
		
		public static function parseXML():void
		{
			// destroy any old ones
			if ( MenuLink.links != null )
			{
				for each( var old:MenuLink in MenuLink.links )
					old.destroy();
			}
			if ( MenuLink.menus != null )
			{
				for ( var oldKey:* in MenuLink.menus )
				{
					MenuLink.menus[oldKey] = null;
					delete MenuLink.menus[oldKey];
				}
			}
			
			// get our xml data
			var xmlData:XMLData = ( DS.main as Main ).xmlDataLoader.getData( MenuLink );
			var xml:XML			= ( xmlData != null ) ? xmlData.data1 : null;
			if ( xml == null )
			{
				MenuLink.links 	= new Vector.<MenuLink>( 0, true );
				MenuLink.menus	= new Object;
				return;
			}
				
			// parse our xml
			MenuLink.links	= new Vector.<MenuLink>;
			MenuLink.menus	= new Object;
			var i:int		= 0;
			for each( var mx:XML in xml.database.table )
			{
				var link:MenuLink 	= new MenuLink;
				var success:Boolean	= false;
				for each( var cx:XML in mx.column )
				{
					// menu_name - we only want the links we made, not the drupal ones
					if ( cx.@name == "menu_name" )
					{
						if( cx.toString() != "primary-links" )
							break;
						else
						{
							success 		= true;
							link.menuName	= cx.toString();
						}
					}
						
					// mlid
					if ( cx.@name == "mlid" )
						link.mlid = int( cx.toString() );
						
					// plid
					if ( cx.@name == "plid" )
						link.plid = int( cx.toString() );
						
					// link path
					if ( cx.@name == "link_path" )
						link.linkPath = cx.toString();
						
					// link title
					if ( cx.@name == "link_title" )
						link.linkTitle = cx.toString();
						
					// link a title
					if ( cx.@name == "options" )
					{
						var options:String 	= cx.toString();
						
						// look for our query
						var index:int 	= options.indexOf( "query\"" );
						var start:int	= 0;
						var end:int		= 0;
						if ( index != -1 )
						{
							start 		= options.indexOf( "\"", index + 6 );
							end			= options.indexOf( "\"", start + 1 );
							link.query	= "?" + options.substring( start + 1, end );
						}
						
						// look for our fragment
						index = options.indexOf( "fragment\"" );
						if ( index != -1 )
						{
							start 			= options.indexOf( "\"", index + 9 );
							end				= options.indexOf( "\"", start + 1 );
							link.fragment 	= "#" + options.substring( start + 1, end );
						}
						
						// look for the title for our link
						index = options.indexOf( "title\"" );
						if ( index != -1 )
						{
							start			= options.indexOf( "\"", index + 6 );
							end				= options.indexOf( "\"", start + 1 );
							link.linkATitle	= options.substring( start + 1, end );
						}
					}
					
					// weight
					if ( cx.@name == "weight" )
						link.weight = int( cx.toString() );
				}
				
				// add it
				if ( success )
					MenuLink.links[i++] = link;
			}
			
			// now go through and sort all our links
			var len:int 	= MenuLink.links.length;
			var ml:MenuLink = null;
			for ( i = 0; i < len; i++ )
			{
				ml				 	= MenuLink.links[i];
				var parent:MenuLink	= ml.parent;
				if ( parent == null )
				{
					// it's a parent link, so add it to our main menu
					var main:Vector.<MenuLink> = MenuLink.menus[ml.menuName];
					if ( main == null ) // one doesn't exist for this menu, so create one
					{
						main 						= new Vector.<MenuLink>;
						MenuLink.menus[ml.menuName]	= main;
					}
					main.push( ml );
				}
				else
					parent.children.push( ml );
			}
			
			// now go through and sort our menus, so they're ranked by weight
			for ( i = 0; i < len; i++ )
			{
				ml = MenuLink.links[i];
				if ( ml.children.length > 0 )
					ml.children.sort( MenuLink._sort );
			}
			for ( var key:String in MenuLink.menus ) // sort all the main menus
				MenuLink.menus[key].sort( MenuLink._sort );
		}
		
		public static function get( mlid:int ):MenuLink
		{
			for each( var m:MenuLink in MenuLink.links  )
			{
				if ( m.mlid == mlid )
					return m;
			}
			return null;
		}
		
		/******************************************************************************/
		
		// the function for sorting our menu links by weight
		private static function _sort( a:MenuLink, b:MenuLink ):int
		{
			return ( a.weight == b.weight ) ? a.mlid - b.mlid : a.weight - b.weight;
		}
		
		/******************************************************************************/
		
		/**
		 * If this is a parent menu link, this is our children
		 */
		public var children:Vector.<MenuLink> = new Vector.<MenuLink>;
		
		public var menuName:String		= null;
		public var mlid:int 			= 0;
		public var plid:int				= 0;
		public var linkPath:String		= null;
		public var linkTitle:String		= null;
		public var linkATitle:String	= null;
		public var weight:int			= 0;
		public var query:String			= null;	// any ?foo=bar 
		public var fragment:String		= null; // any #foo after 
		
		/******************************************************************************/
		
		public function get parent():MenuLink { return ( this.plid > 0 ) ? MenuLink.get( this.plid ) : null; }
		
		/******************************************************************************/
		
		public function destroy():void
		{
			this.children = null;
		}
		
		public function toString():String
		{
			return "[MenuLink mlid:" + this.mlid + ", title: " + this.linkTitle + ", path:" + this.linkPath + "]";
		}
		
	}

}