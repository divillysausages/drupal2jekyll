package model
{
	import com.divillysausages.ds.DS;
	import model.app.XMLData;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class OpenGraph 
	{
		/******************************************************************************/
		
		public static var graphs:Vector.<OpenGraph> = null;
		
		/******************************************************************************/
		
		public static function parseXML():void
		{
			// get our xml data
			var xmlData:XMLData = ( DS.main as Main ).xmlDataLoader.getData( OpenGraph );
			var xml:XML			= ( xmlData != null ) ? xmlData.data1 : null;
			if ( xml == null )
			{
				OpenGraph.graphs = new Vector.<OpenGraph>( 0, true );
				return;
			}
				
			// parse our xml data
			OpenGraph.graphs	= new Vector.<OpenGraph>( xml.database.table.length(), true );
			var i:int			= 0;
			for each( var ox:XML in xml.database.table )
			{
				var graph:OpenGraph = new OpenGraph;
				for each( var cx:XML in ox.column )
				{						
					// node id
					if ( cx.@name == "nid" )
						graph.nid = cx.toString();
						
					// title
					if ( cx.@title == "title" )
						graph.title = cx.toString();
						
					// description
					if ( cx.@name == "description" )
						graph.description = cx.toString();
						
					// image
					if ( cx.@name == "image" )
						graph.image = cx.toString();
						
					// type
					if ( cx.@name == "type" )
						graph.type = cx.toString();
				}
				
				// check
				if ( graph.title == "" )		graph.title 		= null;
				if ( graph.description == "" )	graph.description 	= null;
				if ( graph.image == "" )		graph.image 		= null;
				if ( graph.type == "" )			graph.type 			= null;
				
				// add it
				OpenGraph.graphs[i++] = graph;
			}
		}
		
		public static function getForNode( nid:int ):OpenGraph
		{
			for each( var o:OpenGraph in OpenGraph.graphs )
			{
				if ( o.nid == nid )
					return o;
			}
			return null;
		}
		
		/******************************************************************************/
		
		public var nid:int 				= 0;
		public var title:String			= null;
		public var description:String 	= null;
		public var image:String			= null;
		public var type:String			= null;
		
		/******************************************************************************/
		
		public function get node():Node { return Node.get( this.nid ); }
		
		/******************************************************************************/
		
		public function toString():String
		{
			return "[OpenGraph nid:" + this.nid + ", title: " + this.title + ", description:" + this.description + "]";
		}
		
	}

}