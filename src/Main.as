package 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import controllers.ControllerManager;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.events.UncaughtErrorEvent;
	import model.app.Settings;
	import model.app.XMLDataLoader;
	import model.Site;
	import view.GUI;
	
	/**
	 * Our main application class
	 * @author Damian Connolly
	 */
	public class Main extends Sprite
	{
		
		/******************************************************************************/
		
		/**
		 * The settings for the app
		 */
		public var settings:Settings = null;
		
		/**
		 * The manager for our controllers
		 */
		public var controllerManager:ControllerManager	= null;
		
		/**
		 * The GUI for the app
		 */
		public var gui:GUI = null;
		
		/**
		 * The old site, with all the previous objects in their original form
		 */
		public var site:Site = null;
		
		/**
		 * The loader that we use to load our xml files
		 */
		public var xmlDataLoader:XMLDataLoader = null;
		
		/******************************************************************************/
		
		public function Main():void 
		{
			DS.start( this, 60 );
			DS.pauseDisabled = true;
			
			// create our objects
			this.settings 			= new Settings;
			this.controllerManager	= new ControllerManager;
			this.site				= new Site;
			this.xmlDataLoader		= new XMLDataLoader;
			
			// create our GUI (NOTE: do it last)
			this.gui = new GUI;
			
			// add our uncaught error handler
			this.loaderInfo.uncaughtErrorEvents.addEventListener( UncaughtErrorEvent.UNCAUGHT_ERROR, this._onUncaughtErrorEvent );
		}
		
		/**
		 * Logs a message, both to the console and the on-screen output
		 * @param level The log level to use - use the static consts from the Log class
		 * @param msg The message to log
		 */
		public function log( level:uint, msg:String ):void
		{
			if ( level == Log.DEBUG )
				DS.debug( this, msg );
			else if ( level == Log.LOG )
				DS.log( this, msg );
			else if ( level == Log.WARN )
				DS.log( this, msg );
			else
				DS.error( this, msg );
			this.gui.log( level, msg );
		}
		
		/******************************************************************************/
		
		// called when we've been notified about an uncaught error
		private function _onUncaughtErrorEvent( e:UncaughtErrorEvent ):void
		{
			var message:String		= null;
			var stackTrace:String	= null;
			
			// get the message
			if ( e.error is Error )
			{
				message = ( e.error as Error ).message;
				try	{ stackTrace = ( e.error as Error ).getStackTrace(); }
				catch ( error:Error ) {	stackTrace = "No stack trace"; }
			}
			else if ( e.error is ErrorEvent )
				message = ( e.error as ErrorEvent ).text;
			else
				message = e.error.toString();
			
			// show an alert
			this.log( Log.ERROR, "An uncaught exception has occurred: " + e.errorID + ": " + e.type + ": " + message + ", stack:\n" + stackTrace );
			e.preventDefault();
		}
		
	}
	
}