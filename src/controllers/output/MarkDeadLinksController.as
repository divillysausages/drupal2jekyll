package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import model.Comment;
	import model.deadlinks.DeadLink;
	import model.deadlinks.DeadLinks;
	import model.output.Page;
	
	/**
	 * The controller that we use to mark dead links in our content
	 * @author Damian Connolly
	 */
	public class MarkDeadLinksController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_count:int 	= 0; 	// the number of dead links that we marked
		private var m_pageCount:int	= 0;	// the number of dead links for the current page
		
		/******************************************************************************/
		
		public function MarkDeadLinksController() 
		{
			this.tooltip = "Marks all dead links with a CSS class (REQUIRES VerifyLink)";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Marking dead links" );
			
			this.m_count = 0;
		}
		
		override public function update(dt:Number):void 
		{
			// make sure we have a dead links folder
			if ( ( DS.main as Main ).settings.deadLinkFolder == null )
			{
				( DS.main as Main ).log( Log.ERROR, "Not marking dead links as the Dead link folder hasn't been set in the Settings" );
				this.m_currIndex = this.m_pages.length;
				this.postUpdate();
				return;
			}
			
			// get the current page
			var curr:Page 		= this.m_pages[this.m_currIndex];
			this.m_pageCount 	= 0;
			
			// do our main content
			curr.content = this._processString( curr.content );
			
			// do all our comments
			for each( var comment:Comment in curr.comments )
				this._processComment( comment );
				
			// log it
			if( this.m_pageCount > 0 )
				( DS.main as Main ).log( Log.DEBUG, "Marked " + this.m_pageCount + " individual link(s) as dead in '" + curr.title + "' (" + curr.urlNew + ")" );
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			( DS.main as Main ).log( Log.DEBUG, "Marked " + this.m_count + " individual link(s) as dead" );
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			comment.comment = this._processString( comment.comment );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// processes a string, looking for php
		private function _processString( str:String ):String
		{
			var qt:String = ( DS.main as Main ).settings.quoteType;
			
			// go through each link in the dead links vector and mark it in our content
			var dls:Vector.<DeadLink> = DeadLinks.instance.unreplacedDeadLinks;
			for each( var deadLink:DeadLink in dls )
			{
				var index:int = str.indexOf( deadLink.oldURL );
				while ( index != -1 )
				{
					// the next char needs to be a ' or a ", otherwise a deadLink like "www.google.com" will also
					// match "www.google.com/foo", or it could cover a later deadlink match					
					var char:String = str.charAt( index + deadLink.oldURL.length );
					if ( char != "'" && char != "\"" && char != "<" )
					{
						// false positive - continue
						index = str.indexOf( deadLink.oldURL, index + deadLink.oldURL.length );
						continue;
					}
					
					// find the index of our tags
					var startTagIndex:int 	= str.lastIndexOf( "<", index ); // get the start of our tag: <img... / <a...
					var endTagIndex:int		= str.indexOf( ">", index + deadLink.oldURL.length ); // end tag is after the link: <img src="..." /> / <a href="..."></a>
					
					// check if there's already a class defined
					var classIndex:int = str.indexOf( "class", startTagIndex );
					if ( classIndex != -1 && classIndex < endTagIndex )
					{
						// we already have a class, so we can just add it
						var quoteIndex:int 		= str.indexOf( "\"", classIndex );
						var endQuoteIndex:int	= str.indexOf( "\"", quoteIndex + 1 ); // look for the end quote
						var deadLinkIndex:int	= str.indexOf( "dead-link", quoteIndex );
						if ( deadLinkIndex == -1 || deadLinkIndex > endQuoteIndex ) // quick check to make sure we don't already have it
						{
							str	= str.substring( 0, endQuoteIndex ) +
									" dead-link" +
									str.substring( endQuoteIndex );
													
							this.m_count++;
							this.m_pageCount++;
						}
					}
					else
					{
						// we don't have a class, so we can just add one
						var spaceIndex:int 	= str.indexOf( " ", startTagIndex );
						str 				= str.substring( 0, spaceIndex + 1 ) +
												"class=" + qt + "dead-link" + qt + " " +
												str.substring( spaceIndex + 1 );
												
						this.m_count++;
						this.m_pageCount++;
					}
					
					// get our next index
					index = str.indexOf( deadLink.oldURL, index + deadLink.oldURL.length );
				}
			}
			return str;
		}
		
	}

}