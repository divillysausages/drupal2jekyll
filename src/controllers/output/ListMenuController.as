package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import controllers.debug.MenuLinkController;
	import model.MenuLink;
	import model.Node;
	import model.output.Page;
	
	/**
	 * Prints the menu code
	 * @author Damian Connolly
	 */
	public class ListMenuController extends OutputController 
	{
		
		public function ListMenuController()
		{
			this.tooltip = "Prints HTML code for the menus";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Generating menu code" );
		}
		
		override public function update(dt:Number):void 
		{
			var qt:String = ( DS.main as Main ).settings.quoteType;
			
			// go through and print out our menus
			var menus:Object = MenuLink.menus;
			for ( var key:String in menus )
			{
				var menu:Vector.<MenuLink> 	= menus[key];
				var s:String				= "&lt;div id=" + qt + key + qt + "&gt;\n\t&lt;ul&gt;\n";
				for each( var ml:MenuLink in menu )
					s += this._printMenuLink( ml, "\t\t" );
				s += "\t&lt;/ul&gt;\n&lt;/div&gt;";
				( DS.main as Main ).log( Log.LOG, s );
			}
			
			// post update
			this.m_currIndex = this.m_pages.length; // so we finish right away
			this.postUpdate();
		}
		
		/******************************************************************************/
		
		// prints out the link for our menu node and it's children if we have any
		private function _printMenuLink( ml:MenuLink, tabs:String ):String
		{
			var qt:String 	= ( DS.main as Main ).settings.quoteType;
			var qt2:String = ( qt == "'" ) ? "\"" : "'";
			
			// get the url
			var url:String = null;
			var end:String = ( ( ml.query != null ) ? ml.query : "" ) + ( ( ml.fragment != null ) ? ml.fragment : "" ); // the query (?) and fragment (#) if we have one
			for each( var page:Page in this.m_pages )
			{
				if ( page.urlOld == ml.linkPath || page.urlAlias == ml.linkPath )
				{
					url = "{{ " + qt2 + page.urlNew + end + qt2 + " | prepend: site.baseurl }}";
					break;
				}
			}
			if ( url == null )
			{
				// if we can't find a page, then it's either an external page, or a drupal generated one (e.g. contact)
				if ( ml.linkPath.indexOf( "http" ) != 0 )
				{
					var path:String = ml.linkPath;
					if ( path.charAt( 0 ) != "/" )
						path = "/" + ml.linkPath; // make sure there's an initial slash
					if ( path.charAt( path.length - 1 ) != "/" )
						path = path + "/"; // make sure there's a trailing slash
					url = "{{ " + qt2 + path + end + qt2 + " | prepend: site.baseurl }}";
				}
				else
				{
					// make sure there's no site url
					var siteURL:String = ( DS.main as Main ).settings.siteURL;
					if ( ml.linkPath.indexOf( siteURL ) == 0 )
						url = "{{ " + qt2 + ml.linkPath.substring( siteURL.length ) + end + qt2 + " | prepend: site.baseurl }}";
					else
						url = ml.linkPath + end;
				}
			}
			
			// create our string
			var title:String	= ( ml.linkATitle != null ) ? ml.linkATitle : "";
			var s:String 		= tabs + "&lt;li&gt;&lt;a href=" + qt + url + qt + " title=" + qt + title + qt + "&gt;" + ml.linkTitle + "&lt;/a&gt;";
			if ( ml.children.length > 0 )
			{
				s += "\n" + tabs + "\t&lt;ul&gt;\n";
				for each( var mlc:MenuLink in ml.children )
					s += this._printMenuLink( mlc, tabs + "\t\t" );
				s += tabs + "\t&lt;/ul&gt;\n" + tabs;
			}
			s += "&lt;/li&gt;\n ";
			return s;
		}
		
	}

}