package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import com.divillysausages.ds.util.ObjectUtils;
	import model.Comment;
	import model.output.Page;
	
	/**
	 * Replaces all the old urls with the new format
	 * @author Damian Connolly
	 */
	public class ReplaceOldURLsController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_count:int 	= 0; 	// the total number of replaced urls
		private var m_regExp:RegExp	= null;	// the regexp that we're going to be calling
		
		/******************************************************************************/
		
		public function ReplaceOldURLsController()
		{
			this.tooltip = "Replaces all internal URLs to point to their new format";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Replacing old URLs" );
			this.m_count = 0;
			
			// create our regexp (siteURL + / + WHATEVER + "/'/<
			var siteURL:String	= ( DS.main as Main ).settings.siteURL;
			this.m_regExp		= new RegExp( "(" + siteURL + "\/)[a-zA-Z0-9\/_#\.%\?=]+(\"|'|<)", "g" );
		}
		
		override public function update(dt:Number):void 
		{
			// get the current page
			var curr:Page 		= this.m_pages[this.m_currIndex];
			var siteURL:String	= ( DS.main as Main ).settings.siteURL;
			
			// do our main content
			curr.content = this._processString( curr.content, siteURL );
			
			// do all our comments
			for each( var comment:Comment in curr.comments )
				this._processComment( comment, siteURL );
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			( DS.main as Main ).log( Log.DEBUG, "Replaced " + this.m_count + " olds URLs with their new equivalent" );
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment, siteURL:String ):void
		{
			comment.comment = this._processString( comment.comment, siteURL );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child, siteURL );
			}
		}
		
		// processes a string, replacing old urls
		private function _processString( str:String, siteURL:String ):String
		{
			var urls:Array = str.match( this.m_regExp );
			if ( urls.length > 0 )
			{
				outer: for each( var fullURL:String in urls )
				{
					// first remove the site url + / and the ending char
					var end:String		= fullURL.charAt( fullURL.length - 1 );
					var pageURL:String 	= fullURL.substring( siteURL.length + 1, fullURL.length - 1 );
					
					// if there's a "." in there, ignore it, it's a file
					if ( pageURL.indexOf( "." ) != -1 )
						continue;
					
					// remove the starting ?q= if it's not a clean url
					if ( pageURL.indexOf( "?q=" ) == 0 )
						pageURL = pageURL.substr( 3 );
					
					// check for a query/hash
					var index:int = pageURL.indexOf( "?" );
					if ( index == -1 )
						index = pageURL.indexOf( "#" );
					if ( index != -1 )
					{
						end		= pageURL.substring( index ) + end; // store the end
						pageURL = pageURL.substring( 0, index );
					}
						
					// check for an ending "/"
					if ( pageURL.charAt( pageURL.length - 1 ) == "/" )
						pageURL = pageURL.substring( 0, pageURL.length - 1 );
						
					// make sure it's not encoded
					pageURL = decodeURIComponent( pageURL );
						
					// now go through all our pages and check for a match
					for each( var otherPage:Page in this.m_pages )
					{
						if ( pageURL == otherPage.urlOld ||
							 pageURL == otherPage.urlAlias )
						{
							// NOTE: the urlNew is surrounded in "/"'s
							str = str.replace( fullURL, siteURL + otherPage.urlNew + end );
							this.m_count++;
							continue outer;
						}
					}
				}
			}
			return str;
		}
		
	}

}