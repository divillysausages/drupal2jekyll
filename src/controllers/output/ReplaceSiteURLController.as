package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import model.Comment;
	import model.File;
	import model.output.Page;
	
	/**
	 * Finds any urls that start with our site url and replaces them with liquid comments
	 * @author Damian Connolly
	 */
	public class ReplaceSiteURLController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_count:int 	= 0; 		// the total number of replaced urls
		private var m_regExp:RegExp	= null;		// the regexp that we're going to use
		private var m_replaceStr:String	= null;	// the string that we're going to use to replace our matches with
		
		/******************************************************************************/
		
		public function ReplaceSiteURLController()
		{
			this.tooltip = "Replaces all instances of the site url with the Jekyll site.baseurl equivalent";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Replacing site URLs" );
			this.m_count = 0;
			
			// get our site url and create our regexp
			var siteURL:String 	= ( DS.main as Main ).settings.siteURL;
			this.m_regExp		= new RegExp( "((?:href|src) ?= ?(?:'|\"))(" + siteURL + ")([\\/a-zA-Z0-9-_\\? =\\.#&%~!]{0,})", "g" );
			
			// set up our replace string
			this.m_replaceStr = "| prepend: site.baseurl";
			if ( ( DS.main as Main ).settings.alsoUseSiteURL )
				this.m_replaceStr += " | prepend: site.url";
		}
		
		override public function update(dt:Number):void 
		{
			// get the current page
			var curr:Page = this.m_pages[this.m_currIndex];
			
			// do our main content
			curr.content = this._processString( curr.content );
			
			// do all our comments
			for each( var comment:Comment in curr.comments )
				this._processComment( comment );
				
			// do all our files
			var siteURL:String = ( DS.main as Main ).settings.siteURL;
			for each( var file:File in curr.files )
			{
				if ( file.filepath.indexOf( siteURL ) == 0 )
					file.filepath = "{{ '" + file.filepath.substring( siteURL.length ) + "' " + this.m_replaceStr + " }}";
				else
					file.filepath = "{{ '" + file.filepath + "' | " + this.m_replaceStr + " }}";
			}
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			( DS.main as Main ).log( Log.DEBUG, "Replaced " + this.m_count + " site URLs" );
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			comment.comment = this._processString( comment.comment );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// processes a string, replace all site urls
		private function _processString( str:String ):String
		{
			this.m_count += str.match( this.m_regExp ).length; // up the count
			return str.replace( this.m_regExp, "$1{{ '$3' " + this.m_replaceStr + " }}" );
		}
		
	}

}