package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import flash.globalization.DateTimeFormatter;
	import flash.globalization.LocaleID;
	import model.output.Page;
	
	/**
	 * Creates the new filenames for our pages
	 * @author Damian Connolly
	 */
	public class CreateFilenameController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_dateFormatter:DateTimeFormatter 	= null; // the formatter we use for creating our filenames
		private var m_urlFormatter:DateTimeFormatter	= null;	// the formatter for our url
		
		/******************************************************************************/
		
		public function CreateFilenameController()
		{
			this.tooltip			= "Creates the filename for the final save";
			this.m_dateFormatter 	= new DateTimeFormatter( LocaleID.DEFAULT );
			this.m_urlFormatter		= new DateTimeFormatter( LocaleID.DEFAULT );
			this.m_dateFormatter.setDateTimePattern( "yyyy-MM-dd" );
			this.m_urlFormatter.setDateTimePattern( "yyyy/MM/dd/" );
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Creating new filenames" );
		}
		
		override public function update(dt:Number):void 
		{
			// get the current page
			var curr:Page = this.m_pages[this.m_currIndex];
			
			// create our file name
			var filename:String = curr.title;
			filename			= filename.replace( /\.\.\./g, "-" ); 			// replace any "..." with dashes
			filename			= filename.replace( /\//g, "-" );				// replace / with -
			filename			= filename.replace( /[?!:';"\.,#]/g, "" );		// remove any special chars
			filename			= filename.replace( /&/g, "-" );				// replace & with -
			filename			= filename.replace( /[àáâãäå]/g, "a" );			// replace a special chars
			filename			= filename.replace( /æ/g, "ae" );				// replace ae
			filename			= filename.replace( /ç/g, "c" );				// replace c special chars
			filename			= filename.replace( /[èéêë]/g, "e" );			// replace e special chars
			filename			= filename.replace( /[ìíîï]/g, "i" );			// replace i special chars
			filename			= filename.replace( /ñ/g, "n" );				// replace n special chars
			filename			= filename.replace( /[òóôõö]/g, "o" );			// replace o special chars
			filename			= filename.replace( /[ùúûü]/g, "u" );			// replace u special chars
			filename			= filename.replace( /[ýÿ]/g, "y" );				// replace y special chars
			filename			= filename.replace( / {2,}/g, "" );				// remove any extra spaces (do this last, as removing chars could leave a space)
			filename			= filename.replace( /[_ ]/g, "-" );				// replace spaces and underscores with dashes
			filename			= filename.replace( /-{2,}/g, "-" );			// replace multiple dashes with one
			if ( filename.lastIndexOf( "-" ) == filename.length - 1 )
				filename		= filename.substr( 0, filename.length - 1 ); 	// if '-' is the last char, remove it
			filename			= filename.toLowerCase();						// lowercase it
			
			// generate our url (simplified if it's a page, not a post)
			var oldURL:String	= ( curr.isPage && curr.urlAlias != null ) ? curr.urlAlias : filename;
			var url:String 		= ( curr.isPage ) ? "/" + oldURL + "/" : "/" + this.m_urlFormatter.format( curr.timestamp ) + filename + "/";
			
			// finish our filename (simplified if it's a page, not a post)
			filename = ( curr.isPage ) ? oldURL + ".html" : this.m_dateFormatter.format( curr.timestamp ) + "-" + filename + ".html";
			
			// set our filename and new url (simplified if it's a page, not a post)
			curr.urlNew		= ( curr.frontMatter.permalink != null ) ? curr.frontMatter.permalink : url;
			curr.filename	= filename;
			
			// set the permalink - this is to help stop bugs relating to the locale when posts are
			// being generated
			if( curr.frontMatter.permalink == null )
				curr.frontMatter.permalink = curr.urlNew;
			
			// post update
			this.postUpdate();
		}
		
	}

}