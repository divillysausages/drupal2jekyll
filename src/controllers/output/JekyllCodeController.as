package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import model.Comment;
	import model.output.Page;
	import mx.utils.StringUtil;
	
	/**
	 * Replaces all <pre><code> elements with jekyll code elements, so we can preprocess code highlighting
	 * @author Damian Connolly
	 */
	public class JekyllCodeController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_curr:Page					= null;	// our current page
		private var m_count:int					= 0; 	// the amount of <pre> elements we've changed
		private var m_countComments:int			= 0; 	// the amount of <pre> elements in comments that we've changed
		private var m_jekyllCodeType:String		= null;	// the default style code type for our <code> elements
		private var m_jekyllUseLineNos:Boolean	= false;// should we use line numbers?
		
		/******************************************************************************/
		
		public function JekyllCodeController()
		{
			this.tooltip = "Replaces all <pre><code> tags with {% highlight %} liquid tags for preprocessing code highlighting";
		}
		
		override public function start( pages:Vector.<Page> ):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Replacing &lt;pre&gt;&lt;code&gt; tags with {% highlight %} liquid tags" );
			
			// get our style code type
			this.m_jekyllCodeType 	= ( DS.main as Main ).settings.jekyllCodeType;
			this.m_jekyllUseLineNos	= ( DS.main as Main ).settings.jekyllCodeLineNos;
			
			// reset counts
			this.m_count			= 0;
			this.m_countComments	= 0;
		}
		
		override public function update(dt:Number):void 
		{
			// get the current page
			this.m_curr = this.m_pages[this.m_currIndex];
			
			// we need a jekyll code type set
			if ( this.m_jekyllCodeType == null || StringUtil.trim( this.m_jekyllCodeType ) == "" )
			{
				( DS.main as Main ).log( Log.WARN, "JekyllCodeController needs to have the 'Jekyll &lt;code&gt; type {% highlight %}' set in the Settings menu in order to work" );
				this.m_currIndex = this.m_pages.length;
				this.postUpdate();
				return;
			}
			
			// do our main content
			this.m_curr.content = this._processString( this.m_curr.content, false );
			
			// do all our comments
			for each( var comment:Comment in this.m_curr.comments )
				this._processComment( comment );
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			( DS.main as Main ).log( Log.DEBUG, "Replaced " + this.m_count + " &lt;pre&gt;&lt;code&gt; element(s) (" + this.m_countComments + " comment(s)) with liquid tags" );
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			comment.comment = this._processString( comment.comment, true );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// actually processes our strings
		private function _processString( str:String, isComment:Boolean ):String
		{
			var codeStr:String = null;
			
			var qt:String = ( DS.main as Main ).settings.quoteType;
			
			// find any <pre> elements followed by <code> elements
			var preStartIndex:int 	= str.indexOf( "<pre" );
			var codeStartIndex:int	= preStartIndex; // end for <code> (i.e. the ">")
			var codeEndIndex:int	= preStartIndex; // start for </code> (i.e. the "<")
			var preEndIndex:int		= preStartIndex; // end for </pre> (i.e. the ">")
			while ( preStartIndex != -1 )
			{
				// get the end bracket index (the pre might have a class or id)
				var endBracketIndex:int = str.indexOf( ">", preStartIndex + 4 );
				if ( endBracketIndex == -1 )
				{
					( DS.main as Main ).log( Log.ERROR, "The '" + this.m_curr.title + "' page (" + this.m_curr.filename + ") contains a mal-formatted &lt;pre&gt; tag (is comment: " + isComment + ")" );
					break;
				}
				
				// how much we're going to add to preStartIndex in order to get the next match
				var indexJump:int = 4; // initially to cover the "<pre"
				
				// check if the next bit is code
				var codeIndex:int = str.indexOf( "<code", endBracketIndex + 1 );
				if ( codeIndex == endBracketIndex + 1 ) // <code> has to immediately follow <pre>
				{
					// get the end of the initial <code> tag (it probably has a class) and get the end </code> and </pre> tags
					codeStartIndex 	= str.indexOf( ">", codeIndex + 5 );
					codeEndIndex	= ( codeStartIndex != -1 ) ? str.indexOf( "</code>", codeStartIndex + 1 ) : -1;
					preEndIndex		= ( codeEndIndex != -1 ) ? str.indexOf( "</pre>", codeEndIndex + 7 ) : -1;
					if ( codeStartIndex == -1 || codeEndIndex == -1 || preEndIndex == -1 )
					{
						( DS.main as Main ).log( Log.ERROR, "The '" + this.m_curr.title + "' page (" + this.m_curr.filename + ") contains mal-formatted &lt;pre&gt; or &lt;code&gt; tags (is comment: " + isComment + ")" );
						break;
					}
					
					// adjust the indices so they point to the right part
					codeStartIndex 	+= 1; // after the closing ">" of </code>
					preEndIndex		+= 6; // after the closing ">" of </pre>
					
					// get the strings that we're going to replace with
					var startStr:String = "{% highlight " + this.m_jekyllCodeType;
					var endStr:String	= "{% endhighlight %}";
					if ( this.m_jekyllUseLineNos )
						startStr += " linenos";
					startStr += " %}";
					
					// replace our string
					str = str.substring( 0, preStartIndex ) +
							startStr + 
							str.substring( codeStartIndex, codeEndIndex ) +
							endStr +
							str.substring( preEndIndex );
							
					// update our index jump
					indexJump = startStr.length + ( codeEndIndex - codeStartIndex ) + endStr.length;
					
					// update our count
					this.m_count++;
					if ( isComment )
						this.m_countComments++;
				}
				
				// look for our next index
				preStartIndex = str.indexOf( "<pre", preStartIndex + indexJump );
			} // end <pre> while
			return str;
		}
		
	}

}