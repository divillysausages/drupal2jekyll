package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import model.Comment;
	import model.File;
	import model.Node;
	import model.OpenGraph;
	import model.output.Page;
	import model.Term;
	import model.TermNode;
	import model.Upload;
	import model.URLAlias;
	
	/**
	 * Takes the nodes and converts them to pages, so they can be used in the other controllers
	 * @author Damian Connolly
	 */
	public class ConvertToPageController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_tagRegExp:RegExp 					= null; // the regexp we run to capitalise the tags
		private var m_pagesNotPosts:Array/**String*/	= null; // the urls for pages, not posts
		
		/******************************************************************************/
		
		public function ConvertToPageController()
		{
			this.tooltip = "Converts the raw data into an object form (REQUIRED)";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Starting conversion to pages" );
			this.m_pages 		= new Vector.<Page>;
			this.m_tagRegExp	= /\b[a-zA-Z]/g;
			
			// get the list of urls for pages that we don't treat as posts
			this.m_pagesNotPosts = ( DS.main as Main ).settings.pages;
		}
		
		override public function update(dt:Number):void 
		{
			// get our nodes
			var nodes:Vector.<Node> = ( DS.main as Main ).site.nodes;
			
			// if this is the first node, create our special pages first
			if ( this.m_currIndex == 0 )
			{
				this._createTagPage();
				this._createArchivePage();
				this._createSearchPage();
				this._createContactPage();
				this._create404Page();
			}
			
			// special case in case we have no nodes
			if ( nodes.length == 0 )
			{
				this.postUpdate();
				return;
			}
			
			// get the current node
			var curr:Node = nodes[this.m_currIndex];
			if ( curr.timestamp == null )
				curr.timestamp = new Date;
			
			// create a page for it
			var page:Page = new Page;
			
			// set our properties
			page.frontMatter.title 	= curr.title;
			page.frontMatter.date	= curr.timestamp;
			page.title				= curr.title;
			page.content			= curr.body;
			page.nodeID				= curr.nid;
			page.timestamp			= curr.timestamp;
			
			// update the urls
			var alias:URLAlias = URLAlias.getForNID( page.nodeID );
			if ( alias != null )
			{
				page.urlOld 	= alias.src;
				page.urlAlias	= alias.dst;
			}
			else
				page.urlOld = "node/" + page.nodeID; // there's no alias, so it's just node/{ID}
			
			// check if it's a normal page, not a post
			for each( var pageNotPostURL:String in this.m_pagesNotPosts )
			{
				if ( page.urlAlias == pageNotPostURL )
				{
					page.isPage 			= true;
					page.frontMatter.layout	= "page";
					break;
				}
			}
			
			// update any files
			var uploads:Vector.<Upload> = Upload.getForNode( page.nodeID );
			if ( uploads != null && uploads.length > 0 )
			{
				for each( var upload:Upload in uploads )
				{
					var file:File = upload.file;
					if( file != null )
						page.files.push( file ); // check for null as we might be missing our files xml
				}
				page.frontMatter.add( "hasFiles", "true" ); // used to add the right include
			}
			
			// add any terms
			var terms:Vector.<TermNode> = TermNode.getForNode( page.nodeID );
			if ( terms != null && terms.length > 0 )
			{				
				var a:Array /**String*/ = [];
				for each( var termNode:TermNode in terms )
				{
					var term:Term = termNode.term;
					if( term != null )
						a.push( term.name ); // we might not have the term data xml loaded
				}
					
				// make sure the first letter of each word in the tag is capitalised
				var len:int = a.length;
				for ( var i:int = 0; i < len; i++ )
					a[i] = a[i].replace( this.m_tagRegExp, this._tagCapitalise );
					
				// add it to the frontmatter
				if( a.length > 0 )
					page.frontMatter.tags = a;
			}
			
			// add our comments
			var comments:Vector.<Comment> = Comment.getForNode( page.nodeID );
			if ( comments != null && comments.length > 0 )
			{
				page.comments = comments;
				page.frontMatter.add( "hasComments", "true" ); // used to add the right include
			}
			
			// add any open graph object
			var openGraph:OpenGraph = OpenGraph.getForNode( page.nodeID );
			if ( openGraph != null )
			{
				page.openGraph = openGraph;
				page.frontMatter.add( "hasOpenGraph", "true" ); // use to add the right include
			}
			
			// add it
			this.m_pages.push( page );
			this.postUpdate();
		}
		
		// NOTE: we override the post update as we're creating the vector of pages, so
		// we decide when we're finished based on the nodes instead
		override public function postUpdate():void
		{
			var nodes:Vector.<Node> = ( DS.main as Main ).site.nodes;
			var len:int				= nodes.length;
			
			// update our current index
			this.m_currIndex++;
			this.signalOnUpdate.dispatch( this.m_currIndex / len );
			if ( this.m_currIndex >= len )
			{
				// reverse our vector
				var pages:Vector.<Page> = new Vector.<Page>;
				for ( var i:int = this.m_pages.length - 1; i >= 0; i-- )
					pages.push( this.m_pages[i] );
				this.m_pages = pages;
				
				this.m_active = false;
				this.signalOnComplete.dispatch( this.m_pages );
			}
		}
		
		/******************************************************************************/
		
		// called by the replace
		private function _tagCapitalise():String
		{
			return arguments[0].toUpperCase();
		}
		
		// create a tags page as the liquid syntax to create it is a bit of a pain
		private function _createTagPage():void
		{
			// create a page for it
			var page:Page 		= new Page;
			page.isCreatedByApp	= true;
			
			// set our properties
			page.frontMatter.title 		= "Tags";
			page.frontMatter.date		= new Date;
			page.frontMatter.permalink	= "/tags/"	
			page.title					= "Tags";
			page.timestamp				= new Date;
			page.isPage					= true;
			page.isTagsPage				= true;
			page.urlOld					= "taxonomy/term";
			page.urlAlias				= "tags";
			page.urlNew					= "/tags/";
			
			var qt:String 	= ( DS.main as Main ).settings.quoteType;
			var qt2:String	= ( qt == "\"" ) ? "'" : "\"";
			
			// create our body
			// code from:
			// http://blog.lanyonm.org/articles/2013/11/21/alphabetize-jekyll-page-tags-pure-liquid.html
			page.content = "{% capture site_tags %}{% for tag in site.tags %}{{ tag | first }}{% unless forloop.last %},{% endunless %}{% endfor %}{% endcapture %}" + 
							"\n{% assign tag_words = site_tags | split:',' | sort %}" +
							"\n<ul id=" + qt + "tag-list" + qt + ">" +
							"{% for item in (0..site.tags.size) %}{% unless forloop.last %}" +
							"{% capture this_word %}{{ tag_words[item] | strip_newlines }}{% endcapture %}" +
							"\n\t<li><a href=" + qt + "#{{ this_word | cgi_escape }}" + qt + " class=" + qt + "tag" + qt + ">{{ this_word }} <span class=" + qt + "tag-count" + qt + ">{{ site.tags[this_word].size }}</span></a></li>" +
							"{% endunless %}{% endfor %}" +
							"\n</ul>" +
							"\n\n{% for item in (0..site.tags.size) %}{% unless forloop.last %}" +
							"\n{% capture this_word %}{{ tag_words[item] | strip_newlines }}{% endcapture %}" + 
							"\n<h3 id=" + qt + "{{ this_word | cgi_escape }}" + qt + ">{{ this_word }}</h3>" +
							"\n<ul class=" + qt + "tag-posts" + qt + ">" +
							"{% for post in site.tags[this_word] %}" + 
							"\n\t<li><span class=" + qt + "post-date" + qt + "><time datetime=" + qt + "{{ post.date | date: " + qt2 + "%F %H:%M" + qt2 + " }}" + qt + ">{{ post.date | date: " + qt2 + "%b %d %Y" + qt2 + " }}</time></span> &raquo; <a href=" + qt + "{{ post.url }}" + qt + ">{{ post.title }}</a></li>{% endfor %}" +
							"\n</ul>" +
							"\n{% endunless %}" +
							"\n{% endfor %}";
			
			// add it
			this.m_pages.push( page );
		}
		
		// creates our archive page
		private function _createArchivePage():void
		{
			// create a page for it
			var page:Page 		= new Page;
			page.isCreatedByApp	= true;
			
			// set our properties
			page.frontMatter.title 		= "Archive";
			page.frontMatter.date		= new Date;
			page.frontMatter.permalink	= "/archive/"	
			page.title					= "Archive";
			page.timestamp				= new Date;
			page.isPage					= true;
			page.urlOld					= "archive";
			page.urlAlias				= "archive";
			page.urlNew					= "/archive/";
			
			var qt:String 	= ( DS.main as Main ).settings.quoteType;
			var qt2:String	= ( qt == "\"" ) ? "'" : "\"";
			
			// for the tags on the end, we're using liquid, so include the site.baseurl etc
			var siteURL:String = "| prepend: site.baseurl";
			if ( ( DS.main as Main ).settings.alsoUseSiteURL )
				siteURL += " | prepend: site.url";
			
			// create our body
			// code from:
			// http://stackoverflow.com/questions/19086284/jekyll-liquid-templating-how-to-group-blog-posts-by-year
			page.content = "{% for post in site.posts %}{% capture currentdate %}{{post.date | date: " + qt2 + "%B %Y" + qt2 + "}}{% endcapture %}{% if currentdate != date %}{% unless forloop.first %}" + 
							"\n\t</ul>{% endunless %}" +
							"\n\t<h3>{{ currentdate }}</h3>" + 
							"\n\t<ul>{% capture date %}{{currentdate}}{% endcapture %}{% endif %}" +
							"\n\t\t<li><a href=" + qt + "{{ post.url }}" + qt + ">{{ post.title }}</a>{% for tag in post.tags %} <a href=" + qt + "{{ " + qt2 + "/tags/#" + qt2 + " " + siteURL + " }}{{ tag | replace:" + qt2 + " " + qt2 + "," + qt2 + "+" + qt2 + " }}" + qt + " class=" + qt + "tag" + qt + ">{{ tag }}</a>{% endfor %}</li>{% endfor %}";
			
			// add it
			this.m_pages.push( page );
		}
		
		// create a search page for our site search
		private function _createSearchPage():void
		{
			// create a page for it
			var page:Page 		= new Page;
			page.isCreatedByApp	= true;
			
			// set our properties
			page.frontMatter.title 		= "Search";
			page.frontMatter.date		= new Date;
			page.frontMatter.permalink	= "/search/"	
			page.title					= "Search";
			page.timestamp				= new Date;
			page.isPage					= true;
			page.urlOld					= "search";
			page.urlAlias				= "search";
			page.urlNew					= "/search/";
			
			var qt:String = ( DS.main as Main ).settings.quoteType;
			
			// create our body
			page.content = "<form action=" + qt + "./" + qt + " accept-charset=" + qt + "UTF-8" + qt + " method=" + qt + "get" + qt + " id=" + qt + "main-search-form" + qt + ">" +
				"\n\t<input type=" + qt + "search" + qt + " maxlength=" + qt + "128" + qt + " name=" + qt + "query" + qt + " id=" + qt + "query" + qt + " placeholder=" + qt + "I'm looking for..." + qt + " required>" +
				"\n</form>" +
				"\n<div id=" + qt + "search-results" + qt + ">" +
				"\n\t<p id=" + qt + "search-feedback" + qt + "></p>" +
				"\n</div>";
			
			// add it
			this.m_pages.push( page );
		}
		
		// create a contact page
		private function _createContactPage():void
		{
			// create a page for it
			var page:Page 		= new Page;
			page.isCreatedByApp	= true;
			
			// set our properties
			page.frontMatter.title 		= "Contact me";
			page.frontMatter.date		= new Date;
			page.frontMatter.permalink	= "/contact/"	
			page.title					= "Contact me";
			page.timestamp				= new Date;
			page.isPage					= true;
			page.urlOld					= "contact";
			page.urlAlias				= "contact";
			page.urlNew					= "/contact/";
			
			var qt:String = ( DS.main as Main ).settings.quoteType;
			
			// create our body
			page.content = "<div id=" + qt + "contact" + qt + ">" +
				"\n\t<form action=" + qt + "#" + qt + " accept-charset=" + qt + "UTF-8" + qt + " method=" + qt + "get" + qt + " id=" + qt + "contact-form" + qt + ">" +
				"\n\t\t<div class=" + qt + "indicates-required" + qt + ">* indicates required</div>" +
				"\n\t\t<input type=" + qt + "text" + qt + " maxlength=" + qt + "128" + qt + " name=" + qt + "contact-name" + qt + " id=" + qt + "contact-name" + qt + " placeholder=" + qt + "They call me... *" + qt + " required>" +
				"\n\t\t<input type=" + qt + "email" + qt + " maxlength=" + qt + "128" + qt + " name=" + qt + "contact-email" + qt + " id=" + qt + "contact-email" + qt + " placeholder=" + qt + "lord.jingles@email.com *" + qt + " required>" +
				"\n\t\t<textarea maxlength=" + qt + "500" + qt + " name=" + qt + "contact-body" + qt + " id=" + qt + "contact-body" + qt + " rows=" + qt + "5" + qt + " placeholder=" + qt + "Blah blah blah... *" + qt + " required></textarea>" +
				"\n\t\t<button name=" + qt + "contact-submit" + qt + " id=" + qt + "contact-submit" + qt + ">I swear this is not spam</button>" +
				"\n\t</form>" +
				"\n\t<div class=" + qt + "feedback-success" + qt + "></div>" +
				"\n\t<div class=" + qt + "feedback-failure" + qt + "></div>" +
				"\n</div>";
			
			// add it
			this.m_pages.push( page );
		}
		
		// create a search page for our 404 page
		private function _create404Page():void
		{
			// create a page for it
			var page:Page 		= new Page;
			page.isCreatedByApp	= true;
			
			// set our properties
			page.frontMatter.title 		= "Four oh four";
			page.frontMatter.date		= new Date;
			page.frontMatter.permalink	= "/404/"	
			page.title					= "Four oh four";
			page.timestamp				= new Date;
			page.isPage					= true;
			page.urlOld					= "404";
			page.urlAlias				= "404";
			page.urlNew					= "/404/";
			
			var qt:String 	= ( DS.main as Main ).settings.quoteType;
			var qt2:String	= ( qt == "\"" ) ? "'" : "\"";
			
			// for the tags on the end, we're using liquid, so include the site.baseurl etc
			var siteURL:String = "| prepend: site.baseurl";
			if ( ( DS.main as Main ).settings.alsoUseSiteURL )
				siteURL += " | prepend: site.url";
			
			// create our body
			page.content = "<p>Er, it looks like that page doesn't exist. Want to try a search for it?</p>" +
				"\n<form action=" + qt + "{{ " + qt2 + "/search/" + qt2 + " " + siteURL + " }}" + qt + " accept-charset=" + qt + "UTF-8" + qt + " method=" + qt + "get" + qt + " id=" + qt + "main-search-form" + qt + ">" +
				"\n\t<input type=" + qt + "search" + qt + " maxlength=" + qt + "128" + qt + " name=" + qt + "query" + qt + " id=" + qt + "query" + qt + " placeholder=" + qt + "I'm looking for..." + qt + " required>" +
				"\n</form>";
			
			// add it
			this.m_pages.push( page );
		}
		
	}

}