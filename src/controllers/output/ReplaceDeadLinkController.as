package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import model.Comment;
	import model.deadlinks.DeadLink;
	import model.deadlinks.DeadLinks;
	import model.output.Page;
	
	/**
	 * Replaces all dead links that we have replacements for
	 * @author Damian Connolly
	 */
	public class ReplaceDeadLinkController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_count:int						= 0;	// the number of replaced links
		private var m_canStart:Boolean				= false;// can we start replacing our links
		private var m_deadLinks:Vector.<DeadLink>	= null; // the dead links that we can replace
		
		/******************************************************************************/
		
		public function ReplaceDeadLinkController()
		{
			this.tooltip = "Replaces dead links from the deadLinks.txt file (REQUIRES one run with VerifyLinkController)";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Replacing links" );
			
			// reset
			this.m_deadLinks	= null;
			this.m_count 		= 0;
			this.m_canStart 	= false;
			
			// load our file - if VerifyLinkController has run, this will already have happened
			DeadLinks.instance.loadFile( this._onDeadLinksReady );
		}
		
		override public function update(dt:Number):void 
		{
			// if we're waiting for the file to load, do nothing
			if ( !this.m_canStart )
				return;
				
			// make sure we have a dead links folder
			if ( ( DS.main as Main ).settings.deadLinkFolder == null )
			{
				( DS.main as Main ).log( Log.ERROR, "Not replacing dead links as the Dead link folder hasn't been set in the Settings, so we can't load our dead links file" );
				this.m_currIndex = this.m_pages.length;
				this.postUpdate();
				return;
			}
				
			// get the current page
			var curr:Page = this.m_pages[this.m_currIndex];
			
			// do our main content
			curr.content = this._processString( curr.content, false );
			
			// do all our comments
			for each( var comment:Comment in curr.comments )
				this._processComment( comment );
				
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			( DS.main as Main ).log( Log.DEBUG, "Replaced " + this.m_count + " dead link(s)" );
		}
		
		/******************************************************************************/
		
		// called by DeadLinks when we're ready to move on
		private function _onDeadLinksReady():void
		{
			this.m_canStart 	= true;
			this.m_deadLinks	= DeadLinks.instance.allReplaceableDeadLinks;
			( DS.main as Main ).log( Log.DEBUG, this.m_deadLinks.length + " individual url(s) will be automatically replaced" );
		}
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			comment.comment = this._processString( comment.comment, true );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// processes a string, extracting all the urls
		private function _processString( str:String, isComment:Boolean ):String
		{				
			// replace any dead links
			for each( var dl:DeadLink in this.m_deadLinks )
			{
				var index:int = str.indexOf( dl.oldURL );
				while ( index != -1 )
				{
					// the next char needs to be a ' or a ", otherwise a deadLink like "www.google.com" will also
					// match "www.google.com/foo", or it could cover a later deadlink match					
					var char:String = str.charAt( index + dl.oldURL.length );
					if ( char == "'" || char == "\"" || char == "<" )
					{
						// replace the url
						str = str.substring( 0, index ) + dl.newURL + str.substring( index + dl.oldURL.length );
						this.m_count++;
					}
					
					// get our new index
					index = str.indexOf( dl.oldURL, index + dl.oldURL.length );
				}
			}
			return str;
		}
		
	}

}