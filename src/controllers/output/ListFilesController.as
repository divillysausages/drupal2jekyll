package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import com.divillysausages.ds.util.ObjectUtils;
	import model.Comment;
	import model.File;
	import model.output.Page;
	
	/**
	 * Lists all the files used in the content
	 * @author Damian Connolly
	 */
	public class ListFilesController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_files:Object 	= null;	// the object to hold our classes
		private var m_regExp:RegExp	= null;	// the regexp that we're going to use
		
		/******************************************************************************/
		
		public function ListFilesController()
		{
			this.tooltip = "Lists any files used, or linked in our pages";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Listing files used" );
			
			// create/clean our object
			this.m_files ||= new Object;
			ObjectUtils.clear( this.m_files );
			
			// create our regexp
			var filesFolder:String	= ( DS.main as Main ).settings.siteFilesFolder;
			this.m_regExp 			= new RegExp( filesFolder + "/" + "[a-zA-Z\-_0-9\.\/]+", "g" );
		}
		
		override public function update(dt:Number):void 
		{				
			// get the current page
			var curr:Page = this.m_pages[this.m_currIndex];
			
			// add any files from the page
			for each( var file:File in curr.files )
				this._addFile( file.filepath );
				
			// add any open graph image
			if ( curr.openGraph != null && curr.openGraph.image != null )
			{
				var siteURL:String	= ( DS.main as Main ).settings.siteURL;
				var index:int 		= curr.openGraph.image.indexOf( siteURL );
				if ( index != -1 )
					this._addFile( curr.openGraph.image.substr( index + siteURL.length ) );
				else
					this._addFile( curr.openGraph.image );
			}
			
			// do our main content
			this._processString( curr.content );
			
			// do all our comments
			for each( var comment:Comment in curr.comments )
				this._processComment( comment );
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			// print our files
			var count:int	= 0;
			var a:Array 	= ObjectUtils.keys( this.m_files );
			if ( a == null )
			{
				( DS.main as Main ).log( Log.DEBUG, "Found no files" );
				super.onFinish();
				return;
			}
			
			// sort and print
			a.sort();
			for each( var s:String in a )
			{
				( DS.main as Main ).log( Log.DEBUG, "\t - " + s );
				count++;
			}
				
			( DS.main as Main ).log( Log.DEBUG, "Found " + count + " files" );
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			this._processString( comment.comment );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// processes a string, listing any files
		private function _processString( str:String ):void
		{	
			// get any files linked directly in our content
			var matchedFiles:Array/**String*/ = str.match( this.m_regExp );
			if ( matchedFiles.length != 0 )
			{
				for each( var fileURL:String in matchedFiles )
					this._addFile( fileURL );
			}
		}
		
		// adds a file to our list
		private function _addFile( fileURL:String ):void
		{
			// remove any starting slash
			if ( fileURL.charAt( 0 ) == "/" )
				fileURL = fileURL.substr( 1 );
			this.m_files[fileURL] = true;
		}
		
	}

}