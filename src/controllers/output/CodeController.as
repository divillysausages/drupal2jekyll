package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import model.Comment;
	import model.output.Page;
	import mx.utils.StringUtil;
	
	/**
	 * Makes sure that all code is properly wrapped in pre>code elements
	 * @author Damian Connolly
	 */
	public class CodeController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_curr:Page				= null;	// our current page
		private var m_preCount:int			= 0; 	// the amount of <pre> elements we've changed
		private var m_preCountComments:int	= 0; 	// the amount of <pre> elements in comments that we've changed
		private var m_codeCount:int			= 0; 	// the amount of <code> elements we've changed
		private var m_codeCountComments:int	= 0; 	// the amount of <code> elements in comments that we've changed
		private var m_styleCodeType:String	= null;	// the default style code type for our <code> elements
		
		/******************************************************************************/
		
		public function CodeController()
		{
			this.tooltip = "Makes sure all code is in the form <pre><code style=\"language-X\"></code></pre>";
		}
		
		override public function start( pages:Vector.<Page> ):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Validating code samples" );
			
			// get our style code type
			this.m_styleCodeType = ( DS.main as Main ).settings.styleCodeType;
		}
		
		override public function update(dt:Number):void 
		{
			// get the current page
			this.m_curr = this.m_pages[this.m_currIndex];
			
			// check if it's been set
			if ( this.m_styleCodeType == null || StringUtil.trim( this.m_styleCodeType ) == "" )
			{
				( DS.main as Main ).log( Log.WARN, "CodeController needs to have the 'Default &lt;code&gt; type (style)' set in the Settings menu in order to work" );
				this.m_currIndex = this.m_pages.length;
				this.postUpdate();
				return;
			}
			
			// reset counts
			this.m_preCount				= 0;
			this.m_preCountComments		= 0;
			this.m_codeCount			= 0;
			this.m_codeCountComments	= 0;
			
			// do our main content
			this.m_curr.content = this._processString( this.m_curr.content, false );
			
			// do all our comments
			for each( var comment:Comment in this.m_curr.comments )
				this._processComment( comment );
				
			// log if necessary
			if( this.m_preCount > 0 || this.m_codeCount > 0 )
				( DS.main as Main ).log( Log.DEBUG, "Updated " + this.m_preCount + " &lt;pre&gt; element(s) (" + this.m_preCountComments + " comment(s)) and " + 
					this.m_codeCount + " &lt;code&gt; element(s) (" + this.m_codeCountComments + " comment(s)) in '" + this.m_curr.title + "' (" + this.m_curr.filename + ")" );
			
			// post update
			this.postUpdate();
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			comment.comment = this._processString( comment.comment, true );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// actually processes our strings
		private function _processString( str:String, isComment:Boolean ):String
		{
			var codeStr:String = null;
			
			var qt:String = ( DS.main as Main ).settings.quoteType;
			
			// find any <pre> elements and make sure the inside is <code>
			// NOTE: we're assuming actionscript here
			var startIndex:int 		= str.indexOf( "<pre" );
			var endBracketIndex:int	= startIndex;
			var endIndex:int		= startIndex;
			while ( startIndex != -1 )
			{
				// get the end bracket index (the pre might have a class or id)
				endBracketIndex = str.indexOf( ">", startIndex + 4 );
				if ( endBracketIndex == -1 )
				{
					( DS.main as Main ).log( Log.ERROR, "The '" + this.m_curr.title + "' page (" + this.m_curr.filename + ") contains a mal-formatted &lt;pre&gt; tag (is comment: " + isComment + ")" );
					break;
				}
				
				// check if the next bit is code
				var codeIndex:int = str.indexOf( "<code", endBracketIndex  + 1 );
				if ( codeIndex != 0 )
				{
					// it doesn't have an inner <code>, so add one
					endIndex 	= str.indexOf( "</pre>", endBracketIndex + 1 );
					codeStr		= ( this.m_styleCodeType == null || this.m_styleCodeType == "" ) ? "<code>" : "<code class=" + qt + this.m_styleCodeType + qt + ">";
					str			= str.substring( 0, endBracketIndex + 1 ) + 
									codeStr + 
									str.substring( endBracketIndex + 1, endIndex ) +
									"</code></pre>" +
									str.substring( endIndex + 6 );
					
					this.m_preCount++;
					if ( isComment )
						this.m_preCountComments++;
				}
				//else
					// it has an inner <code> tag - so the check to see if it has the right class will take place later
				
				// look for our next index
				startIndex = str.indexOf( "<pre", endIndex );
			} // end <pre> while
			
			// find any <code> elements and make sure they have a class="language-X"
			if ( this.m_styleCodeType != null && this.m_styleCodeType != "" ) // only if we actually have one set
			{
				startIndex 		= str.indexOf( "<code" );
				endBracketIndex	= startIndex;
				endIndex		= startIndex;
				while ( startIndex != -1 )
				{
					// NOTE: we need to check to see if it has a class already, or if we can just add it. if
					// it already has a class, we need to check if "language-" is part of it, or if we need to
					// add them
					
					// get the end tag index for the <code> element
					endBracketIndex = str.indexOf( ">", startIndex + 5 );
					if ( endBracketIndex == -1 )
					{
						( DS.main as Main ).log( Log.ERROR, "The '" + this.m_curr.title + "' page (" + this.m_curr.filename + ") contains a mal-formatted &lt;code&gt; tag (is comment: " + isComment + ")" );
						break;
					}
					
					// check if we need to wrap it in a <pre> tag (if it's over multiple lines)
					var endCodeIndex:int		= str.indexOf( "</code>", startIndex + 5 );
					var newLineIndex:int 		= str.indexOf( "\r\n", startIndex + 5 );
					var hasIncremented:Boolean	= false;
					if ( newLineIndex != -1 && newLineIndex < endCodeIndex )
					{						
						// check that it's not already in a <pre> tag
						if ( str.indexOf( "</pre>", endCodeIndex + 7 ) != endCodeIndex + 7 )
						{
							str = str.substring( 0, startIndex ) + 
									"<pre>" + 
									str.substring( startIndex, endCodeIndex + 7 ) +
									"</pre>" +
									str.substring( endCodeIndex + 7 );
							startIndex 		+= 5;
							endBracketIndex += 5;
							
							// update our counter
							this.m_codeCount++;
							if ( isComment )
								this.m_codeCountComments++;
							hasIncremented = true;
						}
					}
					
					// check for a "class" property
					var classStartIndex:int = str.indexOf( "class", startIndex + 5 );
					if ( classStartIndex == -1 || classStartIndex > endBracketIndex )
					{
						// here, it doesn't have a class property, so we can just add it
						str = str.substring( 0, startIndex ) + 
								"<code class=" + qt + this.m_styleCodeType + qt + ">" + 
								str.substring( endBracketIndex + 1 );
								
						if ( !hasIncremented )
						{
							this.m_codeCount++;
							if ( isComment )
								this.m_codeCountComments++;
						}
					}
					else
					{
						// it has a class property, so we need to check if "language-" is there
						var classEndIndex:int 	= str.indexOf( "\"", classStartIndex + 5 );
						classEndIndex			= str.indexOf( "\"", classEndIndex + 1 ); // get the end quote
						var languageIndex:int	= str.indexOf( "language-", classStartIndex + 5 );
						if ( languageIndex == -1 || languageIndex > classEndIndex )
						{
							// here, we don't have a "language-" class, so we can add one
							str = str.substring( 0, classEndIndex ) +
									" " + this.m_styleCodeType +
									str.substring( classEndIndex );
									
							if ( !hasIncremented )
							{
								this.m_codeCount++;
								if ( isComment )
									this.m_codeCountComments++;
							}
						}
						// else
							// it already has a "language-" class, so we can ignore it
					}
					
					// look for our next index
					startIndex = str.indexOf( "<code", endBracketIndex + 1 );
					
				} // end <code> while
			}
			return str;
		}
		
	}

}