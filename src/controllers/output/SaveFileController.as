package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import model.output.Page;
	
	/**
	 * Saves the html pages so jekyll can work with them
	 * @author Damian Connolly
	 */
	public class SaveFileController extends OutputController 
	{
		
		public function SaveFileController()
		{
			this.tooltip = "Actually saves the final files to the _post folder (REQUIRES CreateFilename)";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Saving posts" );
		}
		
		override public function update(dt:Number):void 
		{
			// get the current page
			var curr:Page = this.m_pages[this.m_currIndex];
			
			// get our file object and save our content
			var file:File = null;
			if ( curr.isPage )
				file = ( DS.main as Main ).settings.savePagesFolder.resolvePath( curr.filename );
			else
				file = ( DS.main as Main ).settings.savePostsFolder.resolvePath( curr.filename );
			this._saveFile( curr.frontMatter.toString() + curr.content, file );
			
			// save out our comments
			var comments:String = curr.getCommentsStr();
			if ( comments != null )
			{
				file = ( DS.main as Main ).settings.saveCommentsFolder.resolvePath( curr.filename );
				this._saveFile( comments, file );
			}
			
			// save out our files
			var files:String = curr.getFilesStr();
			if ( files != null )
			{
				file = ( DS.main as Main ).settings.saveFilesFolder.resolvePath( curr.filename );
				this._saveFile( files, file );
			}
			
			// save out our open graph
			var openGraph:String = curr.getOpenGraphStr();
			if ( openGraph != null )
			{
				file = ( DS.main as Main ).settings.saveOpenGraphFolder.resolvePath( curr.filename );
				this._saveFile( openGraph, file );
			}
			
			// post update
			this.postUpdate();
		}
		
		/******************************************************************************/
		
		// saves a string to file
		private function _saveFile( str:String, file:File ):void
		{
			var fs:FileStream = new FileStream;
			fs.open( file, FileMode.WRITE );
			fs.writeUTFBytes( str );
			fs.close();
		}
		
	}

}