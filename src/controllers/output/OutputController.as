package controllers.output 
{
	import com.divillysausages.ds.core.IUpdateObj;
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.util.ClassUtils;
	import model.output.Page;
	import org.osflash.signals.Signal;
	
	/**
	 * A controller parses all the nodes in the list, performing a particular job
	 * @author Damian Connolly
	 */
	public class OutputController implements IUpdateObj
	{
		
		/******************************************************************************/
		
		/**
		 * The signal dispatched when we're updating. It should take one param of type 
		 * Number (% completion)
		 */
		public var signalOnUpdate:Signal = null;
		
		/**
		 * The signal dispatched when we're finished this controller. It should take
		 * one param of type Vector.<Page>
		 */
		public var signalOnComplete:Signal = null;
		
		/**
		 * Should we run this controller?
		 */
		public var enabled:Boolean = true;
		
		/**
		 * The tooltip for this controller (for the settings)
		 */
		public var tooltip:String = null;
		
		/**
		 * The simple classname of this class (so we don't need to continously get it)
		 */
		public var simpleClassname:String = null;
		
		/******************************************************************************/
		
		protected var m_pages:Vector.<Page> = null; // the pages that we're working on
		protected var m_currIndex:int		= 0;	// the current page that we're working on
		protected var m_active:Boolean 		= false;// are we currently active
		
		/******************************************************************************/
		
		public function get active():Boolean { return this.m_active; }
		
		/******************************************************************************/
		
		public function OutputController() 
		{
			this.signalOnUpdate		= new Signal( Number ); 		// % completion
			this.signalOnComplete	= new Signal( Vector.<Page> );	// the pages we were working on
			this.simpleClassname	= ClassUtils.getSimpleClassname( this );
			
			DS.addToUpdate( this );
		}
		
		/**
		 * Starts the controller working
		 * @param pages The pages that we're working on
		 */
		public function start( pages:Vector.<Page> ):void
		{
			this.m_pages 		= pages;
			this.m_currIndex	= 0;
			this.m_active		= true;
		}
		
		/**
		 * Called every frame that we're active
		 * @param dt The delta time since the last call to update
		 */
		public function update( dt:Number ):void
		{
			
		}
		
		/**
		 * Called after every update
		 */
		public function postUpdate():void
		{
			// update our current index
			this.m_currIndex++;
			this.signalOnUpdate.dispatch( this.m_currIndex / this.m_pages.length );
			if ( this.m_currIndex >= this.m_pages.length )
			{
				this.m_active = false;
				this.onFinish();
				this.signalOnComplete.dispatch( this.m_pages );
			}
		}
		
		/**
		 * Called when we finish, but before we relinquish control
		 */
		public function onFinish():void
		{
			
		}
		
	}

}