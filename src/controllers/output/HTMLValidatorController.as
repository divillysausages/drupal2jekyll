package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import flash.automation.MouseAutomationAction;
	import model.Comment;
	import model.output.Page;
	
	/**
	 * Makes sure that our content is wrapped with html tags (as Drupal let you write without)
	 * @author Damian Connolly
	 */
	public class HTMLValidatorController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_curr:Page			= null;						// the current page that we're working on
		private var m_reNewLine:RegExp	= /(\r\n)/g;				// the regexp for finding a new line
		private var m_reMultBrs:RegExp	= /(<br>){2,}/g;			// the regexp for finding multiple <br> tags
		private var m_reBr:RegExp		= /<br>/g;					// the regexp for finding a <br>
		private var m_reBrEndTag:RegExp	= /><br>/g;					// the regexp for finding a <br> at the end of a tag
		private var m_rePOrBr:RegExp	= /(<p>)|(<\/p>)|(<br>)/g;	// the regexp for finding <p> || </p> || <br>
		
		/******************************************************************************/
		
		public function HTMLValidatorController()
		{
			this.tooltip = "Does some simple validation to make sure the content is wrapped in <p> tags (needs verification after)";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Validating HTML (simple)" );
		}
		
		override public function update(dt:Number):void 
		{
			// get the current page
			this.m_curr = this.m_pages[this.m_currIndex];
			
			// ignore pages created by the app as we created that html
			if ( !this.m_curr.isCreatedByApp )
			{
				// do our main content
				this.m_curr.content = this._processString( this.m_curr.content, false );
				
				// do all our comments
				for each( var comment:Comment in this.m_curr.comments )
					this._processComment( comment );
			}
			
			// post update
			this.postUpdate();
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			comment.comment = this._processString( comment.comment, true );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// actually processes our strings
		private function _processString( str:String, isComment:Boolean ):String
		{			
			// make sure it starts with a < (i.e. a html tag)
			if ( str.charAt( 0 ) != "<" || str.charAt( str.length - 1 ) != ">" )
			{					
				if ( isComment )
					( DS.main as Main ).log( Log.WARN, "Wrapping a comment on '" + this.m_curr.title + "' (" + this.m_curr.urlNew +
						") in &lt;p&gt; tags" );
				else
					( DS.main as Main ).log( Log.WARN, "It looks like '" + this.m_curr.title + "' (" + this.m_curr.urlNew + 
						") was written in Filtered HTML; wrapping in &lt;p&gt; tags" );
						
				str = str.replace( this.m_reNewLine, "<br>" );			// replace new lines with <br>
				str = str.replace( this.m_reMultBrs, "</p>\r\n<p>" );	// replace multiple <br>s with <p>
				str = str.replace( this.m_reBr, "<br>\r\n" );			// replace the remaining <br>s with <br>s and a newline
				str = str.replace( this.m_reBrEndTag, ">" );			// remove <br>s at the end of another tag (e.g. <li>)
				str = "<p>" + str + "</p>";								// wrap everything in a <p>
				
				// remove <p> and <br> tags that are needlessly wrapping other tags
				var index:int = str.indexOf( "<p><" );
				while ( index != -1 )
				{
					// get the tag name
					var spaceIndex:int 		= str.indexOf( " ", index + 4 ); // could have a class etc
					var endBracketIndex:int	= str.indexOf( ">", index + 4 ); // the ending ">"
					var endIndex:int		= ( endBracketIndex != -1 && endBracketIndex < spaceIndex ) ? endBracketIndex : spaceIndex; // take the first one to get the end of the tag name
					var tagName:String		= str.substring( index + 4, endIndex );
					
					// find the end of this tag
					var endTagIndex:int 	= str.indexOf( "</" + tagName + ">", spaceIndex );
					var endPTagIndex:int	= ( endTagIndex != -1 ) ? str.indexOf( "</p>", endTagIndex ) : -1; // include the wrapping </p> end tag
					var snippet:String		= str.substring( index, ( endPTagIndex == -1 ) ? Number.MAX_VALUE : endPTagIndex + 4 );
					var len:int				= snippet.length;
					snippet					= snippet.replace( this.m_rePOrBr, "" );
					str						= str.substring( 0, index ) + snippet + str.substring( index + len )
					
					// get the next one
					if ( endPTagIndex == -1 )
						break;
					index = str.indexOf( "<p><", endPTagIndex );
				}
			}
			return str;
		}
		
	}

}