package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import com.divillysausages.ds.util.ObjectUtils;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import model.Comment;
	import model.output.Page;
	
	/**
	 * Lists all the css classes used in the content so we can get rid of any old ones
	 * @author Damian Connolly
	 */
	public class ListClassesController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_reClass:RegExp		= /class=('|")[a-zA-Z0-9\-_ ]+('|")/g; // the regexp that we're using for this
		private var m_classes:Object 		= null;	// the object to hold our classes
		private var m_siteClasses:Object	= null; // the site classes from our CSS file
		private var m_startChecking:Boolean	= false;// should we start checking for our classes?
		
		/******************************************************************************/
		
		public function ListClassesController()
		{
			this.tooltip = "Lists all the CSS classes actually used in our old CSS file";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Listing CSS classes used" );
			this.m_startChecking = false;
			
			// create/clean our object
			this.m_classes ||= new Object;
			ObjectUtils.clear( this.m_classes );
			
			// load our css
			if ( this.m_siteClasses == null && ( DS.main as Main ).settings.cssFile != null )
			{
				var file:File = ( DS.main as Main ).settings.cssFile;
				file.addEventListener( Event.COMPLETE, this._onCSSLoad );
				file.addEventListener( IOErrorEvent.IO_ERROR, this._onCSSFail );
				file.addEventListener( SecurityErrorEvent.SECURITY_ERROR, this._onCSSFail );
				file.load()
			}
			else
				this.m_startChecking = true; // we can just start checking
		}
		
		override public function update(dt:Number):void 
		{
			if ( !this.m_startChecking )
				return;
				
			// failsafe if we don't have a css file
			if ( ( DS.main as Main ).settings.cssFile == null )
			{
				( DS.main as Main ).log( Log.WARN, "ListClassesController needs to have the 'Site CSS file' set in the Settings menu in order to work" );
				this.m_currIndex = this.m_pages.length;
				this.postUpdate();
				return;
			}
				
			// get the current page
			var curr:Page = this.m_pages[this.m_currIndex];
			
			// do our main content
			this._processString( curr.content );
			
			// do all our comments
			for each( var comment:Comment in curr.comments )
				this._processComment( comment );
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			// print our classes
			var count:int	= 0;
			var a:Array 	= ObjectUtils.keys( this.m_classes );
			if ( a == null )
			{
				( DS.main as Main ).log( Log.DEBUG, "Found no classes used in the CSS" );
				super.onFinish();
				return;
			}
			
			// sort and print
			a.sort();
			for each( var s:String in a )
			{
				var isUsed:Boolean = ( this.m_siteClasses != null && s in this.m_siteClasses );
				if ( isUsed )
				{
					( DS.main as Main ).log( Log.DEBUG, "\t - ." + s );
					count++;
				}
			}
				
			( DS.main as Main ).log( Log.DEBUG, "Found " + count + " unique class(es) used in the CSS" );
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			this._processString( comment.comment );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// the function that actually processes our string
		private function _processString( str:String ):void
		{
			// get any links in the content
			var matchedCSS:Array/**String*/ = str.match( this.m_reClass );
			if ( matchedCSS.length != 0 )
			{
				for ( var i:int = 0; i < matchedCSS.length; i++ )
				{
					// get the css declaration
					var css:String = matchedCSS[i];
					
					// get the starting quote
					var startIndex:int = css.indexOf( "\"" );
					if ( startIndex == -1 )
						startIndex = css.indexOf( "'" );
						
					// get the url
					var classesStr:String = css.substring( startIndex + 1, css.length - 1 );
					
					// split on a space in the case of multiple classes
					var classes:Array = classesStr.split( " " );
					for each( var c:String in classes )
						this.m_classes[c] = true;
				}
			}
		}
		
		// called when our css has loaded
		private function _onCSSLoad( e:Event ):void
		{
			// clean
			var file:File = ( e.target as File );
			this._cleanUpFile( file );
			
			// get our css
			file.data.position	= 0;
			var css:String 		= file.data.toString();
			file.data.position	= 0;
			
			// find our classes
			this.m_siteClasses	= new Object;
			var classes:Array 	= css.match( /\.[^\[0-9\] ][^) :,\r\n\.;]+/g ); // match anything starting with "." that doesn't contain "){SPACE}:,\r\n"
			for each( var s:String in classes )									// NOTE: there might be some false positives (e.g. images), but it won't 
				this.m_siteClasses[s.substr( 1 )] = true;						// match stuff like "0.85em".
																				// NOTE: When storing, remove the initial "."
			
			// we can start checking
			this.m_startChecking = true;
		}
		
		// called when our css has failed to load
		private function _onCSSFail( e:Event ):void
		{
			// log and clean
			var file:File = ( e.target as File );
			( DS.main as Main ).log( Log.WARN, "Can't load the CSS file '" + file.url + "': " + e );
			this._cleanUpFile( file );
			
			// we can start checking
			this.m_startChecking = true;
		}
		
		// cleans up the file
		private function _cleanUpFile( file:File ):void
		{
			file.removeEventListener( Event.COMPLETE, this._onCSSLoad );
			file.removeEventListener( IOErrorEvent.IO_ERROR, this._onCSSFail );
			file.removeEventListener( SecurityErrorEvent.SECURITY_ERROR, this._onCSSFail );
		}
		
	}

}