package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import model.output.Page;
	import model.Term;
	import model.TermNode;
	
	/**
	 * Generates 301 redirects for a .htaccess file, to move old urls to the new ones
	 * @author Damian Connolly
	 */
	public class HTAccess301Controller extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_escapeChars:Array 			= null; // the chars that we have to escape, otherwise it won't work
		private var m_escapeRegExps:Vector.<RegExp>	= null;	// the chars that we have to escape, as a regexp
		private var m_tagRegExp:RegExp				= null;	// the regexp when dealing with tags
		private var m_redirectStr:String			= null;	// the string for our simple redirects
		private var m_rewriteStr:String				= null;	// the string for our rewrite redirects
		
		/******************************************************************************/
		
		public function HTAccess301Controller()
		{
			this.tooltip = "Generates 301 redirects and URL rewrites for a .htaccess file";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			if ( ( DS.main as Main ).settings.use302Redirects )
				( DS.main as Main ).log( Log.LOG, "Generating 302 redirects" );
			else
				( DS.main as Main ).log( Log.LOG, "Generating 301 redirects" );
			
			// create our escape chars for url matching
			this.m_escapeChars = ["\\", "^", "$", ".", "[", "]", "|", "(", ")", "?", "+", "*", "{", "}"];
			
			// create our regexps
			var len:int				= this.m_escapeChars.length;
			this.m_escapeRegExps	= new Vector.<RegExp>( len, true );
			for ( var i:int = 0; i < len; i++ )
				this.m_escapeRegExps[i] = new RegExp( "\\" + this.m_escapeChars[i], "g" );
				
			// create our tag regexp
			this.m_tagRegExp = /\b[a-zA-Z]/g;
			
			// clear our strings
			this.m_redirectStr 	= "";
			this.m_rewriteStr	= "&lt;IfModule mod_rewrite.c&gt;\n  RewriteEngine on"; // NOTE: log is html, so escape it
		}
		
		override public function update(dt:Number):void 
		{
			// get the redirect to use
			var redirect:String	= ( ( DS.main as Main ).settings.use302Redirects ) ? "302" : "301";
			
			// get the current page
			var curr:Page = this.m_pages[this.m_currIndex];
			if ( curr.isTagsPage )
			{
				this._writeTagsRedirects( curr, redirect );
				this.postUpdate();
				return;
			}
			
			// get the site url and the actual redirect used
			var siteURL:String = ( DS.main as Main ).settings.siteURL;
			
			// escape our new url
			var urlNew:String = this._escapeURL( curr.urlNew );
			
			// add the title as a comment
			this.m_redirectStr 	+= "\n# " + curr.title + "\n";
			this.m_rewriteStr 	+= "\n  # " + curr.title + "\n";
			
			// REDIRECT
			// do the internal url, then the alias
			this.m_redirectStr += "Redirect " + redirect + " /" + curr.urlOld + " " + siteURL + urlNew + "\n";
			if( curr.urlAlias != null )
				this.m_redirectStr += "Redirect " + redirect + " /" + curr.urlAlias + " " + siteURL + urlNew + "\n";
				
			// REWRITE (NOTE: add optional trailing slash)
			var oldEsc:String 	= this._escapeURL( curr.urlOld ) + "/?";
			var aliasEsc:String	= ( curr.urlAlias != null ) ? this._escapeURL( curr.urlAlias ) + "/?" : null;
			this.m_rewriteStr 	+= "  RewriteCond %{QUERY_STRING} ^q=" + oldEsc + "$ [NC" + ( ( curr.urlAlias != null ) ? ",OR]\n" : "]\n" );
			if ( curr.urlAlias != null )
				this.m_rewriteStr += "  RewriteCond %{QUERY_STRING} ^q=" + aliasEsc + "$ [NC]\n";
			this.m_rewriteStr	+= "  RewriteRule ^$ " + siteURL + urlNew + " [L,R=" + redirect + ",QSD]\n";
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			// add the end to the rewrite string
			this.m_rewriteStr += "&lt;/IfModule&gt;"; // NOTE: log is html, so escape it
			
			// log it all out
			( DS.main as Main ).log( Log.DEBUG, "[NOTE]: Remember to remove the old Drupal rewrite rules, as they're not needed anymore" );
			( DS.main as Main ).log( Log.DEBUG, "Copy the following into your .htaccess file if you're using clean URLs or want simple redirects:" );
			( DS.main as Main ).log( Log.LOG, this.m_redirectStr );
			( DS.main as Main ).log( Log.DEBUG, "Copy the following into your .htaccess file if you're NOT using clean URLs or want to keep query params etc:" );
			( DS.main as Main ).log( Log.LOG, this.m_rewriteStr );
		}
		
		/******************************************************************************/
		
		// writes all the redirects for our tags
		private function _writeTagsRedirects( page:Page, redirect:String ):void
		{
			// get the site url
			var siteURL:String = ( DS.main as Main ).settings.siteURL;
			
			// get all our tags (NOTE: Terms not TermNodes)
			var tags:Vector.<Term> = Term.terms;
			for each( var t:Term in tags )
			{			
				// add the title as a comment
				this.m_redirectStr 	+= "\n# (tag) " + t.name + "\n";
				this.m_rewriteStr 	+= "\n  # (tag) " + t.name + "\n";
				
				var oldURL:String 	= "taxonomy/term/" + t.tid;
				var urlNew:String	= this._escapeURL( page.urlNew + "#" + t.name.replace( this.m_tagRegExp, this._tagCapitalise ) );
				
				// REDIRECT
				// do the internal url, then the alias
				this.m_redirectStr += "Redirect " + redirect + " /" + oldURL + " " + siteURL + urlNew + "\n";
					
				// REWRITE (NOTE: add optional trailing slash)
				var oldEsc:String 	= this._escapeURL( oldURL ) + "/?";
				this.m_rewriteStr 	+= "  RewriteCond %{QUERY_STRING} ^q=" + oldEsc + "$ [NC]\n";
				this.m_rewriteStr	+= "  RewriteRule ^$ " + siteURL + urlNew + " [L,R=" + redirect + ",QSD]\n";
			}
		}
		
		// called by the replace
		private function _tagCapitalise():String
		{
			return arguments[0].toUpperCase();
		}
		
		// escapes a url
		private function _escapeURL( url:String ):String
		{
			var len:int = this.m_escapeRegExps.length;
			for ( var i:int = 0; i < len; i++ )
			{
				var regexp:RegExp 	= this.m_escapeRegExps[i];
				var char:String		= this.m_escapeChars[i];
				url 				= url.replace( regexp, "\\" + char );
			}
			
			// replace any spaces
			url = url.replace( / /g, "+" );
			return url;
		}
		
	}

}