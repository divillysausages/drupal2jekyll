package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.utils.Dictionary;
	import model.Comment;
	import model.deadlinks.DeadLink;
	import model.deadlinks.DeadLinks;
	import model.output.Page;
	
	/**
	 * Tries to connect to all absolute links to make sure they're still valid
	 * @author Damian Connolly
	 */
	public class VerifyLinkController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_reURL:RegExp					= /(href|src) ?= ?('|")[a-zA-Z0-9:_#&=%,~!@; \$\.\/\?\-\+]+('|")/g; // the regexp we use to match urls
		private var m_currURLs:Dictionary			= null;	// the current urls that we're loading
		private var m_currChecks:Vector.<URLLoader>	= null; // the current request verifying our links
		private var m_previouslyChecked:Dictionary	= null;	// previously checked links (which we don't need to do again)
		private var m_links:Array/**String*/		= null;	// the current links for the page
		private var m_count:int						= 0;	// the number of bad links
		private var m_individualCount:int			= 0;	// the total number of bad links
		private var m_total:int						= 0;	// the total number of links
		private var m_canStartLoading:Boolean		= false;// can we start loading our links?
		
		/******************************************************************************/
		
		public function VerifyLinkController()
		{
			this.m_currChecks 			= new Vector.<URLLoader>;
			this.m_currURLs				= new Dictionary( true );
			this.m_previouslyChecked	= new Dictionary( true );
			this.tooltip				= "Verifies all URLs, listing those that are dead";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Verifying links" );
			this.m_count 			= 0;
			this.m_individualCount	= 0;
			this.m_total			= 0;
			
			// clear our previous dictionaries
			for ( var key:* in this.m_previouslyChecked )
			{
				this.m_previouslyChecked[key] = null;
				delete this.m_previouslyChecked[key];
			}
			
			// load our file
			this.m_canStartLoading = false;
			DeadLinks.instance.loadFile( this._onDeadLinksReady );
		}
		
		override public function update(dt:Number):void 
		{
			// if we're currently checking, or we can't start loading, do nothing
			if ( this.m_currChecks.length > 0 || !this.m_canStartLoading )
				return;
				
			// if the dead links folder doesn't exist, then do nothing
			if ( ( DS.main as Main ).settings.deadLinkFolder == null )
			{
				( DS.main as Main ).log( Log.ERROR, "Not verifying links as the Dead link folder hasn't been set in the Settings, so we won't be able to load/save our dead links file" );
				this.m_currIndex = this.m_pages.length;
				this.postUpdate();
				return;
			}
				
			// get the current page
			var curr:Page = this.m_pages[this.m_currIndex];
			
			// do our main content
			curr.content = this._processString( curr.content, false );
			
			// do all our comments
			for each( var comment:Comment in curr.comments )
				this._processComment( comment );
			
			// load up as many as possible
			if ( this.m_links != null && this.m_links.length > 0 )
			{
				while ( this.m_links.length > 0 && this.m_currChecks.length < 5 )
					this._loadNextURL();
			}
			else
				this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			( DS.main as Main ).log( Log.DEBUG, "Found " + this.m_count + " unique broken link(s) (" + this.m_individualCount + 
				" instance(s)) from a total of " + this.m_total + ( ( this.m_total > 0 ) ? " (" + ( this.m_count / this.m_total ).toFixed( 2 ) + ")" : "" ) );
				
			// get DeadLinks to save its file
			DeadLinks.instance.saveFile();
		}
		
		/******************************************************************************/
		
		// called by DeadLinks when we're ready to move on
		private function _onDeadLinksReady():void
		{
			this.m_canStartLoading = true;
		}
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			comment.comment = this._processString( comment.comment, true );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// processes a string, extracting all the urls
		private function _processString( str:String, isComment:Boolean ):String
		{
			if ( isComment )
			{
				var links:Array = str.match( this.m_reURL );
				for each( var link:String in links )
					this.m_links.push( link );
			}
			else
				this.m_links = str.match( this.m_reURL );
			return str;
		}
		
		// loads the next url from our list
		private function _loadNextURL():void
		{
			// if we've no more links to check, move on
			if ( this.m_links == null || this.m_links.length == 0 )
			{
				// only move on once all the checks for this page are done
				if( this.m_currChecks.length == 0 )
					this.postUpdate();
				return;
			}
				
			// remove the url
			var url:String 		= this.m_links.shift();
			var startIndex:int 	= url.indexOf( "\"" );
			if ( startIndex == -1 )
				startIndex = url.indexOf( "'" );
			url = url.substring( startIndex + 1, url.length - 1 );
			
			// if it's not an absolute link, or we've already checked it, ignore it
			if ( url.indexOf( "http" ) != 0 || url in this.m_previouslyChecked )
			{
				if( DeadLinks.instance.hasDeadLink( url, false ) )
					this.m_individualCount++;
				this._loadNextURL();
				return;
			}
			
			// store it as a previously checked link
			this.m_previouslyChecked[url] = true;
			this.m_total++;
			
			// if this is a url that will be automatically replaced, we can ignore it
			if( DeadLinks.instance.hasDeadLink( url, false ) )
			{
				this._loadNextURL();
				return;
			}
			
			// create our request
			var request:URLRequest 	= new URLRequest( url );
			request.method			= URLRequestMethod.HEAD;
			request.idleTimeout		= 500;
			
			// create our loader
			var urlLoader:URLLoader = new URLLoader;
			urlLoader.addEventListener( Event.COMPLETE, this._cleanLoader );
			urlLoader.addEventListener( HTTPStatusEvent.HTTP_STATUS, this._onHTTPStatusEvent );
			urlLoader.addEventListener( IOErrorEvent.IO_ERROR, this._onFail );
			urlLoader.addEventListener( SecurityErrorEvent.SECURITY_ERROR, this._onFail );
			this.m_currChecks.push( urlLoader );
			this.m_currURLs[urlLoader] = url;
			try 
			{ 
				urlLoader.load( request ); 
			}
			catch ( e:Error )
			{
				this._onFail( null, urlLoader );
			}
		}
		
		// cleans the loader
		private function _cleanLoader( e:Event = null, loader:URLLoader = null ):void
		{
			// get our loader
			if ( loader == null && e != null )
				loader = ( e.target as URLLoader );
				
			if ( loader == null )
				return;
			loader.removeEventListener( Event.COMPLETE, this._cleanLoader );
			loader.removeEventListener( HTTPStatusEvent.HTTP_STATUS, this._onHTTPStatusEvent );
			loader.removeEventListener( IOErrorEvent.IO_ERROR, this._onFail );
			loader.removeEventListener( SecurityErrorEvent.SECURITY_ERROR, this._onFail );
			
			// remove it from our vector
			var index:int = this.m_currChecks.indexOf( loader );
			if ( index != -1 )
				this.m_currChecks.splice( index, 1 );
				
			// remove if from our dictionary
			this.m_currURLs[loader] = null;
			delete this.m_currURLs[loader];
			
			// load the next url
			this._loadNextURL();
		}
		
		// called when there's been an io error with our loader
		private function _onFail( e:Event = null, loader:URLLoader = null ):void
		{
			// get our loader
			if ( loader == null && e != null )
				loader = ( e.target as URLLoader );
				
			// get our url
			var url:String = this.m_currURLs[loader] as String;
			if ( url != null )
			{
				( DS.main as Main ).log( Log.ERROR, "Couldn't load link '" + url + "' from page '" + this.m_pages[this.m_currIndex].title + "'" );
				DeadLinks.instance.addDeadLink( url, -1, this.m_pages[this.m_currIndex].title );
				this.m_count++;
				this.m_individualCount++;
			}
			this._cleanLoader( e, loader );
		}

		// called when we get the http response from our loader
		private function _onHTTPStatusEvent( e:HTTPStatusEvent ):void
		{
			// get our loader and url
			var loader:URLLoader 	= ( e.target as URLLoader );
			var url:String			= this.m_currURLs[loader] as String;
			
			// NOTE: these match the default status codes for the url monitor
			// NOTE: don't clean the loader, otherwise onComplete will crash
			var isOK:Boolean = ( e.status == 200 /*OK*/ || e.status == 202 /*Accepted*/ || e.status == 204 /*No content*/ || e.status == 205 /*Reset content*/ || e.status == 206 /*Partial content*/ );
			if ( !isOK )
			{
				( DS.main as Main ).log( Log.ERROR, e.status + " - '" + url + "' in '" + this.m_pages[this.m_currIndex].title + "' (" + this.m_pages[this.m_currIndex].urlNew + ")" );
				
				// remove it from the urls dictionary, which we'll use as a sign that we've already logged this url, to stop double logging
				// as the io error event will trigger
				this.m_currURLs[loader] = null;
				delete this.m_currURLs[loader];
				
				DeadLinks.instance.addDeadLink( url, e.status, this.m_pages[this.m_currIndex].title );
				this.m_count++;
				this.m_individualCount++;
			}
		}
		
	}

}