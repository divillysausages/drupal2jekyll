package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import model.Comment;
	import model.output.Page;
	
	/**
	 * Lists all the files that have php code in them
	 * @author Damian Connolly
	 */
	public class ListPHPController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_rePHP:RegExp	= /<\?(php)?/g; // the regexp we use for finding php
		private var m_curr:Page		= null; // the current page
		private var m_count:int 	= 0; 	// the number of php files we found
		
		/******************************************************************************/
		
		public function ListPHPController()
		{
			this.tooltip = "Lists all the files that have embedded php code";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Listing files with PHP code" );
			
			this.m_count = 0;
		}
		
		override public function update(dt:Number):void 
		{				
			// get the current page
			this.m_curr = this.m_pages[this.m_currIndex];
			
			// do our main content
			this._processString( this.m_curr.content );
			
			// do all our comments
			for each( var comment:Comment in this.m_curr.comments )
				this._processComment( comment );
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			( DS.main as Main ).log( Log.DEBUG, "Found " + this.m_count + " file(s) with PHP code" );
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			this._processString( comment.comment );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// processes a string, looking for php
		private function _processString( str:String ):void
		{
			// get any files linked directly in our content
			// NOTE: we're looking for "<?php", or "<?" (short tags)
			var matchedPHP:Array/**String*/ = str.match( this.m_rePHP );
			if ( matchedPHP.length > 0 )
			{
				this.m_curr.hasPHP = true;
				this.m_count++;
				( DS.main as Main ).log( Log.DEBUG, "\t - " + this.m_curr.urlNew );
			}
		}
		
	}

}