package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import model.Comment;
	import model.File;
	import model.output.Page;
	
	/**
	 * Finds any relative urls and replaces them with absolute ones (works better with RSS)
	 * @author Damian Connolly
	 */
	public class ReplaceRelativeURLController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_reURL:RegExp			= /(href|src) ?= ?('|")[a-zA-Z0-9:_#&=%,~!@; \$\.\/\?\-\+]+('|")/g; // the regexp we use to find urls
		private var m_reURLReplace:RegExp	= /[\.\/]{2,}/g; // the regexp we use when replacing urls
		private var m_curr:Page				= null;	// the current page
		private var m_count:int 			= 0;	// the total number of replaced urls
		
		/******************************************************************************/
		
		public function ReplaceRelativeURLController()
		{
			this.tooltip = "Replaces all relative URLs with their absolute counterparts";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Replacing relative URLs" );
			this.m_count = 0;
		}
		
		override public function update(dt:Number):void 
		{
			// get the current page
			this.m_curr 		= this.m_pages[this.m_currIndex];
			var siteURL:String	= ( DS.main as Main ).settings.siteURL;
			
			// do our main content
			this.m_curr.content = this._processString( this.m_curr.content, siteURL );
			
			// do all our comments
			for each( var comment:Comment in this.m_curr.comments )
				this._processComment( comment, siteURL );
				
			// do all our files
			for each( var file:File in this.m_curr.files )
			{
				if ( file.filepath.indexOf( "http" ) != 0 )
				{
					file.filepath = siteURL + file.filepath;
					this.m_count++;
				}
			}
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			( DS.main as Main ).log( Log.DEBUG, "Replaced " + this.m_count + " relative URLs" );
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment, siteURL:String ):void
		{
			comment.comment = this._processString( comment.comment, siteURL );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child, siteURL );
			}
		}
		
		// processes a string, replacing relative urls
		private function _processString( str:String, siteURL:String ):String
		{
			var matchedURLs:Array/**String*/ = str.match( this.m_reURL );
			if ( matchedURLs.length != 0 )
			{
				for ( var i:int = 0; i < matchedURLs.length; i++ )
				{
					// get the url
					var url:String = matchedURLs[i];
					
					// get the starting quote
					var startIndex:int = url.indexOf( "\"" );
					if ( startIndex == -1 )
						startIndex = url.indexOf( "'" );
						
					// get the url
					var newURL:String = url.substring( startIndex + 1, url.length - 1 );
					
					// relative links will start with a "#" or a "?", while absolute links
					// will have a "http" in front, so outside of these, we're only interested 
					// in the links that start with "/" or "../"
					if ( newURL.charAt( 0 ) == "?" || newURL.charAt( 0 ) == "#" )
						newURL = ( this.m_curr.urlAlias != null ) ? "/" + this.m_curr.urlAlias + newURL : "/" + this.m_curr.urlOld + newURL; // add the page url before it
					else if ( newURL.indexOf( "www" ) == 0 )
						newURL = "http://" + newURL;
					else if ( newURL.charAt( 0 ) != "/" && newURL.charAt( 0 ) != "." )
						continue;
						
					// if the url starts with "//", then assume that we've wanted to leave it that way
					if ( newURL.indexOf( "//" ) == 0 )
						continue;
						
					// replace any "../../"'s with just a "/" - this is because all pages are delivered
					// from the same "place" in drupal, so all relative links should have the same number
					// of dots
					newURL = newURL.replace( this.m_reURLReplace, "/" );
					
					// add in the site url, and the href/src part
					newURL = url.substring( 0, startIndex + 1 ) + siteURL + newURL + url.charAt( url.length - 1 );
					
					// replace it in the content
					str = str.replace( url, newURL );
					this.m_count++;
				}
			}
			return str;
		}
		
	}

}