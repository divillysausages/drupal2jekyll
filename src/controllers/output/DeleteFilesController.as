package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.log.Log;
	import flash.filesystem.File;
	import model.output.Page;
	
	/**
	 * Deletes all the files currently in the save folder
	 * @author Damian Connolly
	 */
	public class DeleteFilesController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_files:Array = null; // the array of files to remove
		
		/******************************************************************************/
		
		public function DeleteFilesController()
		{
			this.tooltip = "Deletes any previously-created files";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Deleting previously generated files" );
			
			// get our list of files
			this.m_files 		= ( DS.main as Main ).settings.savePostsFolder.getDirectoryListing();
			var allPages:Array	= ( DS.main as Main ).settings.savePagesFolder.getDirectoryListing();
			var comments:Array 	= ( DS.main as Main ).settings.saveCommentsFolder.getDirectoryListing();
			var files:Array		= ( DS.main as Main ).settings.saveFilesFolder.getDirectoryListing();
			var opengraph:Array	= ( DS.main as Main ).settings.saveOpenGraphFolder.getDirectoryListing();
			for each( var f:File in allPages )
				this.m_files.push( f );
			for each( f in comments )
				this.m_files.push( f );
			for each( f in files )
				this.m_files.push( f );
			for each( f in opengraph )
				this.m_files.push( f );
		}
		
		override public function update(dt:Number):void 
		{
			// failsafe - this is possible if we have no files to delete
			if ( this.m_currIndex >= this.m_files.length )
			{
				this.postUpdate();
				return;
			}
			
			// get our current file
			var curr:File = this.m_files[this.m_currIndex];
			
			// try and delete it
			try { curr.deleteFile(); }
			catch ( e:Error ) { ( DS.main as Main ).log( Log.ERROR, "Can't delete '" + curr.nativePath + "': " + e.errorID + ": " + e.name + ": " + e.message ); }
			
			// call post update
			this.postUpdate();
		}
		
		// NOTE: we override the post update as we're creating the vector of pages, so
		// we decide when we're finished based on the nodes instead
		override public function postUpdate():void
		{
			// update our current index
			this.m_currIndex++;
			this.signalOnUpdate.dispatch( this.m_currIndex / this.m_files.length );
			if ( this.m_currIndex >= this.m_files.length )
			{				
				this.m_active = false;
				this.signalOnComplete.dispatch( this.m_pages ); // null as we haven't created any pages yet
			}
		}
		
	}

}