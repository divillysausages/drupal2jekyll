package controllers.output 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.input.Input;
	import com.divillysausages.ds.input.Key;
	import com.divillysausages.ds.log.Log;
	import com.divillysausages.ds.util.ObjectUtils;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.filesystem.File;
	import model.Comment;
	import model.output.Page;
	
	/**
	 * Lists all the IDs used in the content so we can get rid of any old ones (CSS)
	 * @author Damian Connolly
	 */
	public class ListIDsController extends OutputController 
	{
		
		/******************************************************************************/
		
		private var m_reIDs:RegExp			= /id=('|")[a-zA-Z0-9\-_]+('|")/g; // the regexp we use for finding our ids
		private var m_ids:Object 			= null;	// the object to hold our classes
		private var m_siteIDs:Object		= null; // the site ids from our CSS file
		private var m_startChecking:Boolean	= false;// should we start checking for our ids?
		
		/******************************************************************************/
		
		public function ListIDsController()
		{
			this.tooltip = "Lists all the CSS IDs actually used in our old CSS file";
		}
		
		override public function start(pages:Vector.<Page>):void 
		{
			super.start(pages);
			( DS.main as Main ).log( Log.LOG, "Listing IDs used" );
			
			// create/clean our object
			this.m_ids ||= new Object;
			ObjectUtils.clear( this.m_ids );
			
			// load our css
			if ( this.m_siteIDs == null && ( DS.main as Main ).settings.cssFile != null )
			{
				var file:File = ( DS.main as Main ).settings.cssFile;
				file.addEventListener( Event.COMPLETE, this._onCSSLoad );
				file.addEventListener( IOErrorEvent.IO_ERROR, this._onCSSFail );
				file.addEventListener( SecurityErrorEvent.SECURITY_ERROR, this._onCSSFail );
				file.load()
			}
			else
				this.m_startChecking = true; // we can just start checking
		}
		
		override public function update(dt:Number):void 
		{
			if ( !this.m_startChecking )
				return;
				
			// failsafe if we don't have a css file
			if ( ( DS.main as Main ).settings.cssFile == null )
			{
				( DS.main as Main ).log( Log.WARN, "ListIDsController needs to have the 'Site CSS file' set in the Settings menu in order to work" );
				this.m_currIndex = this.m_pages.length;
				this.postUpdate();
				return;
			}
				
			// get the current page
			var curr:Page = this.m_pages[this.m_currIndex];
			
			// do our main content
			this._processString( curr.content );
			
			// do all our comments
			for each( var comment:Comment in curr.comments )
				this._processComment( comment );
			
			// post update
			this.postUpdate();
		}
		
		override public function onFinish():void 
		{
			// print our classes
			var count:int	= 0;
			var a:Array 	= ObjectUtils.keys( this.m_ids );
			if ( a == null )
			{
				( DS.main as Main ).log( Log.DEBUG, "Found no IDs used in the CSS" );
				super.onFinish();
				return;
			}
			
			// sort and print
			a.sort();
			for each( var s:String in a )
			{
				var isUsed:Boolean = ( this.m_siteIDs != null && s in this.m_siteIDs );
				if ( isUsed )
				{
					( DS.main as Main ).log( Log.DEBUG, "\t - #" + s );
					count++;
				}
			}
				
			( DS.main as Main ).log( Log.DEBUG, "Found " + count + " unique ID(s) used in the CSS" );
		}
		
		/******************************************************************************/
		
		// processes a comment and all its children
		private function _processComment( comment:Comment ):void
		{
			this._processString( comment.comment );
			if ( comment.children.length > 0 )
			{
				for each( var child:Comment in comment.children )
					this._processComment( child );
			}
		}
		
		// processes a string for any ids
		private function _processString( str:String ):void
		{
			// get any links in the content
			var matchedIDs:Array/**String*/ = str.match( this.m_reIDs );
			if ( matchedIDs.length != 0 )
			{
				for ( var i:int = 0; i < matchedIDs.length; i++ )
				{
					// get the id declaration
					var idStr:String = matchedIDs[i];
					
					// get the starting quote
					var startIndex:int = idStr.indexOf( "\"" );
					if ( startIndex == -1 )
						startIndex = idStr.indexOf( "'" );
						
					// get the id itself and add it to our object
					var id:String 	= idStr.substring( startIndex + 1, idStr.length - 1 );
					this.m_ids[id]	= true;
				}
			}
		}
		
		// called when our css has loaded
		private function _onCSSLoad( e:Event ):void
		{
			// clean
			var file:File = ( e.target as File );
			this._cleanUpFile( file );
			
			// get our css
			file.data.position	= 0;
			var css:String 		= file.data.toString();
			file.data.position	= 0;
			
			// find our classes
			this.m_siteIDs	= new Object;
			var ids:Array 	= css.match( /#[a-zA-Z0-9-_]+/g ); 	// match anything starting with "#"
			for each( var s:String in ids )						// NOTE: there might be some false positives (e.g. colours), but ah well
				this.m_siteIDs[s.substr( 1 )] = true;
			
			// we can start checking
			this.m_startChecking = true;
		}
		
		// called when our css has failed to load
		private function _onCSSFail( e:Event ):void
		{
			// log and clean
			var file:File = ( e.target as File );
			( DS.main as Main ).log( Log.WARN, "Can't load the CSS file '" + file.url + "': " + e );
			this._cleanUpFile( file );
			
			// we can start checking
			this.m_startChecking = true;
		}
		
		// cleans up the file
		private function _cleanUpFile( file:File ):void
		{
			file.removeEventListener( Event.COMPLETE, this._onCSSLoad );
			file.removeEventListener( IOErrorEvent.IO_ERROR, this._onCSSFail );
			file.removeEventListener( SecurityErrorEvent.SECURITY_ERROR, this._onCSSFail );
		}
		
	}

}