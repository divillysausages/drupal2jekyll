package controllers.debug 
{
	import flash.text.TextField;
	import model.Upload;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class UploadController extends Controller
	{
		public var txtFID:TextField		= null;
		public var txtNID:TextField		= null;
		public var txtDesc:TextField	= null;
		public var txtFile:TextField	= null;
		public var txtNode:TextField	= null;
		
		public function UploadController(main:Main) 
		{
			super(main);
			this.max = Upload.uploads.length;
			
			this.txtFID		= this._createTextField( 10, 10 );
			this.txtNID		= this._createTextField( 10, this.txtFID.y + this.txtFID.height + 5.0 );
			this.txtDesc	= this._createTextField( 10, this.txtNID.y + this.txtNID.height + 5.0 );
			this.txtFile	= this._createTextField( 10, this.txtDesc.y + this.txtDesc.height + 5.0 );
			this.txtNode	= this._createTextField( 10, this.txtFile.y + this.txtFile.height + 5.0 );
		}
		
		override protected function _show(i:int):void 
		{
			var upload:Upload = Upload.uploads[i];
				
			// display it
			this.txtFID.text 	= upload.fid + "";
			this.txtNID.text	= upload.nid + "";
			this.txtDesc.text	= upload.description;
			this.txtFile.text	= upload.file + "";
			this.txtNode.text	= upload.node + "";
		}
		
	}

}