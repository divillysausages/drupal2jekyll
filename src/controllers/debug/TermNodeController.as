package controllers.debug 
{
	import flash.text.TextField;
	import model.TermNode;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class TermNodeController extends Controller
	{
		public var txtNID:TextField		= null;
		public var txtTID:TextField		= null;
		public var txtNode:TextField	= null;
		public var txtTerm:TextField	= null;
		
		public function TermNodeController(main:Main) 
		{
			super(main);
			this.max = TermNode.termNodes.length;
			
			this.txtNID		= this._createTextField( 10, 10 );
			this.txtTID		= this._createTextField( 10, this.txtNID.y + this.txtNID.height + 5.0 );
			this.txtNode	= this._createTextField( 10, this.txtTID.y + this.txtTID.height + 5.0 );
			this.txtTerm	= this._createTextField( 10, this.txtNode.y + this.txtNode.height + 5.0 );
		}
		
		override protected function _show(i:int):void 
		{
			var term:TermNode = TermNode.termNodes[i];
				
			// display it
			this.txtNID.text	= term.nid + "";
			this.txtTID.text 	= term.tid + "";
			this.txtNode.text	= term.node + "";
			this.txtTerm.text	= term.term + "";
		}
		
	}

}