package controllers.debug 
{
	import flash.text.TextField;
	import model.File;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class FileController extends Controller 
	{
		public var txtFID:TextField		= null;
		public var txtName:TextField	= null;
		public var txtPath:TextField	= null;
		public var txtMime:TextField	= null;
		public var txtSize:TextField	= null;
		public var txtDate:TextField	= null;
		
		public function FileController(main:Main) 
		{
			super(main);
			this.max = File.files.length;
			
			this.txtFID		= this._createTextField( 10, 10 );
			this.txtName	= this._createTextField( 10, this.txtFID.y + this.txtFID.height + 5.0 );
			this.txtPath	= this._createTextField( 10, this.txtName.y + this.txtName.height + 5.0 );
			this.txtMime	= this._createTextField( 10, this.txtPath.y + this.txtPath.height + 5.0 );
			this.txtSize	= this._createTextField( 10, this.txtMime.y + this.txtMime.height + 5.0 );
			this.txtDate	= this._createTextField( 10, this.txtSize.y + this.txtSize.height + 5.0 );
		}
		
		override protected function _show(i:int):void 
		{
			var file:File = File.files[i];
				
			// display it
			this.txtFID.text 	= file.fid + "";
			this.txtName.text 	= file.filename;
			this.txtPath.text 	= file.filepath;
			this.txtMime.text 	= file.filemime;
			this.txtSize.text 	= file.filesize + "";
			this.txtDate.text 	= file.timestamp + "";
		}
		
	}

}