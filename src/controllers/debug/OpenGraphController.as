package controllers.debug 
{
	import flash.text.TextField;
	import model.OpenGraph;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class OpenGraphController extends Controller 
	{
		public var txtNID:TextField		= null;
		public var txtTitle:TextField	= null;
		public var txtDesc:TextField	= null;
		public var txtImage:TextField	= null;
		public var txtType:TextField	= null;
		public var txtNode:TextField	= null;
		
		public function OpenGraphController(main:Main) 
		{
			super(main);
			this.max = OpenGraph.graphs.length;
			
			this.txtNID		= this._createTextField( 10, 10 );
			this.txtTitle	= this._createTextField( 10, this.txtNID.y + this.txtNID.height + 5.0 );
			this.txtDesc	= this._createTextField( 10, this.txtTitle.y + this.txtTitle.height + 5.0 );
			this.txtImage	= this._createTextField( 10, this.txtDesc.y + this.txtDesc.height + 5.0 );
			this.txtType	= this._createTextField( 10, this.txtImage.y + this.txtImage.height + 5.0 );
			this.txtNode	= this._createTextField( 10, this.txtType.y + this.txtType.height + 5.0 );
		}
		
		override protected function _show(i:int):void 
		{
			var graph:OpenGraph = OpenGraph.graphs[i];
				
			// display it
			this.txtNID.text 	= graph.nid + "";
			this.txtTitle.text 	= graph.title + "";
			this.txtDesc.text 	= graph.description + "";
			this.txtImage.text 	= graph.image + "";
			this.txtType.text 	= graph.type + "";
			this.txtNode.text 	= graph.node + "";
		}
		
	}

}