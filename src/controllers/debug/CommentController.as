package controllers.debug 
{
	import flash.text.TextField;
	import model.Comment;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class CommentController extends Controller 
	{
		public var txtCID:TextField 		= null;
		public var txtPID:TextField			= null;
		public var txtNID:TextField			= null;
		public var txtComment:TextField		= null;
		public var txtTimestamp:TextField	= null;
		public var txtName:TextField		= null;
		public var txtMail:TextField		= null;
		public var txtHomepage:TextField	= null;
		public var txtParent:TextField		= null;
		public var txtNode:TextField		= null;
		
		public function CommentController(main:Main) 
		{
			super(main);
			this.max = Comment.comments.length;
			
			this.txtCID 		= this._createTextField( 10, 10 );
			this.txtPID			= this._createTextField( 10, this.txtCID.y + this.txtCID.height + 5.0 );
			this.txtNID			= this._createTextField( 10, this.txtPID.y + this.txtPID.height + 5.0 );
			this.txtComment		= this._createTextField( 10, this.txtNID.y + this.txtNID.height + 5.0 );
			this.txtTimestamp	= this._createTextField( 10, this.txtComment.y + this.txtComment.height + 5.0 );
			this.txtName		= this._createTextField( 10, this.txtTimestamp.y + this.txtTimestamp.height + 5.0 );
			this.txtMail		= this._createTextField( 10, this.txtName.y + this.txtName.height + 5.0 );
			this.txtHomepage	= this._createTextField( 10, this.txtMail.y + this.txtMail.height + 5.0 );
			this.txtParent		= this._createTextField( 10, this.txtHomepage.y + this.txtHomepage.height + 5.0 );
			this.txtNode		= this._createTextField( 10, this.txtParent.y + this.txtParent.height + 5.0 );
		}
		
		override protected function _show(i:int):void 
		{
			var comment:Comment = Comment.comments[i];
				
			// display it
			this.txtCID.text 		= comment.cid + "";
			this.txtPID.text 		= comment.pid + "";
			this.txtNID.text 		= comment.nid + "";
			this.txtComment.text	= comment.comment;
			this.txtTimestamp.text	= comment.timestamp + "";
			this.txtName.text		= comment.name + "";
			this.txtMail.text		= comment.mail + "";
			this.txtHomepage.text	= comment.homepage + "";
			this.txtParent.text		= comment.parent + "";
			this.txtNode.text	 	= comment.node + "";
		}
		
	}

}