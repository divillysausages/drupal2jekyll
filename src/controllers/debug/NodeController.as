package controllers.debug 
{
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import model.Node;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class NodeController extends Controller
	{
		public var txtNID:TextField 	= null;
		public var txtTitle:TextField	= null;
		public var txtBody:TextField	= null;
		public var txtDate:TextField	= null;
		
		public function NodeController( main:Main ) 
		{
			super( main );
			this.max = Node.nodes.length;
			
			this.txtNID 			= this._createTextField( 10, 10 );
			this.txtTitle			= this._createTextField( 10, this.txtNID.y + this.txtNID.height + 5.0 );
			this.txtBody			= this._createTextField( 10, this.txtTitle.y + this.txtTitle.height + 5.0 );
			this.txtBody.autoSize	= TextFieldAutoSize.NONE;
			this.txtBody.width		= 500;
			this.txtBody.height		= 500;
			this.txtBody.wordWrap	= true;
			this.txtBody.multiline	= true;
			this.txtDate			= this._createTextField( 10, this.txtBody.y + this.txtBody.height + 5.0 );
		}
		
		override protected function _show(i:int):void 
		{
			var node:Node = Node.nodes[i];
				
			// display it
			this.txtNID.text 	= node.nid + "";
			this.txtTitle.text	= node.title;
			this.txtBody.text	= node.body;
			this.txtDate.text	= node.timestamp + "";
		}
		
	}

}