package controllers.debug 
{
	import flash.text.TextField;
	import model.URLAlias;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class URLAliasController extends Controller
	{
		public var txtPID:TextField		= null;
		public var txtSrc:TextField		= null;
		public var txtDest:TextField	= null;
		public var txtNode:TextField	= null;
		
		public function URLAliasController(main:Main) 
		{
			super(main);
			this.max = URLAlias.aliases.length;
			
			this.txtPID		= this._createTextField( 10, 10 );
			this.txtSrc		= this._createTextField( 10, this.txtPID.y + this.txtPID.height + 5.0 );
			this.txtDest	= this._createTextField( 10, this.txtSrc.y + this.txtSrc.height + 5.0 );
			this.txtNode	= this._createTextField( 10, this.txtDest.y + this.txtDest.height + 5.0 );
		}
		
		override protected function _show(i:int):void 
		{
			var alias:URLAlias = URLAlias.aliases[i];
				
			// display it
			this.txtPID.text 	= alias.pid + "";
			this.txtSrc.text 	= alias.src;
			this.txtDest.text	= alias.dst;
			this.txtNode.text	= alias.node + "";
		}
		
	}

}