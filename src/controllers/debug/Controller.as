package controllers.debug 
{
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class Controller 
	{
		public var main:Main = null;
		public var curr:int = 0;
		public var max:int = 0;
		
		public function Controller( main:Main ):void 
		{
			this.main = main;
			this.main.stage.addEventListener( KeyboardEvent.KEY_DOWN, this._onKeyDown );
		}
		
		protected function _show( i:int ):void
		{
			
		}
		
		protected function _createTextField( x:Number, y:Number ):TextField
		{
			var t:TextField 	= new TextField;
			var tf:TextFormat	= new TextFormat( "Trebuchet Ms", 12 );
			t.defaultTextFormat	= tf;
			t.border			= true;
			t.autoSize			= TextFieldAutoSize.LEFT;
			t.x					= x;
			t.y					= y;
			t.selectable		= false;
			t.mouseEnabled		= false;
			t.mouseWheelEnabled	= false;
			t.text				= " ";
			this.main.addChild( t );
			return t;
		}
		
		private function _onKeyDown( e:KeyboardEvent ):void
		{
			if ( e.keyCode != Keyboard.SPACE )
				return;
				
			this._show( this.curr );
			this.curr++;
			if ( this.curr >= this.max )
				this.curr = 0;
		}
		
	}

}