package controllers.debug 
{
	import flash.text.TextField;
	import model.Term;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class TermController extends Controller
	{
		public var txtTID:TextField		= null;
		public var txtName:TextField	= null;
		
		public function TermController(main:Main) 
		{
			super(main);
			this.max = Term.terms.length;
			
			this.txtTID		= this._createTextField( 10, 10 );
			this.txtName	= this._createTextField( 10, this.txtTID.y + this.txtTID.height + 5.0 );
		}
		
		override protected function _show(i:int):void 
		{
			var term:Term = Term.terms[i];
				
			// display it
			this.txtTID.text 	= term.tid + "";
			this.txtName.text 	= term.name;
		}
	}

}