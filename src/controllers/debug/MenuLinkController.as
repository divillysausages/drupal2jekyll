package controllers.debug 
{
	import flash.text.TextField;
	import model.MenuLink;
	/**
	 * ...
	 * @author Damian Connolly
	 */
	public class MenuLinkController extends Controller 
	{
		public var txtMLID:TextField	= null;
		public var txtPLID:TextField	= null;
		public var txtPath:TextField	= null;
		public var txtTitle:TextField	= null;
		public var txtATitle:TextField	= null;
		public var txtWeight:TextField	= null;
		public var txtParent:TextField	= null;
		
		public function MenuLinkController(main:Main) 
		{
			super(main);
			this.max = MenuLink.links.length;
			
			this.txtMLID	= this._createTextField( 10, 10 );
			this.txtPLID	= this._createTextField( 10, this.txtMLID.y + this.txtMLID.height + 5.0 );
			this.txtPath	= this._createTextField( 10, this.txtPLID.y + this.txtPLID.height + 5.0 );
			this.txtTitle	= this._createTextField( 10, this.txtPath.y + this.txtPath.height + 5.0 );
			this.txtATitle	= this._createTextField( 10, this.txtTitle.y + this.txtTitle.height + 5.0 );
			this.txtWeight	= this._createTextField( 10, this.txtATitle.y + this.txtATitle.height + 5.0 );
			this.txtParent	= this._createTextField( 10, this.txtWeight.y + this.txtWeight.height + 5.0 );
		}
		
		override protected function _show(i:int):void 
		{
			var link:MenuLink = MenuLink.links[i];
				
			// display it
			this.txtMLID.text 	= link.mlid + "";
			this.txtPLID.text	= link.plid + "";
			this.txtPath.text	= link.linkPath;
			this.txtTitle.text	= link.linkTitle;
			this.txtATitle.text	= link.linkATitle + "";
			this.txtWeight.text	= link.weight + "";
			this.txtParent.text	= link.parent + "";
		}
		
	}

}