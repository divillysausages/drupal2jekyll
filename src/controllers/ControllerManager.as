package controllers 
{
	import com.divillysausages.ds.DS;
	import com.divillysausages.ds.util.ClassUtils;
	import controllers.output.CodeController;
	import controllers.output.ConvertToPageController;
	import controllers.output.CreateFilenameController;
	import controllers.output.DeleteFilesController;
	import controllers.output.HTAccess301Controller;
	import controllers.output.HTMLValidatorController;
	import controllers.output.JekyllCodeController;
	import controllers.output.ListClassesController;
	import controllers.output.ListFilesController;
	import controllers.output.ListIDsController;
	import controllers.output.ListPHPController;
	import controllers.output.MarkDeadLinksController;
	import controllers.output.OutputController;
	import controllers.output.ListMenuController;
	import controllers.output.ReplaceDeadLinkController;
	import controllers.output.ReplaceOldURLsController;
	import controllers.output.ReplaceRelativeURLController;
	import controllers.output.ReplaceSiteURLController;
	import controllers.output.SaveFileController;
	import controllers.output.TapirPushController;
	import controllers.output.VerifyLinkController;
	import model.app.Settings;
	import model.deadlinks.DeadLinks;
	import model.output.Page;
	import org.osflash.signals.Signal;
	/**
	 * The controller manager holds all the controllers that will be working on our pages
	 * and tells us when they're all done
	 * @author Damian Connolly
	 */
	public class ControllerManager 
	{
		
		/******************************************************************************/
		
		/**
		 * The signal dispatched when we're updating. It should take two params of type 
		 * Number (% completion, one for the current controller, and one for overall)
		 */
		public var signalOnUpdate:Signal = null;
		
		/**
		 * The signal dispatched when we're finished this controller. It should take
		 * no params
		 */
		public var signalOnComplete:Signal = null;
		
		/******************************************************************************/
		
		private var m_controllers:Vector.<OutputController>	= null;
		private var m_currIndex:int							= 0;
		private var m_currEnabledIndex:int					= 0;
		private var m_numEnabledControllers:int				= 0;
		
		/******************************************************************************/
		
		/**
		 * The controllers that we're using
		 */
		public function get controllers():Vector.<OutputController> { return this.m_controllers; }
		
		/******************************************************************************/
		
		public function ControllerManager() 
		{
			this.signalOnUpdate		= new Signal( Number, Number ); // % on curr controller, % overall
			this.signalOnComplete	= new Signal;
			this.m_controllers		= new Vector.<OutputController>;
			
			// add all our controllers
			this.add( new DeleteFilesController );
			this.add( new ConvertToPageController );
			this.add( new CreateFilenameController );
			this.add( new HTMLValidatorController );
			this.add( new CodeController );
			this.add( new JekyllCodeController ); // NOTE: should come after CodeController, but it's not essential
			this.add( new ReplaceRelativeURLController );
			this.add( new VerifyLinkController );
			this.add( new ReplaceDeadLinkController ); // NOTE: should come after VerifyLinkController and before MarkDeadLinksController, but it's not essential
			this.add( new MarkDeadLinksController );
			this.add( new ReplaceOldURLsController ); // NOTE: this requires the ReplaceRelativeURLController before it, but should come before the ReplaceSiteURLController
			this.add( new ReplaceSiteURLController );
			this.add( new ListClassesController );
			this.add( new ListIDsController );
			this.add( new ListFilesController );
			this.add( new ListPHPController );
			this.add( new ListMenuController );
			this.add( new TapirPushController ); // needs to come after the CreateFilenameController, as that's where the new urls are generated
			this.add( new HTAccess301Controller ); // needs to come after the CreateFilenameController, as that's where the new urls are generated
			
			// NOTE: this one *has* to be last
			this.add( new SaveFileController );
		}
		
		/**
		 * Adds an output controller - it'll be added in the order necessary
		 * @param controller The controller that we're adding
		 */
		public function add( controller:OutputController ):void
		{
			this.m_controllers.push( controller );
		}
		
		/**
		 * Starts the controllers off
		 */
		public function start():void
		{
			// before we start, we need to check if our controllers are enabled
			var settings:Settings 			= ( DS.main as Main ).settings;
			this.m_numEnabledControllers	= 0;
			for each( var controller:OutputController in this.m_controllers )
			{
				controller.enabled = settings.getUseController( controller.simpleClassname );
				if ( controller.enabled )
					this.m_numEnabledControllers++;
			}
			
			// reset the DeadLinks class
			DeadLinks.instance.reset();
			
			// set our index and start
			this.m_currIndex 		= -1;
			this.m_currEnabledIndex = -1;
			this._onControllerFinished( null ); // the first controller should create the vector of pages
		}
		
		/******************************************************************************/
		
		// called when a controller is updating
		private function _onControllerUpdate( pcComplete:Number ):void
		{
			this.signalOnUpdate.dispatch( pcComplete, this.m_currEnabledIndex / this.m_numEnabledControllers );
		}
		
		// called when a controller is finished
		private function _onControllerFinished( pages:Vector.<Page> ):void
		{
			// clear our signals
			var curr:OutputController = ( this.m_currIndex < 0 || this.m_currIndex >= this.m_controllers.length ) ? null : this.m_controllers[this.m_currIndex];
			if ( curr != null )
			{
				curr.signalOnUpdate.remove( this._onControllerUpdate );
				curr.signalOnComplete.remove( this._onControllerFinished );
			}
			
			// get the next one
			this.m_currIndex++;
			if ( this.m_currIndex >= this.m_controllers.length )
			{
				this.signalOnUpdate.dispatch( 1.0, 1.0 );
				this.signalOnComplete.dispatch();
			}
			else
			{
				curr = this.m_controllers[this.m_currIndex];
				if ( curr.enabled )
					this.m_currEnabledIndex++;
				this.signalOnUpdate.dispatch( 0.0, this.m_currEnabledIndex / this.m_controllers.length );
				if ( curr.enabled )
				{
					curr.signalOnUpdate.add( this._onControllerUpdate );
					curr.signalOnComplete.add( this._onControllerFinished );
					curr.start( pages );
				}
				else
					this._onControllerFinished( pages ); // move on to the next one
			}
		}
		
	}

}