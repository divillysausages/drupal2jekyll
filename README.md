# README #

Drupal2Jekyll is a tool to help convert a Drupal 6 site to a Jekyll static site. See [http://divillysausages.com/2015/06/03/drupal-2-jekyll/](http://divillysausages.com/2015/06/03/drupal-2-jekyll/) for more info on [what it can do](http://divillysausages.com/2015/06/03/drupal-2-jekyll/#functionality), and [how to download the necessary content](http://divillysausages.com/2015/06/03/drupal-2-jekyll/#howto) from your Drupal site in order to convert it.

To run the tool, you can install the Drupal2Jekyll.air file, or compile from source using FlashDevelop (FlashBuilder should work as well, but I haven't included the project files).

Also included, in the js/ folder, are 3 support JS files, that I use for the functionality for the auto-generated 404, contact, and search pages.

Damian

[http://divillysausages.com](http://divillysausages.com)